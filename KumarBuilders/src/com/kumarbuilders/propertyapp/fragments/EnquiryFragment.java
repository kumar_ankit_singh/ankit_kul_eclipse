package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class EnquiryFragment extends Fragment {

	private Activity activity;
	private EditText edtEmail, edtName, edtContactNo;
	private Spinner propertySpinner;
	private String name, email, contactno, propertyname;
	private Button btnsend;
	private List<String> properties = new ArrayList<String>();
	private int pos;
	private String property_name;
	private RelativeLayout header;
	private ArrayAdapter<String> adapter;
	private PropertyService propertyService;
	private ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.enquiry, null);

		edtEmail = (EditText) v.findViewById(R.id.edtemail);
		edtName = (EditText) v.findViewById(R.id.edtname);
		edtContactNo = (EditText) v.findViewById(R.id.edtmobno);
		propertySpinner = (Spinner) v.findViewById(R.id.spinnerproperty);
		btnsend = (Button) v.findViewById(R.id.btnenquirysend);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {

			activity = getActivity();
			getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			propertyService = PropertyServiceImpl.getInstance();

			getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getIntentData();
			getAllPropertyList(property_name);

			edtContactNo.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					String stringstate = edtContactNo.getText().toString();
					int pos = stringstate.length();
					CommonFunctions.phoneNoLimitWithAutoSoftKeyClose(pos, activity);

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			edtName.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});

			btnsend.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						getEditTextData();
						validations();
						edtName.setText("");
						edtEmail.setText("");
						edtContactNo.setText("");

					} catch (Exception e) {

					}

				}
			});

			propertySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
					int position = propertySpinner.getSelectedItemPosition();

				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

					propertySpinner.setSelection(pos);

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void validations() {
		Pattern userNamePattern = Pattern.compile("^[A-Za-z0-9]{3,16}$");
		Pattern pattern = Pattern
				.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		boolean valid = PhoneNumberUtils.isGlobalPhoneNumber(contactno);

		if (name.equals("") && email.equals("") && contactno.equals("")) {
			Toast.makeText(activity, "Fill All Details", Toast.LENGTH_SHORT).show();
		} else {

			Matcher matcher1 = userNamePattern.matcher(name);
			Matcher matcher = pattern.matcher(email);
			if (!matcher1.matches()) {
				Toast.makeText(activity, "Invalid Username", Toast.LENGTH_SHORT).show();
			} else if (!matcher.matches()) {
				Toast.makeText(activity, "Invalid Email Address", Toast.LENGTH_SHORT).show();
			} else if (!valid) {
				Toast.makeText(activity, "Invalid Contact Number", Toast.LENGTH_SHORT).show();
			} else {
				if (propertyname.equals("activity")) {
					sendEmail(name, email, contactno, property_name);
				} else {
					sendEmail(name, email, contactno, propertyname);
				}
			}
		}
	}

	public void getAllPropertyList(String property_name) {
		for (int i = 0; i < getallproperties().size(); i++) {
			PropertiesDetails property = propertyList.get(i);
			properties.add(property.getProperty_name());
		}
		adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, properties);
		propertySpinner.setAdapter(adapter);

		if (!property_name.equals("activity")) {
			propertySpinner.setEnabled(false);
			propertySpinner.setClickable(false);
			propertySpinner.setFocusable(false);
			for (int i = 0; i < properties.size(); i++) {
				if (properties.get(i).toString().equals(property_name)) {
					pos = i;
					break;
				}
			}
			propertySpinner.setSelection(pos);
		}

	}

	public void getEditTextData() {

		name = edtName.getText().toString();
		email = edtEmail.getText().toString();
		contactno = edtContactNo.getText().toString();
		propertyname = propertySpinner.getSelectedItem().toString();

	}

	public void getIntentData() {
		try {
			Intent intent = activity.getIntent();
			Bundle bundle = intent.getExtras();
			if (bundle != null) {

				property_name = bundle.getString("propertyName");
			} else {
				property_name = "activity";
				header.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void sendEmail(String name, String email, String contactno, String propertyname) {
		try {

			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			// Mime type of the attachment (or) u can use
			// sendIntent.setType("*/*")
			sendIntent.setType("text/xml");
			// Subject for the message or Email
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Name- " + name + "\nEmail- " + email + "\nContact no- "
					+ contactno + "\nProperty- " + propertyname);
			;
			sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "sales@kul.co.in" });

			final PackageManager pm = activity.getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")
						|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
			activity.startActivity(sendIntent);

		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(activity, "There in no image to send email", Toast.LENGTH_SHORT).show();

		}

	}

	private ArrayList<PropertiesDetails> getallproperties() {

		try {
			propertyList = (ArrayList<PropertiesDetails>) propertyService

			.getAllData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		try {
			super.onDestroyView();

			FragmentManager manager = getActivity().getSupportFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove(new EnquiryFragment());
			trans.commit();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		InputMethodManager imm;
		try {
			imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(edtContactNo.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(edtEmail.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(edtName.getWindowToken(), 0);
			edtEmail.clearFocus();
			edtName.clearFocus();
			edtContactNo.clearFocus();
			imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);

		} catch (Exception e) {
		}

	}

}
