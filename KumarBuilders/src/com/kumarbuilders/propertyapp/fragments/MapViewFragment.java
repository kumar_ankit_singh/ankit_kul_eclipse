package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.PropertyDetailsActivity;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;

public class MapViewFragment extends Fragment {

	private GoogleMap googleMap;
	private LatLng latLng;
	private SupportMapFragment supportMapFragment;
	private Activity activity;
	private double latitude, longLat;
	private Boolean isInternetPresent;
	private PropertyService propertyService;
	ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view_serach = null;

		try {
			view_serach = inflater.inflate(R.layout.mapfragment, container, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return view_serach;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			activity = getActivity();
			isInternetPresent = CheckNetwork.isInternetPresent(activity);
			propertyService = PropertyServiceImpl.getInstance();

			if (isInternetPresent) {

				try {
					activity = getActivity();
					getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
					supportMapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
							.findFragmentById(R.id.mapfragment);

					googleMap = supportMapFragment.getMap();

					googleMap.clear();

					for (int i = 0; i < getallproperties().size(); i++) {

						propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();

						PropertiesDetails property = propertyList.get(i);

						latitude = Double.parseDouble(property.getProperty_latitude());
						longLat = Double.parseDouble(property.getProperty_longitude());

						BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.locappicon);

						googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longLat))
								.title(property.getProperty_name()).snippet(property.getProperty_address()).icon(icon));

					}

					googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
						@Override
						public void onInfoWindowClick(Marker marker) {

							for (int i = 0; i < getallproperties().size(); i++) {

								try {
									propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
								} catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}

								PropertiesDetails property = propertyList.get(i);

								if (marker.getTitle().equals(property.getProperty_name())) {

									try {

										Intent intent = new Intent(activity, PropertyDetailsActivity.class);
										intent.putExtra("propertyId", property.getProperty_id());
										intent.putExtra("propertyName", property.getProperty_name());
										intent.putExtra("propertylat", property.getProperty_latitude());
										intent.putExtra("propertylong", property.getProperty_longitude());
										intent.putExtra("propertyLocation", property.getProperty_address());
										intent.putExtra("propertyUrl", property.getProperty_imgurl());
										intent.putExtra("description", property.getProperty_description());
										intent.putStringArrayListExtra("gallery", property.getGalleryList());
										intent.putExtra("ameneties", property.getAmenities());
										intent.putStringArrayListExtra("AminitiesList", property.getAmenitiesList());
										intent.putStringArrayListExtra("projectplan", property.getProjectPlanList());
										intent.putStringArrayListExtra("floorplan", property.getFloorPlanList());
										intent.putStringArrayListExtra("construction", property.getConstructionList());
										intent.putStringArrayListExtra("buildingplan", property.getBuildingPlanList());
										intent.putStringArrayListExtra("unitplan", property.getUnitPlanList());
										intent.putStringArrayListExtra("angular", property.getAngularList());
										intent.putExtra("specification", property.getSpecificationList());
										intent.putExtra("CostPdf", property.getPrice_pdf_url());

										startActivity(intent);
									} catch (Exception e) {

									}

								}
							}

						}

					});

					latLng = new LatLng(latitude, longLat);
					googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(21.7679, 78.8718), 4.0f));
				} catch (Exception e) {
					e.printStackTrace();

				}

			} else {
				CheckNetwork.showNetworkConnectionMessage(activity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		try {

			if (this.supportMapFragment != null
					&& getFragmentManager().findFragmentById(this.supportMapFragment.getId()) != null) {

				getFragmentManager().beginTransaction().remove(this.supportMapFragment).commit();
				this.supportMapFragment = null;
			}
		} catch (Exception e) {

		}
	}

	private ArrayList<PropertiesDetails> getallproperties() {

		try {
			propertyList = (ArrayList<PropertiesDetails>) propertyService

			.getAllData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

}
