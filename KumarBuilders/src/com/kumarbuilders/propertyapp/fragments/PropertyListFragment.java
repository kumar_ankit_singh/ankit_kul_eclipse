package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.PropertyDetailsActivity;
import com.kumarbuilders.propertyapp.adapter.PropertiesAdapter;
import com.kumarbuilders.propertyapp.databasehandler.ApplicationPreferences;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

public class PropertyListFragment extends Fragment {

	private ListView list;
	private PropertiesAdapter adapter;
	private Activity activity;
	public static List<Drawable> listDrawables = new ArrayList<Drawable>();
	// public static Property proper = new Property();
	PropertyService propertyService;
	ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();

	public PropertyListFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.properties, null);
		list = (ListView) v.findViewById(R.id.propertieslist);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {

			activity = getActivity();

			getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			new ApplicationPreferences(activity);
			// Create custom adapter for listview
			propertyService = PropertyServiceImpl.getInstance();

			adapter = new PropertiesAdapter(activity, getallproperties());

			// Set adapter to listview
			list.setAdapter(adapter);

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					try {
						propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
						PropertiesDetails property = propertyList.get(position);

						Intent intent = new Intent(activity, PropertyDetailsActivity.class);
						intent.putExtra("propertyId", property.getProperty_id());
						intent.putExtra("propertyName", property.getProperty_name());
						intent.putExtra("propertylat", property.getProperty_latitude());
						intent.putExtra("propertylong", property.getProperty_longitude());
						intent.putExtra("propertyLocation", property.getProperty_address());
						intent.putExtra("propertyUrl", property.getProperty_imgurl());
						intent.putExtra("description", property.getProperty_description());
						intent.putStringArrayListExtra("gallery", property.getGalleryList());
						intent.putExtra("ameneties", property.getAmenities());
						intent.putStringArrayListExtra("AminitiesList", property.getAmenitiesList());
						intent.putStringArrayListExtra("projectplan", property.getProjectPlanList());
						intent.putStringArrayListExtra("floorplan", property.getFloorPlanList());
						intent.putStringArrayListExtra("construction", property.getConstructionList());
						intent.putStringArrayListExtra("buildingplan", property.getBuildingPlanList());
						intent.putStringArrayListExtra("unitplan", property.getUnitPlanList());
						intent.putStringArrayListExtra("angular", property.getAngularList());
						intent.putStringArrayListExtra("specification", property.getSpecificationList());
						intent.putExtra("CostPdf", property.getPrice_pdf_url());

						startActivity(intent);
					} catch (Exception e) {

					}

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private List<PropertiesDetails> getallproperties() {

		try {
			propertyService = new PropertyServiceImpl();

			propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
			if (propertyList.size() <= 0) {

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

}
