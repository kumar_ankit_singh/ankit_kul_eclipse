package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.SpecificationAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;

public class SpecificationFragment extends Fragment {
	private Bundle extras;
	private ArrayList<String> specification;
	private ListView listView;
	private SpecificationAdapter adapter;
	private Activity activity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.specification_fragment, container, false);
		listView = (ListView) rootView.findViewById(R.id.spec_list);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		activity = getActivity();
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		Intent intent = getActivity().getIntent();
		extras = intent.getExtras();
		if (extras != null) {

			specification = extras.getStringArrayList("specification");

			adapter = new SpecificationAdapter(activity, specification);

			listView.setAdapter(adapter);
		}
	}
}
