package com.kumarbuilders.propertyapp.fragments;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewFlipper;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.PropertyDetailsActivity;
import com.kumarbuilders.propertyapp.activity.ViewLocationActivity;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyService;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.FileDownloader;
import com.kumarbuilders.propertyapp.utils.LoadMenu;
import com.kumarbuilders.propertyapp.utils.StringConstant;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

public class PropertyDetailsFragment extends Fragment implements TaskResponse, StringConstant {

	private Activity activity;
	private String propertyName, propertyLocation, propertyImage, costUrl;
	private int propertyId;
	public List<FavProperty> listfavusers = new ArrayList<FavProperty>();
	private ViewFlipper viewFlipper;
	private int i = 0;
	private TextView txtPropertyName, txtPropertyLocation, txtprobertyAbout;
	private ImageView imageViewshare, imgMap;
	private String description;
	private FragmentManager fragmentmanager;
	private Bundle bundle;
	private TextView layoutMap;
	private ToggleButton buttonFav;
	private ImageView defaultImageFlipper;
	private Button btnEnquiry, btnCostdetails;
	private ArrayList<String> imgFlipper = new ArrayList<String>();
	private FavPropertyService favPropertyService;
	int userId;
	private FavProperty favProperty;
	private String latitude, longitude;
	private UserSignUp signedInfo;
	private String kulUserId;
	public AddSignUpToDbImpl signDta;
	private ImageLoader imageLoader;
	PropertyService propertyService;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

		View v = inflater.inflate(R.layout.propertiesdetails, container, false);

		try {
			txtPropertyName = (TextView) v.findViewById(R.id.txtpropname);
			txtprobertyAbout = (TextView) v.findViewById(R.id.txtaboutproperty);
			txtPropertyLocation = (TextView) v.findViewById(R.id.txtproploc);
			imageViewshare = (ImageView) v.findViewById(R.id.imageViewshare);
			imgMap = (ImageView) v.findViewById(R.id.imgmap);
			layoutMap = (TextView) v.findViewById(R.id.txtmap);
			buttonFav = (ToggleButton) v.findViewById(R.id.btncheckfav);
			defaultImageFlipper = (ImageView) v.findViewById(R.id.imageFlipper);
			btnEnquiry = (Button) v.findViewById(R.id.btnenquiry);
			btnCostdetails = (Button) v.findViewById(R.id.btncost);
			viewFlipper = (ViewFlipper) v.findViewById(R.id.viewFlipper);

		} catch (Exception e) {
		}

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			activity = getActivity();
			PropertyDetailsActivity.txtHeader.setText("Property Details");
			imageLoader = new ImageLoader(activity.getApplicationContext());
			favProperty = new FavProperty();
			favPropertyService = new FavPropertyServiceImpl();

			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

			getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			bundle = activity.getIntent().getExtras();
			propertyId = bundle.getInt("propertyId");
			propertyName = bundle.getString("propertyName");
			propertyLocation = bundle.getString("propertyLocation");
			bundle.getString("propertyUrl");
			bundle.getString("propertyFile");
			propertyImage = bundle.getString("propertyUrl");
			description = bundle.getString("description");
			imgFlipper = bundle.getStringArrayList("gallery");
			latitude = bundle.getString("propertylat");
			longitude = bundle.getString("propertylong");
			costUrl = bundle.getString("CostPdf");

			listfavusers = favPropertyService.getAllData();

			if (listfavusers != null) {

				for (int i = 0; i < listfavusers.size(); i++) {

					try {
						favProperty = (FavProperty) favPropertyService.findById(propertyId);

						if (favProperty != null && favProperty.getPropertyName().equals(propertyName)
								&& favProperty.getProperty_id().equals(propertyId)) {
							buttonFav.setChecked(true);
							buttonFav.setBackgroundResource(R.drawable.bookornage);
						} else {
							buttonFav.setChecked(false);
							buttonFav.setBackgroundResource(R.drawable.bookwhite);
						}

					} catch (Exception e) {

						e.printStackTrace();
					}

				}
			}

			txtPropertyName.setText(propertyName);

			txtPropertyLocation.setText(propertyLocation);

			txtprobertyAbout.setText(description);

			imageViewshare.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						LoadMenu loadMenu = new LoadMenu();
						loadMenu.loadGridItems(propertyName, propertyLocation, propertyImage, activity);
					} catch (Exception e) {

					}
				}
			});
			layoutMap.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {

						showMap();

					} catch (Exception e) {
					}

				}
			});

			imgMap.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					showMap();
				}
			});
			btnEnquiry.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						PropertyDetailsActivity.txtHeader.setText("Enquiry");
						fragmentmanager = getActivity().getSupportFragmentManager();
						FragmentTransaction ft = fragmentmanager.beginTransaction();
						Fragment fragment = new EnquiryFragment();
						fragment.setArguments(bundle);
						ft.addToBackStack(null);
						ft.replace(R.id.frame_details, fragment);
						ft.commit();
					} catch (Exception e) {

					}
				}
			});

			btnCostdetails.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {

						openPdf(costUrl);

					} catch (Exception e) {

					}

				}

			});

			buttonFav.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					try {
						signDta = new AddSignUpToDbImpl();
						List<UserSignUp> signlist = signDta.getAllDetails();

						if (signlist != null && signlist.size() != 0) {

							signedInfo = new UserSignUp();
							if (signlist.size() != 0) {
								for (int i = 0; i < signlist.size(); i++) {
									signedInfo = signlist.get(i);
									kulUserId = signedInfo.getUserId();
								}
							}

							if (isChecked) {

								buttonFav.setChecked(isChecked);
								buttonFav.setBackgroundResource(R.drawable.bookornage);

								addtoFavourite();

							} else {
								buttonFav.setChecked(isChecked);
								buttonFav.setBackgroundResource(R.drawable.bookwhite);

								new AsyncHttpPost(activity, UrlConstantUtils.FAV_PROPERTY, PropertyDetailsFragment.this,
										true).execute(
												favPropertyObject(Integer.parseInt(kulUserId), propertyId, "DELETE"));

							}
							favPropertyService.createOrUpdate(favProperty);
						} else {
							// need to do the requirement in else part

						}

					} catch (Exception e) {
						e.printStackTrace();

					}

				}

			});

			try {
				if (imgFlipper != null && imgFlipper.size() > 0) {
					if (imgFlipper.get(0).equals("") || imgFlipper.isEmpty()) {

						propertyService = new PropertyServiceImpl();
						PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
						String defaultimgUrl = property.getProperty_imgurl();
						Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
						defaultImageFlipper.setImageBitmap(bmp);
					} else {

						imageFlipper(imgFlipper);
					}

				} else {

					propertyService = new PropertyServiceImpl();
					PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
					String defaultimgUrl = property.getProperty_imgurl();
					Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
					defaultImageFlipper.setImageBitmap(bmp);
				}

				do {
					viewFlipper.setAutoStart(true);
					viewFlipper.setFlipInterval(2000);
					viewFlipper.startFlipping();
					i++;
					viewFlipper.startFlipping();
				} while (i <= imgFlipper.size());
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void showMap() {
		try {
			Intent intent = new Intent(activity, ViewLocationActivity.class);
			intent.putExtra("propertyLocation", propertyLocation);
			intent.putExtra("latitude", latitude);
			intent.putExtra("longitude", longitude);
			startActivity(intent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void callCostDetails() {
		try {
			final AlertDialog alertDialog;
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
			Context mContext = activity.getApplicationContext();
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.costdetailpopup,
					(ViewGroup) (activity).findViewById(R.id.linearlayout));
			builder.setView(layout);
			alertDialog = builder.create();
			alertDialog.show();
			final Button btnContinue = (Button) layout.findViewById(R.id.btnclosecost);

			btnContinue.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					alertDialog.dismiss();

				}

			});
		} catch (Exception e) {

		}
	}

	public void imageFlipper(ArrayList<String> imgflipper) {
		try {
			ImageLoader imageLoader = new ImageLoader(activity.getApplicationContext());

			for (int i = 0; i < imgflipper.size(); i++) {
				try {
					ImageView image = new ImageView(activity.getApplicationContext());

					Bitmap bmp = imageLoader.getBitmap(imgflipper.get(i));
					image.setImageBitmap(bmp);
					if (bmp == null) {
						Bitmap bmp1 = imageLoader.getBitmap(imgflipper.get(i - 1));
						image.setImageBitmap(bmp1);
					}

					viewFlipper.addView(image);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONObject favPropertyObject(int userid, int propertyId, String method) {
		JSONObject favObject = new JSONObject();
		try {
			favObject.put("user_id", userid);
			favObject.put("property_id", propertyId);
			favObject.put("method", method);

		} catch (JSONException e) {
		}
		return favObject;
	}

	private void addtoFavourite() {
		try {

			new AsyncHttpPost(activity, UrlConstantUtils.FAV_PROPERTY, PropertyDetailsFragment.this, true)
					.execute(favPropertyObject(Integer.parseInt(kulUserId), propertyId, "POST"));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void taskResponse(String response) {
		try {
			JSONObject jsonobject = new JSONObject(response);

			String status = jsonobject.getString("STATUS");
			if (status.equals("FAVOURITE_ADDED")) {

				FavProperty favProperty = new FavProperty();

				favProperty.setPropertyimage(propertyImage);
				favProperty.setPropertyName(propertyName);
				favProperty.setPropertyLocation(propertyLocation);
				favProperty.setProperty_id(propertyId);
				listfavusers.add(favProperty);

				favPropertyService.createOrUpdate(favProperty);

			} else if (status.equals("DELETED")) {

				for (int i = 0; i < listfavusers.size(); i++) {

					if (propertyName.equals(listfavusers.get(i).getPropertyName())) {

						favProperty = (FavProperty) favPropertyService.findById(listfavusers.get(i).getProperty_id());
						favPropertyService.deleteById(favProperty.getProperty_id());

						listfavusers.remove(favProperty);
						listfavusers.remove(i);

						System.out.println("List Size=" + listfavusers.size());

					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void openPdf(String costUrl) {

		try {

			String phrase = costUrl;
			// change url production or development-
			String delims = "http://www.kul.co.in/mobile/price-details-pdf/";
			// String delims =
			// "http://dev.geekyworks.com/kul/price-details-pdf/";
			String pdfName = phrase.replace(delims, "");

			File pdfFile = new File(Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/" + pdfName);

			if (pdfFile.exists()) {
				Uri path = Uri.fromFile(pdfFile);

				Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
				pdfIntent.setDataAndType(path, "application/pdf");
				pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(pdfIntent);

			} else {
				new DownloadFile().execute(costUrl, pdfName);
				Uri path = Uri.fromFile(pdfFile);

				Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
				pdfIntent.setDataAndType(path, "application/pdf");
				pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(pdfIntent);

			}

		} catch (ActivityNotFoundException e) {
			Toast.makeText(activity, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
		}

	}

	private class DownloadFile extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... strings) {
			String fileUrl = strings[0]; // ->
											// http://maven.apache.org/maven-1.x/maven.pdf
			String fileName = strings[1]; // -> maven.pdf
			String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			File folder = new File(extStorageDirectory, "kulCostPdfFiles");
			folder.mkdir();

			File pdfFile = new File(folder, fileName);

			try {
				pdfFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileDownloader.downloadFile(fileUrl, pdfFile);
			return null;
		}
	}

	public void onBackPressed() {

		FragmentManager fragmentmanager = getActivity().getSupportFragmentManager();
		if (fragmentmanager.getBackStackEntryCount() == 0) {
			getActivity().finish();
		}

	}

}
