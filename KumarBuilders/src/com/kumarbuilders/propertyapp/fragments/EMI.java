package com.kumarbuilders.propertyapp.fragments;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.CurrencyConverterActivity;
import com.kumarbuilders.propertyapp.activity.EMICalculator;
import com.kumarbuilders.propertyapp.activity.LoanEligibilityActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class EMI extends Fragment {

	private Button mEMIbtn, mLoanBtn, mCurrencyBtn;
	private View rootview;
	private Activity activity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		activity = getActivity();
		try {
			rootview = inflater.inflate(R.layout.calculator_fragment, container, false);

			mEMIbtn = (Button) rootview.findViewById(R.id.EmiBtn);
			mLoanBtn = (Button) rootview.findViewById(R.id.loanBtn);
			mCurrencyBtn = (Button) rootview.findViewById(R.id.CurrencyBtn);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootview;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = getActivity();
		try {

			mEMIbtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(activity, EMICalculator.class);
					startActivity(intent);

				}
			});

			mLoanBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activity, LoanEligibilityActivity.class);
					startActivity(intent);

				}
			});

			mCurrencyBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activity, CurrencyConverterActivity.class);
					startActivity(intent);

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
