package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.StringConstant;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FutureRequirement extends Fragment implements StringConstant,
		TaskResponse {

	private Activity activity;
	private Spinner yearSpinner, prpt_TypeSpinner, bhkSpinner, citySpinner;
	private EditText locSpinner;
	private Button submitReq;
	private Boolean isInternetPresent;
	private View rootview;
	private ArrayList<String> timePeroid = new ArrayList<String>();
	private ArrayList<String> property_type = new ArrayList<String>();
	private ArrayList<String> BHK_type = new ArrayList<String>();
	private ArrayList<String> city_Choice = new ArrayList<String>();
	private String reqTime, property, roomTye, chosenCity, location;
	private String loggedUserId = null;
	private AddSignUpToDbImpl signDB;
	private UserSignUp signedInfo;
	private Boolean yearFlag=false,pptFlag=false,bhkFlag=false,cityFlag=false;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		try {
			activity = getActivity();
			isInternetPresent = CheckNetwork.isInternetPresent(activity);
			rootview = inflater.inflate(R.layout.requriment, container, false);

			yearSpinner = (Spinner) rootview.findViewById(R.id.reqSpinner);
			prpt_TypeSpinner = (Spinner) rootview
					.findViewById(R.id.pptTypeSpinner);
			bhkSpinner = (Spinner) rootview.findViewById(R.id.bhkSpinner);
			citySpinner = (Spinner) rootview.findViewById(R.id.citySpinner);
			locSpinner = (EditText) rootview.findViewById(R.id.prefLoc);
			submitReq = (Button) rootview.findViewById(R.id.reqSumbtn);

			requiredPeroid();
			propert_TypeReq();
			bhk_TypeReq();
			cityChoosed();

		} catch (Exception e) {
			CommonFunctions.showCustomToast(VIEW_EXCEPTION, activity);
		}

		return rootview;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			signDB = new AddSignUpToDbImpl();
			yearSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							reqTime = parent.getItemAtPosition(position)
									.toString();
							if (reqTime
									.trim()
									.equals("Please click to select when are you looking to buy ?")) {
								yearFlag = false;
							}
							else
							{
								yearFlag = true;
							}

						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							yearFlag = false;

						}
					});

			prpt_TypeSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							property = parent.getItemAtPosition(position)
									.toString();
							if (property.trim().equals(
									"Please click to select Property type")) {
								pptFlag = false;
							}else{
								pptFlag = true;
							}
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							pptFlag = false;

						}
					});
			bhkSpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {

							roomTye = parent.getItemAtPosition(position)
									.toString();
							if (roomTye.trim().equals(
									"Please click to select BHK")) {
								bhkFlag = false;
							}else{
								bhkFlag=true;
							}
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							bhkFlag = false;

						}
					});
			citySpinner
					.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							chosenCity = parent.getItemAtPosition(position)
									.toString();
							if (chosenCity.trim().equals(
									"Please click to select City")) {
								cityFlag=false;
							}else{
								cityFlag=true;
							}

						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
							cityFlag=false;

						}
					});

			submitReq.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					location = locSpinner.getText().toString();
					// CheckNetwork.showPopupMessage(activity, "In Progress");
					View view = activity.getCurrentFocus();
					if (view != null) {
						InputMethodManager imm = (InputMethodManager) activity
								.getSystemService(activity.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(view.getWindowToken(),
								InputMethodManager.HIDE_NOT_ALWAYS);
					}

					if (isInternetPresent) {
						// new AsynHttpTask on post call
						getSignedInId();
						Log.v("Requriment", " " + reqTime + " " + property
								+ " " + roomTye + " " + chosenCity + " "
								+ location);
						if (yearFlag==false|| pptFlag==false || bhkFlag==false || cityFlag==false) {
							Toast.makeText(activity,
									"Please select before placing requriement",
									Toast.LENGTH_LONG).show();
						} else {
							new AsyncHttpPost(getActivity(),
									UrlConstantUtils.FUTURE_REQUIREMENT_URL,
									FutureRequirement.this, true)
									.execute(requrimentData(loggedUserId,
											reqTime, property, roomTye,
											chosenCity, location));
						}

					} else {
						CheckNetwork.showNetworkConnectionMessage(activity);
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void requiredPeroid() {
		try {

			ArrayAdapter<String> periodAdapter = new ArrayAdapter<String>(
					activity, android.R.layout.simple_spinner_item, timePeroid) {
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View v = super.getView(position, convertView, parent);
					if (position == getCount()) {
						((TextView) v.findViewById(android.R.id.text1))
								.setText("");
						((TextView) v.findViewById(android.R.id.text1))
								.setHint(getItem(getCount())); // "Hint to be displayed"
					}
					return v;
				}

				@Override
				public int getCount() {
					return super.getCount() - 1;
				}
			};
			periodAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			timePeroid.add("3 months");
			timePeroid.add("6 months");
			timePeroid.add("1 year");
			timePeroid.add("2 years");
			timePeroid.add("3 years");
			timePeroid
					.add("Please click to select when are you looking to buy ?");
			yearSpinner.setAdapter(periodAdapter);
			yearSpinner.setSelection(periodAdapter.getCount());
			// yearSpinner.setOnItemSelectedListener((OnItemSelectedListener)
			// activity);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void propert_TypeReq() {
		try {
			ArrayAdapter<String> propertyAdapter = new ArrayAdapter<String>(
					activity, android.R.layout.simple_spinner_item,
					property_type) {
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View v = super.getView(position, convertView, parent);
					if (position == getCount()) {
						((TextView) v.findViewById(android.R.id.text1))
								.setText("");
						((TextView) v.findViewById(android.R.id.text1))
								.setHint(getItem(getCount())); // "Hint to be displayed"
					}
					return v;
				}

				@Override
				public int getCount() {
					return super.getCount() - 1;
				}
			};
			propertyAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			property_type.add("Apartment");
			property_type.add("House");
			property_type.add("Land");
			property_type.add("Commercial Property");
			property_type.add("Office Space");
			property_type.add("Villa");
			property_type.add("Bungalow");
			property_type.add("Farm House");
			property_type.add("Please click to select Property type");
			prpt_TypeSpinner.setAdapter(propertyAdapter);
			prpt_TypeSpinner.setSelection(propertyAdapter.getCount());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void bhk_TypeReq() {
		try {
			ArrayAdapter<String> bhkAdapter = new ArrayAdapter<String>(
					activity, android.R.layout.simple_spinner_item, BHK_type) {
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View v = super.getView(position, convertView, parent);
					if (position == getCount()) {
						((TextView) v.findViewById(android.R.id.text1))
								.setText("");
						((TextView) v.findViewById(android.R.id.text1))
								.setHint(getItem(getCount())); // "Hint to be displayed"
					}
					return v;
				}

				@Override
				public int getCount() {
					return super.getCount() - 1;
				}
			};
			bhkAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			BHK_type.add("1BHK");
			BHK_type.add("2BHK");
			BHK_type.add("3BHK");
			BHK_type.add("Please click to select BHK");
			bhkSpinner.setAdapter(bhkAdapter);
			bhkSpinner.setSelection(bhkAdapter.getCount());
		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
	}

	public void cityChoosed() {
		try {
			ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(
					activity, android.R.layout.simple_spinner_item, city_Choice) {
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View v = super.getView(position, convertView, parent);
					if (position == getCount()) {
						((TextView) v.findViewById(android.R.id.text1))
								.setText("");
						((TextView) v.findViewById(android.R.id.text1))
								.setHint(getItem(getCount())); // "Hint to be displayed"
					}
					return v;
				}

				@Override
				public int getCount() {
					return super.getCount() - 1;
				}
			};
			cityAdapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			city_Choice.add("Pune");
			city_Choice.add("Mumbai");
			city_Choice.add("Bangalore");
			city_Choice.add("Panchgani");
			city_Choice.add("Hyderabad");
			city_Choice.add("Please click to select City");
			citySpinner.setAdapter(cityAdapter);
			citySpinner.setSelection(cityAdapter.getCount());

		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
	}

	public void getSignedInId() {
		signedInfo = new UserSignUp();
		List<UserSignUp> list = signDB.getAllDetails();
		if (list.size() != 0) {
			for (int i = 0; i < list.size(); i++) {
				signedInfo = list.get(i);
				loggedUserId = signedInfo.getUserId();
			}
		}
	}

	public static JSONObject requrimentData(String loginid, String period,
			String property, String bhk, String user_city, String loc) {
		JSONObject requirement = new JSONObject();
		try {

			requirement.put(REQ_LOGID, loginid);
			requirement.put(REQ_DUR, period);
			requirement.put(REQ_PPT, property);
			requirement.put(REQ_BHK, bhk);
			requirement.put(REQ_CITY, user_city);
			requirement.put(REQ_LOC, loc);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return requirement;
	}

	@Override
	public void taskResponse(String response) {
		try {
			String status = null;
			if (response != null) {
				JSONObject jObject = new JSONObject(response.toString());
				status = jObject.getString(STATUS);
				if (status.trim().equals("SUCCESS")) {
					Toast.makeText(activity, "Your requirement is placed",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(activity,
							"Your requirement is not placed.Please try again",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(activity, "response is null", Toast.LENGTH_LONG)
						.show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
