package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.GalleryActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

public class PropertyFloorPlanFragment extends Fragment {

	private Button btnProjPlan, btnFloorPlan, btnBuildingPlan, btnUnitPlan, btnConstructionPlan;
	private Bundle extras;
	private ArrayList<String> projectPlan, floorPlan, buildingPlan, unitPlan, constructionPlan;
	private int propertyId;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.floorplan, container, false);
		btnProjPlan = (Button) view.findViewById(R.id.btnprojplan);
		btnFloorPlan = (Button) view.findViewById(R.id.btnfloorplan);
		btnBuildingPlan = (Button) view.findViewById(R.id.btnbuildingplan);
		btnUnitPlan = (Button) view.findViewById(R.id.btnunitplan);
		btnConstructionPlan = (Button) view.findViewById(R.id.btnconstructionplan);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		try {
			Intent intent = getActivity().getIntent();
			extras = intent.getExtras();
			if (extras != null) {

				projectPlan = extras.getStringArrayList("projectplan");
				floorPlan = extras.getStringArrayList("floorplan");// floorplan
				buildingPlan = extras.getStringArrayList("buildingplan");
				unitPlan = extras.getStringArrayList("unitplan");
				constructionPlan = extras.getStringArrayList("construction");
				propertyId = extras.getInt("propertyId");

			}

			btnProjPlan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), GalleryActivity.class);
					intent.putExtra("activityname", "projectplan");
					intent.putStringArrayListExtra("gallery", projectPlan);
					intent.putExtra("propertyId", propertyId);
					startActivity(intent);

				}
			});

			btnBuildingPlan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), GalleryActivity.class);
					intent.putExtra("activityname", "buildingplan");
					intent.putStringArrayListExtra("gallery", buildingPlan);
					intent.putExtra("propertyId", propertyId);
					startActivity(intent);
				}
			});

			btnFloorPlan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), GalleryActivity.class);
					intent.putExtra("activityname", "floorplan");
					intent.putStringArrayListExtra("gallery", floorPlan);
					intent.putExtra("propertyId", propertyId);
					startActivity(intent);
				}
			});
			btnUnitPlan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), GalleryActivity.class);
					intent.putExtra("activityname", "unitplan");
					intent.putStringArrayListExtra("gallery", unitPlan);
					intent.putExtra("propertyId", propertyId);
					startActivity(intent);
				}
			});

			btnConstructionPlan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), GalleryActivity.class);
					intent.putExtra("activityname", "constructionplan");
					intent.putStringArrayListExtra("gallery", constructionPlan);
					intent.putExtra("propertyId", propertyId);
					startActivity(intent);
				}
			});
		} catch (Exception e) {

		}
	}
}
