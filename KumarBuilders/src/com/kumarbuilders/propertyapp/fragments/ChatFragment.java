package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;

import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.packet.Message;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.ChatMessageAdapter;
import com.kumarbuilders.propertyapp.chat.ChatConnectionService;
import com.kumarbuilders.propertyapp.chat.LocalBinder;
import com.kumarbuilders.propertyapp.chat.MyXMPPReceiver;
import com.kumarbuilders.propertyapp.domain.UserMessage;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.StringConstant;

public class ChatFragment extends Fragment {

	private Activity activity;
	private ListView messageListView;
	private ImageButton sendButton;
	private LinearLayout formLayout;
	private EditText senderMessages;
	private View rootView;
	private ChatConnectionService chatService;
	private boolean isInternetConnected = false, mBounded;
	private String msg;
	private ChatMessageAdapter chatAdapter;
	private ArrayList<UserMessage> messageList = new ArrayList<UserMessage>();

	private final ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceDisconnected(ComponentName name) {
			chatService = null;
			mBounded = false;
			Log.d("TAG", "onServiceDisconnected");

		}

		@SuppressWarnings("unchecked")
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			chatService = ((LocalBinder<ChatConnectionService>) service)
					.getService();
			mBounded = true;
			Log.d("TAG", "onServiceConnected");
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		try {
			rootView = inflater.inflate(R.layout.chat, container, false);
			messageListView = (ListView) rootView
					.findViewById(R.id.msgListView);
			sendButton = (ImageButton) rootView
					.findViewById(R.id.sendMessageButton);
			formLayout = (LinearLayout) rootView.findViewById(R.id.form);
			senderMessages = (EditText) rootView
					.findViewById(R.id.messageEditText);

			/*
			 * here i have to call web service to get all chat id of sales
			 * person
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		activity = getActivity();
		try {
			/*
			 * getActivity().getFragmentManager().popBackStack(null,
			 * FragmentManager.POP_BACK_STACK_INCLUSIVE);
			 */

			UserMessage messages = new UserMessage();
			messages.setMessage("Hi,Welcome to KUL");
			messages.setDelivered(true);
			messages.setRead(true);
			messages.setTime(CommonFunctions.getCurrentTime());
			messages.setDate(CommonFunctions.getCurrentDate());
			messages.setUser(false);
			messageList.add(messages);
			// ----Set autoscroll of listview when a new message arrives----//
			messageListView
					.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
			messageListView.setStackFromBottom(true);
			chatAdapter = new ChatMessageAdapter(activity, messageList);
			messageListView.setAdapter(chatAdapter);

			// send button contain some bugs therefore for time being i have commented this code
	/*		sendButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					msg = senderMessages.getText().toString();
					if (msg != null && !msg.equals("") && msg.length() > 0) {
						UserMessage messages = new UserMessage();
						messages.setMessage(msg);
						messages.setUser(true);
						// messages.setId();
						messages.setDelivered(true);
						messages.setRead(true);
						messages.setTime(CommonFunctions.getCurrentTime());
						messages.setDate(CommonFunctions.getCurrentDate());

						isInternetConnected = CheckNetwork
								.isInternetPresent(activity);
						if (isInternetConnected
								&& MyXMPPReceiver.connection.isConnected()) {
							messages.setDelivered(true);
							Message msg1 = new Message("chatId"
									+ StringConstant.CHAT_SERVICE,
									Message.Type.chat);
							msg1.setBody(msg + "");
							try {
								MyXMPPReceiver.connection.sendPacket(msg1);
							} catch (NotConnectedException e) {
								Log.e("Chat NotConnectedException",
										"" + e.getMessage());
							}
						} else {
							messages.setDelivered(false);
							CheckNetwork
									.showAlertMessage(getActivity(),
											"Connection not available.Please Try later.");
						}
						
						 * UserCurrentMessage currentObj =
						 * UserCurrentMessage.getInstance();
						 * currentObj.getUserMessages().add(messages);
						 
						messageList.add(messages);
						messageListView
								.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
						messageListView.setStackFromBottom(true);
						chatAdapter = new ChatMessageAdapter(activity,
								messageList);
						messageListView.setAdapter(chatAdapter);

					}

				}
			});*/

		} catch (Exception e) {
			Log.e("Chat demo", "" + e.getMessage());
		}
	}

	public void sendTextMessage() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
