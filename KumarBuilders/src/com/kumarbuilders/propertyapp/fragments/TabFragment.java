package com.kumarbuilders.propertyapp.fragments;

import com.kumarbuilders.propertyapp.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class TabFragment extends BaseContainerFragment {
	private static final String TAB_1_TAG = "tab_1";
	private static final String TAB_2_TAG = "tab_2";
	private static final String TAB_3_TAG = "tab_3";

	private FragmentTabHost mTabHost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.tabfragment, null);
		mTabHost = (FragmentTabHost) v.findViewById(android.R.id.tabhost);
		mTabHost.setup(getActivity(), getChildFragmentManager(), R.id.realtabcontent);
		// mTabHost.setup(getActivity(), getChildFragmentManager());

		InitView();
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			Bundle extras = getActivity().getIntent().getExtras();
			if (extras != null) {
				Boolean flag = extras.getBoolean("filter");
				if (flag.equals(true)) {

					mTabHost.setCurrentTab(2);
				}
			}
		} catch (Exception e) {

		}

		// mTabHost.clearAllTabs();
	}

	private void InitView() {

		try {
			mTabHost.addTab(setIndicator(getActivity(), mTabHost.newTabSpec(TAB_1_TAG), R.drawable.tab_indicator_gen,
					"PROJECT LIST"), PropertyListFragment.class, null);
			mTabHost.addTab(setIndicator(getActivity(), mTabHost.newTabSpec(TAB_2_TAG), R.drawable.tab_indicator_gen,
					"MAP VIEW"), MapViewFragment.class, null);
			mTabHost.addTab(
					setIndicator(getActivity(), mTabHost.newTabSpec(TAB_3_TAG), R.drawable.tab_indicator_gen, "FILTER"),
					FilterFragment.class, null);

			LayoutParams param = (LayoutParams) mTabHost.getTabWidget().getChildAt(0).getLayoutParams();
			param.height = 60;
			mTabHost.getTabWidget().getChildAt(0).setLayoutParams(param);

			LayoutParams param1 = (LayoutParams) mTabHost.getTabWidget().getChildAt(1).getLayoutParams();
			param1.height = 60;
			mTabHost.getTabWidget().getChildAt(1).setLayoutParams(param1);

			LayoutParams param2 = (LayoutParams) mTabHost.getTabWidget().getChildAt(2).getLayoutParams();
			param1.height = 60;
			mTabHost.getTabWidget().getChildAt(2).setLayoutParams(param2);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private TabSpec setIndicator(Context ctx, TabSpec spec, int resid, String string) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.tab_item, null);
		v.setBackgroundResource(resid);
		TextView tv = (TextView) v.findViewById(R.id.txt_tabtxt);
		// ImageView img = (ImageView) v.findViewById(R.id.img_tabtxt);

		tv.setText(string);
		// img.setBackgroundResource(genresIcon);
		return spec.setIndicator(v);
	}

}
