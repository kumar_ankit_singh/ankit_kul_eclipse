package com.kumarbuilders.propertyapp.fragments;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.ComparePropertyServerListService;
import com.kumarbuilders.propertyapp.ormservices.ComparePropertyServerListServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpGET;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.FileDownloader;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

import android.app.Activity;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class TabFragmentnew extends Fragment implements TaskResponse {
	Fragment fragment = null;
	private LinearLayout llProjectlist, llMapview, llFilter;
	private Activity activity;
	private View rootView;
	Bundle extras;
	PropertyService propertyService;
	private View viewProjectlist, viewMapview, viewFilter;
	ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	String dbVersion;
	public AddSignUpToDbImpl signupData;
	private UserSignUp signUpInfo;
	private String signupUserId;
	private PropertiesDetails propertyDetails;
	private ComparePropertyServerListService comparePropertyServerListService;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			rootView = inflater.inflate(R.layout.tabfragmnetlayout, container, false);

			llProjectlist = (LinearLayout) rootView.findViewById(R.id.llprojectdetails);
			llMapview = (LinearLayout) rootView.findViewById(R.id.llmapview);
			llFilter = (LinearLayout) rootView.findViewById(R.id.llfilter);

			viewProjectlist = rootView.findViewById(R.id.viewprojectlist);
			viewMapview = rootView.findViewById(R.id.viewmapview);
			viewFilter = rootView.findViewById(R.id.viewfilter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {

			try {
				extras = getActivity().getIntent().getExtras();
				fragmentLoad(new PropertyListFragment());
				tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE);

				// Checking version to update DB-
				propertyService = new PropertyServiceImpl();

				getallproperties();

				if (getallproperties().size() == 0) {
					getallproperties();
				}
				dbVersion = propertyList.get(0).getVersion();
				new AsyncHttpGET(activity, UrlConstantUtils.GET_VERSION, TabFragmentnew.this).execute();

				//

				if (extras != null) {
					Boolean flag = extras.getBoolean("filter");
					if (flag.equals(true)) {

						fragmentLoad(new FilterFragment());
						tabViewsVisibility(View.INVISIBLE, View.INVISIBLE, View.VISIBLE);

					} else {
						fragmentLoad(new PropertyListFragment());
						tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
					}
				} else {
					fragmentLoad(new PropertyListFragment());
					tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

			llProjectlist.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						fragmentLoad(new PropertyListFragment());
						tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			llMapview.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						fragmentLoad(new MapViewFragment());
						tabViewsVisibility(View.INVISIBLE, View.VISIBLE, View.INVISIBLE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
			llFilter.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						fragmentLoad(new FilterFragment());
						tabViewsVisibility(View.INVISIBLE, View.INVISIBLE, View.VISIBLE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void tabViewsVisibility(int v1, int v2, int v3) {

		viewProjectlist.setVisibility(v1);
		viewMapview.setVisibility(v2);
		viewFilter.setVisibility(v3);

	}

	private void fragmentLoad(Fragment fragment) {

		try {

			FragmentManager fragmentmanager = getActivity().getSupportFragmentManager();
			FragmentTransaction ft = fragmentmanager.beginTransaction();
			// fragment = new TabFragment();
			fragment.setArguments(extras);
			ft.replace(R.id.mainfragment, fragment);
			ft.commit();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private List<PropertiesDetails> getallproperties() {

		try {
			propertyService = new PropertyServiceImpl();
			propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
			if (propertyList.size() <= 0) {
				// CheckNetwork.showDataNotFoundMessage(getActivity());
				// CheckNetwork.showNetworkTimeOutMessage(getActivity());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

	@Override
	public void taskResponse(String response) {
		try {
			String status = null, mResponse = null;

			if (response != null) {
				JSONObject jObject;

				jObject = new JSONObject(response);

				mResponse = jObject.getString("RESPONSE");
				status = jObject.getString("STATUS");
				if (mResponse.equals("VERSION")) {
					if (status.equals("SUCCESS")) {
						String serverVersion = jObject.getString("version");
						if (!dbVersion.equals(serverVersion)) {
							// GET PROPERTIES DATA 1 WEB SERVICE CALL
							try {
								propertyService.deleteAllData(PropertiesDetails.class);
								File pdfFile = new File(
										Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/");
								Boolean flagDeleted = CommonFunctions.deleteDirectory(pdfFile);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							new AsyncHttpGET(activity, UrlConstantUtils.GET_PROPERTIES_DATA1, TabFragmentnew.this)
									.execute();
							getPropertiesdata();

						}

					}
				} else if (mResponse.equals("PROPERTIES")) {
					if (status.equals("SUCCESS")) {
						try {
							propertyService = new PropertyServiceImpl();
							propertyDetails = new PropertiesDetails();
							String serverVersion = jObject.getString("version");
							JSONArray jsonarray;

							jsonarray = jObject.getJSONArray("data");

							for (int i = 0; i < jsonarray.length(); i++) {
								new HashMap<String, String>();

								jObject = jsonarray.getJSONObject(i);
								propertyDetails.setVersion(serverVersion);
								propertyDetails.setProperty_id(jObject.getInt("property_id"));
								propertyDetails.setProperty_name(jObject.getString("property_name"));
								propertyDetails.setProperty_description(jObject.getString("property_desc"));
								propertyDetails.setProperty_address(jObject.getString("propery_add"));
								propertyDetails.setProperty_latitude(jObject.getString("latitude"));
								propertyDetails.setProperty_longitude(jObject.getString("longitude"));
								propertyDetails.setProperty_imgurl(jObject.getString("image_url"));
								propertyDetails.setProperty_type(jObject.getString("property_type"));
								propertyDetails.setAmenities(jObject.getString("amenities"));
								propertyDetails.setUpdated_date(jObject.getString("updated_date"));
								propertyDetails.setCity(jObject.getString("city"));
								propertyDetails.setProperty_subtype(jObject.getString("property_sub_type"));
								propertyDetails.setPrice_range_max(jObject.getString("price_range_max"));
								propertyDetails.setPrice_range_min(jObject.getString("price_range_min"));
								propertyDetails.setPrice_pdf_url(jObject.getString("price_pdf_url"));

								if (jObject.getString("price_pdf_url") != null
										|| jObject.getString("price_pdf_url").length() != 0) {

									downloadPdf(jObject.getString("price_pdf_url"));
								}

								JSONArray specarray;
								ArrayList<String> speclist = new ArrayList<String>();

								specarray = jObject.getJSONArray("property_specification");

								for (int j = 0; j < specarray.length(); j++) {

									speclist.add(specarray.getString(j));

								}
								propertyDetails.setSpecificationList(speclist);

								propertyService.createOrUpdate(propertyDetails);

							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} else if (mResponse.equals("GALLERY")) {
					if (status.equals("SUCCESS")) {
						propertyDetails = new PropertiesDetails();
						JSONArray jsonarray;

						jsonarray = jObject.getJSONArray("data");
						try {

							propertyService = new PropertyServiceImpl();

						} catch (SQLException e) {
							e.printStackTrace();
						}

						for (int i = 0; i < jsonarray.length(); i++) {
							new HashMap<String, String>();
							jObject = jsonarray.getJSONObject(i);

							try {
								propertyDetails = (PropertiesDetails) propertyService
										.findById(jObject.getInt("property_id"));
							} catch (Exception e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}

							getAminities(jObject);
							getGallery(jObject);
							getProjectPlan(jObject);
							getBuildingPlan(jObject);
							getUnitPlan(jObject);
							getAngularPlan(jObject);
							getPropertyComparison(jObject);
							getfloorPlan(jObject);
							getconstructionImages(jObject);
							try {
								propertyService.createOrUpdate(propertyDetails);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}
				}
			}

		} catch (JSONException e)

		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void getPropertiesdata() {
		getLoginData();

		new AsyncHttpPost(activity, UrlConstantUtils.GET_PROPERTIES_DATA2, TabFragmentnew.this, false)
				.execute(PropertyDetailsFun(Integer.parseInt(signupUserId)));

	}

	private void getLoginData() {
		try {
			signupData = new AddSignUpToDbImpl();

			List<UserSignUp> signuplist = signupData.getAllDetails();
			if (signuplist != null && signuplist.size() != 0) {

				signUpInfo = new UserSignUp();
				if (signuplist.size() != 0) {
					for (int i = 0; i < signuplist.size(); i++) {
						signUpInfo = signuplist.get(i);
						signupUserId = signUpInfo.getUserId();
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONObject PropertyDetailsFun(int userid) {
		JSONObject userObject = new JSONObject();
		try {
			userObject.put("user_id", userid);

		} catch (JSONException e) {
		}
		return userObject;
	}

	private void getAminities(JSONObject jsonobject) {

		try {
			JSONArray amenitiesarray;
			ArrayList<String> amenitieslist = new ArrayList<String>();

			amenitiesarray = jsonobject.getJSONArray("amenities_photos");

			for (int j = 0; j < amenitiesarray.length(); j++) {

				amenitieslist.add(amenitiesarray.getString(j));

			}
			propertyDetails.setAmenitiesList(amenitieslist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getGallery(JSONObject jsonobject) {

		try {
			JSONArray galleryarray;
			ArrayList<String> gallerylist = new ArrayList<String>();

			galleryarray = jsonobject.getJSONArray("gallery_photos");

			for (int j = 0; j < galleryarray.length(); j++) {

				gallerylist.add(galleryarray.getString(j));

			}
			propertyDetails.setGalleryList(gallerylist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getProjectPlan(JSONObject jsonobject) {

		try {
			JSONArray projectplanarray;
			ArrayList<String> projectlist = new ArrayList<String>();

			projectplanarray = jsonobject.getJSONArray("project_photos");

			for (int j = 0; j < projectplanarray.length(); j++) {

				projectlist.add(projectplanarray.getString(j));

			}
			propertyDetails.setProjectPlanList(projectlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getBuildingPlan(JSONObject jsonobject) {

		try {
			JSONArray buildingplanarray;
			ArrayList<String> buildinglist = new ArrayList<String>();

			buildingplanarray = jsonobject.getJSONArray("building_photos");

			for (int j = 0; j < buildingplanarray.length(); j++) {

				buildinglist.add(buildingplanarray.getString(j));

			}
			propertyDetails.setBuildingPlanList(buildinglist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getUnitPlan(JSONObject jsonobject) {

		try {
			JSONArray unitplanarray;
			ArrayList<String> unitlist = new ArrayList<String>();

			unitplanarray = jsonobject.getJSONArray("unit_photos");

			for (int j = 0; j < unitplanarray.length(); j++) {

				unitlist.add(unitplanarray.getString(j));

			}
			propertyDetails.setUnitPlanList(unitlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getAngularPlan(JSONObject jsonobject) {

		try {
			JSONArray angularplanarray;
			ArrayList<String> angularlist = new ArrayList<String>();

			angularplanarray = jsonobject.getJSONArray("anguler_photos");

			for (int j = 0; j < angularplanarray.length(); j++) {

				angularlist.add(angularplanarray.getString(j));

			}
			propertyDetails.setAngularList(angularlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getfloorPlan(JSONObject jsonobject) {

		try {
			JSONArray floorplanarray;
			ArrayList<String> floorlist = new ArrayList<String>();

			floorplanarray = jsonobject.getJSONArray("floor_photos");

			for (int j = 0; j < floorplanarray.length(); j++) {

				floorlist.add(floorplanarray.getString(j));

			}
			propertyDetails.setFloorPlanList(floorlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getconstructionImages(JSONObject jsonobject) {

		try {
			JSONArray constructionnarray;
			ArrayList<String> constructionlist = new ArrayList<String>();

			constructionnarray = jsonobject.getJSONArray("construction_photos");

			for (int j = 0; j < constructionnarray.length(); j++) {

				constructionlist.add(constructionnarray.getString(j));

			}
			propertyDetails.setConstructionList(constructionlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getPropertyComparison(JSONObject jsonobject) {

		try {
			JSONArray comparisonnarray;
			ArrayList<String> comparisonlist = new ArrayList<String>();

			comparePropertyServerListService = new ComparePropertyServerListServiceImpl();

			comparisonnarray = jsonobject.getJSONArray("comparison_properies");

			for (int i = 0; i < comparisonnarray.length(); i++) {
				new HashMap<String, String>();
				ComparePropertyServerList compareServerList = new ComparePropertyServerList();
				JSONObject jsonobjectcompare = comparisonnarray.getJSONObject(i);

				compareServerList.setProperty_id(Integer.parseInt(jsonobjectcompare.getString("property_id")));
				compareServerList.setPropertyName(jsonobjectcompare.getString("property_name"));
				compareServerList.setKulpropertycompare_id(Integer.parseInt(jsonobject.getString("property_id")));

				comparePropertyServerListService.createOrUpdate(compareServerList);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void downloadPdf(String costUrl) {

		String phrase = costUrl;
		// change url production or development-

		String delims = "http://www.kul.co.in/mobile/price-details-pdf/";
		// String delims = "http://dev.geekyworks.com/kul/price-details-pdf/";
		String pdfName = phrase.replace(delims, "");

		File pdfFile = new File(Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/" + pdfName);

		new DownloadFile().execute(costUrl, pdfName);
		Uri path = Uri.fromFile(pdfFile);

	}

	private class DownloadFile extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... strings) {
			String fileUrl = strings[0]; // ->
											// http://maven.apache.org/maven-1.x/maven.pdf
			String fileName = strings[1]; // -> maven.pdf
			String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			File folder = new File(extStorageDirectory, "kulCostPdfFiles");
			folder.mkdir();

			File pdfFile = new File(folder, fileName);

			try {
				pdfFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileDownloader.downloadFile(fileUrl, pdfFile);
			return null;
		}
	}

}
