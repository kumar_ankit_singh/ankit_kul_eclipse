package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.List;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.GridImageAdapter;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;

import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

public class AmenitiesFragment extends Fragment {

	private GridView gv;
	private Activity activity;

	private ImageView defaultImageFlipper;
	private ViewFlipper viewFlipper;
	private int i = 0;
	private Bundle bundle;
	private String amenities;
	private ArrayList<String> imgFlipper = new ArrayList<String>();
	List<Integer> intList = new ArrayList<Integer>();
	private ImageLoader imageLoader;
	private int propertyId;
	PropertyService propertyService;
	private ArrayList<String> drawables = new ArrayList<String>();
	private ArrayList<String> AminitiesIconNameList = new ArrayList<String>();
	public ProgressBar loading_spinner;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.amenities, container, false);
		gv = (GridView) view.findViewById(R.id.gridView1);
		defaultImageFlipper = (ImageView) view.findViewById(R.id.imageFlipper);
		viewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);
		loading_spinner = (ProgressBar) view.findViewById(R.id.loading_spinneraminities);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			activity = getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
			imageLoader = new ImageLoader(activity.getApplicationContext());
			Intent intent = activity.getIntent();
			bundle = intent.getExtras();

			if (bundle != null) {

				bundle.getString("propertyName");
				propertyId = bundle.getInt("propertyId");
				imgFlipper = bundle.getStringArrayList("AminitiesList");
				amenities = bundle.getString("ameneties");
			}

			if (amenities.length() != 0) {
				getAminitiesNumber(amenities);
				gv.setAdapter(new GridImageAdapter(activity, intList));

			}

			if (imgFlipper != null && imgFlipper.size() > 0) {
				if (imgFlipper.get(0).equals("") || imgFlipper.isEmpty()) {
					loading_spinner.setVisibility(View.GONE);

					propertyService = new PropertyServiceImpl();
					PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
					String defaultimgUrl = property.getProperty_imgurl();
					Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
					defaultImageFlipper.setImageBitmap(bmp);
				} else {

					imageFlipper(imgFlipper);
				}

			} else {

				propertyService = new PropertyServiceImpl();
				PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
				String defaultimgUrl = property.getProperty_imgurl();
				Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
				defaultImageFlipper.setImageBitmap(bmp);
			}

			do {
				viewFlipper.setAutoStart(true);
				viewFlipper.setFlipInterval(2000);
				viewFlipper.startFlipping();
				i++;
				viewFlipper.startFlipping();
			} while (i <= imgFlipper.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getAminitiesNumber(String amenities) {

		String[] tokens = amenities.split("[|,]");

		for (int i = 0; i < tokens.length; i++) {
			intList.add(Integer.parseInt(tokens[i]));
		}

	}

	public void imageFlipper(ArrayList<String> imgflipper) {
		try {
			ImageLoader imageLoader = new ImageLoader(activity.getApplicationContext());
			loading_spinner.setVisibility(View.GONE);

			for (int i = 0; i < imgflipper.size(); i++) {
				try {

					ImageView image = new ImageView(activity.getApplicationContext());

					Bitmap bmp = imageLoader.getBitmap(imgflipper.get(i));
					image.setImageBitmap(bmp);
					if (bmp == null) {
						Bitmap bmp1 = imageLoader.getBitmap(imgflipper.get(i - 1));
						image.setImageBitmap(bmp1);
					}

					viewFlipper.addView(image);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
