package com.kumarbuilders.propertyapp.fragments;

import com.kumarbuilders.propertyapp.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.TabHost.TabSpec;

public class About extends BaseContainerFragment {

	private static final String TAB_1_TAG = "tab_1";
	private static final String TAB_2_TAG = "tab_2";
	private FragmentTabHost mTabHost;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.about, container, false);

		mTabHost = (FragmentTabHost) rootView
				.findViewById(android.R.id.tabhost);

		mTabHost.setup(getActivity(), getChildFragmentManager(),
				R.id.realtabcontent);

		InitView();

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			Bundle extras = getActivity().getIntent().getExtras();
			if (extras != null) {
				Boolean flag = extras.getBoolean("filter");
				if (flag.equals(true)) {

					mTabHost.setCurrentTab(2);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void InitView() {

		try {
			mTabHost.addTab(
					setIndicator(getActivity(), mTabHost.newTabSpec(TAB_1_TAG),
							R.drawable.tab_indicator_gen, "FAQ"),
					FAQFragment.class, null);
			mTabHost.addTab(
					setIndicator(getActivity(), mTabHost.newTabSpec(TAB_2_TAG),
							R.drawable.tab_indicator_gen, "About Kul"),
					AboutKulFragment.class, null);
			
			LayoutParams param = (LayoutParams) mTabHost.getTabWidget().getChildAt(0).getLayoutParams();
			param.height = 55;
			mTabHost.getTabWidget().getChildAt(0).setLayoutParams(param);
			
			LayoutParams param1 = (LayoutParams) mTabHost.getTabWidget().getChildAt(1).getLayoutParams();
			param1.height = 55;
			mTabHost.getTabWidget().getChildAt(1).setLayoutParams(param1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private TabSpec setIndicator(Context ctx, TabSpec spec, int resid,
			String string) {
		View v = LayoutInflater.from(ctx).inflate(R.layout.tab_item, null);
		v.setBackgroundResource(resid);
		TextView tv = (TextView) v.findViewById(R.id.txt_tabtxt);

		tv.setText(string);

		return spec.setIndicator(v);
	}

}
