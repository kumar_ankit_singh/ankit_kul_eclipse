package com.kumarbuilders.propertyapp.fragments;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.LaunchingActivity;
import com.kumarbuilders.propertyapp.activity.PropertyDetailsActivity;
import com.kumarbuilders.propertyapp.adapter.FavPropertiesAdapter;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyService;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

public class CustomerFavPropertyFragment extends Fragment {

	private ListView list;
	public FavPropertiesAdapter adapter;
	private Activity activity;
	public List<FavProperty> listfavusers = new ArrayList<FavProperty>();
	public List<FavProperty> favUserList = new ArrayList<FavProperty>();
	private PropertyService propertyService;
	private FavPropertyService favpropertyService;
	private ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	private TextView txtMessage;

	private View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		try {
			v = LayoutInflater.from(getActivity()).inflate(R.layout.customerfav, null);
			txtMessage = (TextView) v.findViewById(R.id.txtmessage);

			list = (ListView) v.findViewById(R.id.favpropertieslist);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {
			activity = getActivity();
			activity.getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Favourite" + "</font>"));

			getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			favpropertyService = FavPropertyServiceImpl.getInstance();
			propertyService = PropertyServiceImpl.getInstance();
			// Create custom adapter for listview

			// internet present
			try {
				listfavusers.clear();
				listfavusers = (List<FavProperty>) favpropertyService.getAllData();
				if (listfavusers.size() != 0) {

					for (int i = 0; i < listfavusers.size(); i++) {
						if (listfavusers.get(i).getPropertyName() != null) {
							FavProperty property = (FavProperty) favpropertyService
									.findById(listfavusers.get(i).getProperty_id());
							favUserList.add(property);
							adapter = new FavPropertiesAdapter(activity, favUserList);
							list.setAdapter(adapter);
						}

					}

				} else {
					txtMessage.setVisibility(View.VISIBLE);
					list.setVisibility(View.GONE);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					try {
						new FavProperty();

						propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();

						PropertiesDetails property = (PropertiesDetails) propertyService
								.findById(favUserList.get(position).getProperty_id());

						Intent intent = new Intent(activity, PropertyDetailsActivity.class);
						intent.putExtra("propertyId", property.getProperty_id());
						intent.putExtra("propertyName", property.getProperty_name());
						intent.putExtra("propertylat", property.getProperty_latitude());
						intent.putExtra("propertylong", property.getProperty_longitude());
						intent.putExtra("propertyLocation", property.getProperty_address());
						intent.putExtra("propertyUrl", property.getProperty_imgurl());
						intent.putExtra("description", property.getProperty_description());
						intent.putStringArrayListExtra("gallery", property.getGalleryList());
						intent.putExtra("ameneties", property.getAmenities());
						intent.putStringArrayListExtra("AminitiesList", property.getAmenitiesList());
						intent.putStringArrayListExtra("projectplan", property.getProjectPlanList());
						intent.putStringArrayListExtra("floorplan", property.getFloorPlanList());
						intent.putStringArrayListExtra("construction", property.getConstructionList());
						intent.putStringArrayListExtra("buildingplan", property.getBuildingPlanList());
						intent.putStringArrayListExtra("unitplan", property.getUnitPlanList());
						intent.putStringArrayListExtra("angular", property.getAngularList());
						intent.putExtra("specification", property.getSpecificationList());
						intent.putExtra("CostPdf", property.getPrice_pdf_url());
						intent.putExtra("favlist", true);

						startActivity(intent);
						getActivity().finish();

					} catch (Exception e) {

					}

				}
			});
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public static JSONObject favPropertyObject(int userid, String method) {
		JSONObject favObject = new JSONObject();
		try {
			favObject.put("user_id", userid);
			favObject.put("method", method);

		} catch (JSONException e) {
		}
		return favObject;
	}

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		try {
			super.onDestroyView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
