package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.FilterOptionsActivity;
import com.kumarbuilders.propertyapp.activity.FilterPropertyActivity;
import com.kumarbuilders.propertyapp.activity.WheelActivity;
import com.kumarbuilders.propertyapp.adapter.FilterListAdapter;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;

public class FilterFragment extends Fragment {

	private ListView list;
	private FilterListAdapter adapter;
	private Activity activity;
	private ArrayList<String> Filterlist = new ArrayList<String>();
	private String select_item = "";
	private String list_item;
	private Button btnFilter;
	private PropertyService propertyService;
	private PropertyServiceImpl propertyServiceImpl;
	private Bundle extras;
	public static ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	private static String houseType = "Any", maxPrice = "20 lac-05 Cr", minArea = "", lastUpdate = "", city = "Any", amenities = "",
			minbedrooms = "";

	public FilterFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = LayoutInflater.from(getActivity()).inflate(R.layout.filter, null);
		list = (ListView) v.findViewById(R.id.propertieslist);
		btnFilter = (Button) v.findViewById(R.id.btnfilter);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		try {

			activity = getActivity();
			getActivity().getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

			try {
				Bundle extras = activity.getIntent().getExtras();

				if (extras != null) {
					select_item = extras.getString("list_selected_item");
					list_item = extras.getString("filterlistitem");

					if (select_item == null && list_item == null) {
						adapter = new FilterListAdapter(activity, getAllFilterListContent(), "", "");
					} else {

						if (list_item.equals("House Type")) {
							houseType = select_item;
						} else if (list_item.equals("Min-Max Price")) {
							maxPrice = select_item;
						} else if (list_item.equals("Bedrooms")) {

							minbedrooms = select_item;
						} else if (list_item.equals("Area")) {
							minArea = select_item;
						} else if (list_item.equals("Last Updated")) {
							lastUpdate = select_item;
						} else if (list_item.equals("Cities,Towns and Location")) {
							city = select_item;
						} else if (list_item.equals("Amenities")) {
							amenities = select_item;
						}
						extras.getInt("position");

						adapter = new FilterListAdapter(activity, getAllFilterListContent(), select_item, list_item);
						adapter.notifyDataSetChanged();
					}

				} else {
					adapter = new FilterListAdapter(activity, getAllFilterListContent(), "", list_item);
				}

			} catch (Exception e1) {
				e1.printStackTrace();

			}

			// Set adapter to listview
			list.setAdapter(adapter);

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					try {
						String select_item = Filterlist.get(position).toString();

						if (select_item.equals("Min-Max Price") || select_item.equals("Bedrooms")
								|| select_item.equals("Area")) {
							Intent intent = new Intent(getActivity(), WheelActivity.class);
							intent.putStringArrayListExtra("listitem", Filterlist);
							intent.putExtra("filterlistitem", Filterlist.get(position).toString());
							intent.putExtra("position", position);
							startActivity(intent);
							activity.finish();

						} else {

							Intent intent = new Intent(getActivity(), FilterOptionsActivity.class);
							intent.putStringArrayListExtra("listitem", Filterlist);
							intent.putExtra("filterlistitem", Filterlist.get(position).toString());
							intent.putExtra("position", position);
							startActivity(intent);
							activity.finish();
						}
					} catch (Exception e) {
						e.printStackTrace();

					}

				}
			});

			btnFilter.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {
						propertyService = propertyServiceImpl.getInstance();
						new PropertiesDetails();
						propertyList.clear();
//
						propertyList = (ArrayList<PropertiesDetails>) propertyService.getPropertyByCondition(amenities,
								lastUpdate, city, maxPrice, minArea, houseType, getActivity());

						if (propertyList.size() > 0) {
							Intent intent = new Intent(getActivity(), FilterPropertyActivity.class);
							intent.putExtra("city", city);
							intent.putExtra("amenities", amenities);
							intent.putExtra("lastUpdate", lastUpdate);
							intent.putExtra("maxPrice", maxPrice);
							intent.putExtra("minArea", minArea);
							intent.putExtra("houseType", houseType);
							intent.putExtra("allList", "none");
							startActivity(intent);
						} else {
							CheckNetwork.showDataNotFoundMessage(getActivity());
						}

					} catch (Exception e) {

						if (extras == null) {
							Intent intent = new Intent(getActivity(), FilterPropertyActivity.class);
							intent.putExtra("allList", "all");
							startActivity(intent);

						} else {
							Toast.makeText(activity, "Property not Found", Toast.LENGTH_SHORT).show();
						}
					}

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private List<String> getAllFilterListContent() {

		Filterlist.add("House Type");
		Filterlist.add("Min-Max Price");
		Filterlist.add("Bedrooms");
		Filterlist.add("Area");
		Filterlist.add("Last Updated");
		Filterlist.add("Cities,Towns and Location");
		Filterlist.add("Amenities");
		return Filterlist;

	}

}
