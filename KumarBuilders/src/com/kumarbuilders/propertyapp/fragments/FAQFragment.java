package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.ExpandableListAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

public class FAQFragment extends Fragment {

	private ExpandableListAdapter adapter;
	private ExpandableListView lstView;
	private List<String> headTitle;
	private HashMap<String, List<String>> childTitle;
	private Activity activity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		activity = getActivity();
		View rootView = inflater.inflate(R.layout.faq_fragment, container, false);
		lstView = (ExpandableListView) rootView.findViewById(R.id.expLV);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		try {
			kulFAQList();
			adapter = new ExpandableListAdapter(activity, headTitle, childTitle);
			lstView.setAdapter(adapter);
			// lstView.setIndicatorBounds(lstView.getWidth()-25,lstView.getWidth()
			// );
		} catch (Exception e) {

		}
	}

	public void kulFAQList() {

		headTitle = new ArrayList<String>();
		childTitle = new HashMap<String, List<String>>();

		headTitle.add("What is the difference between the Lease and the Leave and the License agreement?");
		headTitle.add(" I have a flat which i want to sell and buy a new flat bigger in area.what are my "
				+ "tax implications with regard to capital gains?");
		headTitle.add("How does purchasing a home compare with renting?");

		List<String> Question1 = new ArrayList<String>();
		Question1.add("A Lease, defined under Section 105 of The Transfer of Property Act, 1882,"
				+ " is a transfer of the right to enjoy the concerned property for a pre-defined "
				+ "time period or in perpetuity. The lessor (owner of the property) gives "
				+ "the lessee (the one leasing the property) such consideration periodically, usually "
				+ "at the beginning or end of a lease agreement. License is defined in Section 52 of "
				+ "the Indian Easements Act,1882. License does not allow any interest in the premises"
				+ " on the licensee's part. It merely gives the licensee the right to use and occupy "
				+ "the premises for a limited duration. A lease deed needs to be stamped and registered."
				+ " The amount payable towards the lease deed's stamp duty is more than that payable "
				+ "towards the Leave and License's. For a period exceeding three years, the stamp duty "
				+ "is same for both agreements. ");

		List<String> Question2 = new ArrayList<String>();
		Question2.add("If you purchase a new flat within two years of the date of sale of the "
				+ "original flat and invest the entire amount of capital gained into the new flat, "
				+ "you will not have to pay any capital gains tax. ");

		List<String> Question3 = new ArrayList<String>();
		Question3.add("The two don't really compare at all. The one advantage of renting is being "
				+ "generally free of most maintenance responsibilities. But by renting, you lose the "
				+ "chance to build equity, take advantage of tax benefits, and protect yourself against "
				+ "rent increases. Also, you may not be free to decorate without permission and may be at "
				+ "the mercy of the landlord for housing.Buying a home gives the freedom, stability, and "
				+ "security of owning your own home, they are worth it. ");

		childTitle.put(headTitle.get(0), Question1);
		childTitle.put(headTitle.get(1), Question2);
		childTitle.put(headTitle.get(2), Question3);
	}
}
