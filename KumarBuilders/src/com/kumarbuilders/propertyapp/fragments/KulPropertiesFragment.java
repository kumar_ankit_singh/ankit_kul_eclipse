package com.kumarbuilders.propertyapp.fragments;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class KulPropertiesFragment extends Fragment {

	private View rootView;
	private TextView mkulText;
	String property_type;
	Integer property_id;
	private PropertiesDetails property;
	private PropertyService propertyService;

	// this is a parameterize constructor
	public KulPropertiesFragment(Integer prop_id, String prop_type) {
		property_id = prop_id;
		property_type = prop_type;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {
			rootView = (ViewGroup) inflater.inflate(R.layout.kul_property_fragment, container, false);
			mkulText = (TextView) rootView.findViewById(R.id.kulPpt);

			propertyService = new PropertyServiceImpl();
			property = (PropertiesDetails) propertyService.findById(property_id);
			// Here we are checking type of property selected by user to view.

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rootView;
	}

	// this method is used to set string value in string array which will be set
	// on
	// view.By passing two parameter And delimited by # as a delimiter.
	private void setTextValues(String value, TextView view) {

		if (value.equals("") || value.equals("no description") || value.length() == 0) {
			view.setText("No Description");
		} else {
			String[] token = value.split("[#]+");
			for (int i = 1; i < token.length; i++) {

				view.append(i + ". " + token[i] + "\n\n");
				System.out.println(token[i]);
			}
		}
	}

}
