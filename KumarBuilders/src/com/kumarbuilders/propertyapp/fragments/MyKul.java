package com.kumarbuilders.propertyapp.fragments;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.databasehandler.ApplicationPreferences;
import com.kumarbuilders.propertyapp.domain.UserMyKul;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDb;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.CustomValidators;
import com.kumarbuilders.propertyapp.utils.StringConstant;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class MyKul extends Fragment implements StringConstant, TaskResponse {

	private Button saveButton, editEmail, editPhone, editCity;
	private EditText eMail, ePhone, eCity;
	private ImageView userImage;
	private ScrollView scrol1;
	private Activity activity;
	final int CAMERA_CAPTURE = 1;
	final int CROP_PIC = 2;
	private static final int SELECT_PICTURE = 3;
	private Uri picUri;
	private View rootview;
	private String selectedImagePath;
	private PopupMenu cameraMenu;
	public static final String pEmail = "emailKey";
	public static final String pPhone = "phoneKey";
	public static final String pCity = "cityKey";
	private String emailId;
	private String phoneNo;
	private String cityName;
	private String changedPwd, prefEmail, prefPhone, prefCity, signupUserId = null, kulUserId = null;
	private TextView mailimage, cityimage, phoneimage;
	private String kEmail, kPhone, kCity;
	private UserSignUp signInfo;
	private JSONObject jObject;
	private Boolean isInternetPresent;
	private Typeface face;
	private byte[] profPhoto;
	private byte[] byteArray;
	public AddSignUpToDb signUpData;
	public UserSignUp userdata;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		try {

			activity = getActivity();
			isInternetPresent = CheckNetwork.isInternetPresent(activity);
			rootview = inflater.inflate(R.layout.my_kul, container, false);

			saveButton = (Button) rootview.findViewById(R.id.sveBtn);
			editEmail = (Button) rootview.findViewById(R.id.editBtn1);
			editPhone = (Button) rootview.findViewById(R.id.editBtn2);
			editCity = (Button) rootview.findViewById(R.id.editBtn3);

			eMail = (EditText) rootview.findViewById(R.id.mailEdt);
			ePhone = (EditText) rootview.findViewById(R.id.phoneEdit);
			eCity = (EditText) rootview.findViewById(R.id.cityEdit);

			mailimage = (TextView) rootview.findViewById(R.id.mailView);
			cityimage = (TextView) rootview.findViewById(R.id.mapImg);
			phoneimage = (TextView) rootview.findViewById(R.id.phView);

			face = Typeface.createFromAsset(activity.getAssets(), "HealthAssist.ttf");
			mailimage.setTypeface(face);
			cityimage.setTypeface(face);
			phoneimage.setTypeface(face);

			userImage = (ImageView) rootview.findViewById(R.id.userImg);

			scrol1 = (ScrollView) rootview.findViewById(R.id.scrlVw);

			cameraMenu = new PopupMenu(activity, userImage);
			cameraMenu.inflate(R.menu.camera_option);

		} catch (Exception e) {
			CommonFunctions.showCustomToast(VIEW_EXCEPTION, activity);
		}
		return rootview;

	}

	@Override
	public void onResume() {
		super.onResume();
		// getPreferencesValue();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			activity = getActivity();

			signUpData = new AddSignUpToDbImpl();
			getmyKulInfo();
			emailFunction();
			cityFunction();
			phoneFunction();

			saveButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					InputMethodManager imm = (InputMethodManager) activity
							.getSystemService(Activity.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
					saveDetails();
				}
			});

			userImage.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						cameraMenu.show();
					} catch (Exception e) {

					}

				}
			});

			cameraMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
				@Override
				public boolean onMenuItemClick(MenuItem item) {
					switch (item.getItemId()) {
					case R.id.openCamera:
						openCameraInSignUp();
						break;
					case R.id.openGallery:
						openGalleryInSignUp();
						break;

					}
					return false;
				}
			});

		} catch (IllegalStateException e) {
			CommonFunctions.showCustomToast(ILLEGAL_EXC, activity);
		} catch (Exception e) {
			CommonFunctions.showCustomToast(SUPER_EXCEPTION, activity);
		}
	}

	@Override
	public void onDestroyView() {
		try {
			super.onDestroyView();
			FragmentManager manager = getActivity().getSupportFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove(new MyKul());
			trans.commit();
			// manager.popBackStack();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// user defined function to set edit text not editable
	public void fieldDisabled() {
		eMail.setEnabled(false);
		ePhone.setEnabled(false);
		eCity.setEnabled(false);
	}

	// user function
	private void saveDetails() {
		try {
			if (isInternetPresent) {
				signInfo = new UserSignUp();
				List<UserSignUp> list = signUpData.getAllDetails();
				for (int i = 0; i < list.size(); i++) {
					signInfo = list.get(i);
					signupUserId = signInfo.getUserId();
				}
				emailId = eMail.getText().toString();
				phoneNo = ePhone.getText().toString();
				cityName = eCity.getText().toString();
				CustomValidators valdition = new CustomValidators();
				if (!valdition.isValidEmail(emailId)) {
					Toast.makeText(getActivity(), "please check mail address", Toast.LENGTH_LONG).show();
					eMail.setText("");
				} else if (!valdition.isValidPhone(phoneNo)) {
					Toast.makeText(getActivity(), "please enter only 10 digits", Toast.LENGTH_LONG).show();
					ePhone.setText("");
				} else if (!valdition.isValidName(cityName)) {
					Toast.makeText(getActivity(), "Enter only alphabets", Toast.LENGTH_LONG).show();
					eCity.setText("");
				} else {
					// fieldDisabled();
					new AsyncHttpPost(activity, UrlConstantUtils.MYKUL_URL, MyKul.this, true)
							.execute(postKulInfo(signupUserId, StringConstant.POST_METHOD, cityName, phoneNo, emailId));

				}
			} else {
				CheckNetwork.showNetworkConnectionMessage(activity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// mail function implementation
	private void emailFunction() {
		try {
			eMail.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					String stateString = eMail.getText().toString();
					if (!stateString.trim().equals("")) {

						eMail.setError(null);
						editEmail.setVisibility(View.VISIBLE);
					} else {
						editEmail.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			eMail.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_NEXT) {
						eMail.setFocusable(true);
					}
					return false;
				}
			});

			editEmail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					eMail.setText(" ");

				}
			});
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	// phone function implementation
	private void phoneFunction() {
		try {
			ePhone.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					String stateString = ePhone.getText().toString();
					if (!stateString.trim().equals("")) {
						ePhone.setError(null);
						editPhone.setVisibility(View.VISIBLE);
					} else {
						editPhone.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			editPhone.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					ePhone.setText(" ");
				}
			});
		} catch (Exception e) {

		}

	}

	// City function implementation
	private void cityFunction() {
		try {

			eCity.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					String stateString = eCity.getText().toString();
					if (!stateString.trim().equals("")) {
						eCity.setError(null);
						editCity.setVisibility(View.VISIBLE);
					} else {
						editCity.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			editCity.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					eCity.setText("");

				}
			});

			editCity.setOnEditorActionListener(new OnEditorActionListener() {

				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
					if (actionId == EditorInfo.IME_ACTION_GO) {
						saveDetails();
					}
					return false;
				}
			});
		} catch (Exception e) {

		}
	}

	public void getPreferencesValue() {
		try {
			ApplicationPreferences appP = new ApplicationPreferences(activity);
			prefEmail = appP.getPreference(pEmail, "");
			prefPhone = appP.getPreference(pPhone, "");
			prefCity = appP.getPreference(pCity, "");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setPreferenceValue() {
		try {
			ApplicationPreferences setPref = new ApplicationPreferences(activity);
			String email = eMail.getText().toString();
			String phone = ePhone.getText().toString();
			String city = eCity.getText().toString();
			Log.v("data", "" + email + " " + phone + " " + city);
			setPref.savePreference(pEmail, email);
			setPref.savePreference(pPhone, phone);
			setPref.savePreference(pCity, city);
			setPref.savePreference("flag", "store");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void getmyKulInfo() {
		// String logUserId = null,kulUserId = null;
		try {
			signInfo = new UserSignUp();
			List<UserSignUp> signList = signUpData.getAllDetails();
			for (int i = 0; i < signList.size(); i++) {
				signInfo = signList.get(i);
				signupUserId = signInfo.getUserId();
			}
			UserSignUp signupUser = signUpData.findById(Integer.parseInt(signupUserId));
			kEmail = signupUser.getEmailId();
			kPhone = signupUser.getPhone();
			kCity = signupUser.getCity();
			profPhoto = signupUser.getUserPhoto();
			setmyKulInfo();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setmyKulInfo() {
		try {
			eMail.setText(kEmail);
			eCity.setText(kCity);
			ePhone.setText(kPhone);

			if (profPhoto != null || profPhoto.length == 0) {
				Bitmap bmp = BitmapFactory.decodeByteArray(profPhoto, 0, profPhoto.length);
				userImage.setImageBitmap(bmp);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static JSONObject postKulInfo(String uId, String postMethod, String mCity, String mPhone, String mail) {
		JSONObject postKulObj = new JSONObject();
		try {
			postKulObj.put(KUL_USERID, uId);
			postKulObj.put(METHOD, postMethod);
			postKulObj.put(KUL_CITY, mCity);
			postKulObj.put(KUL_PHONE, mPhone);
			postKulObj.put(KUL_POST_EMAIL, mail);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return postKulObj;
	}

	@Override
	public void taskResponse(String response) {
		String status = null;
		String methodType = null;
		if (response != null) {
			try {
				jObject = new JSONObject(response.toString());
				status = jObject.getString(STATUS);
				methodType = jObject.getString(METHOD_TYPE);
				if (status.trim().equals("SUCCESS")) {
					if (methodType.trim().equals("GET")) {
						JSONArray jArray = jObject.getJSONArray("data");
						for (int i = 0; i < jArray.length(); i++) {
							new UserMyKul();
							JSONObject kulObject = new JSONObject();
							kulObject = jArray.getJSONObject(i);
							// kUserid = kulObject.getString(KUL_USERID);
							kEmail = kulObject.getString(KUL_MAIL);
							kCity = kulObject.getString(KUL_CITY);
							kPhone = kulObject.getString(KUL_PHONE);
						}
						// setmyKulInfo();
					} else if (methodType.trim().equals("POST")) {
						setSignUpInfo();
						Toast.makeText(getActivity(), "Information saved successfully", Toast.LENGTH_LONG).show();
					} else {

					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			CheckNetwork.showNetworkConnectionMessage(activity);
		}

	}

	public void openCameraInSignUp() {

		try {
			// use standard intent to capture an image
			Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			// we will handle the returned data in onActivityResult
			startActivityForResult(captureIntent, CAMERA_CAPTURE);
		} catch (ActivityNotFoundException anfe) {
			CommonFunctions.showCustomToast(IMAG_CROP, activity);
		}
	}

	public void openGalleryInSignUp() {
		try {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(intent, SELECT_PICTURE);
		} catch (ActivityNotFoundException anfe) {
			CommonFunctions.showCustomToast(IMAG_CROP, activity);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		try {
			if (data != null) {
				if (resultCode == activity.RESULT_OK) {
					if (requestCode == CAMERA_CAPTURE) {
						picUri = data.getData();
						// Bitmap photo;
						// photo = decodeSampledBitmapFromUri(picUri,
						// userImage.getWidth(), userImage.getHeight());

						Bitmap photo = (Bitmap) data.getExtras().get("data");
						userImage.setImageBitmap(photo);
						insertOrUpdateProfilePic(photo);

					}
					// user is returning from cropping the image
					else if (requestCode == CROP_PIC) {
						// get the returned data
						Bundle extras = data.getExtras();
						// get the cropped bitmap
						Bitmap profilePic = extras.getParcelable("data");

						userImage.setImageBitmap(profilePic);
						insertOrUpdateProfilePic(profilePic);

					} else if (requestCode == SELECT_PICTURE) {
						Uri selectedImageUri = data.getData();
						// getPath(selectedImageUri);
						Bitmap bm;
						bm = decodeSampledBitmapFromUri(selectedImageUri, userImage.getWidth(), userImage.getHeight());
						userImage.setImageBitmap(bm);
						// userImage.setImageURI(selectedImageUri);
						// Bitmap bm = BitmapFactory
						// .decodeStream(getActivity().getContentResolver().openInputStream(selectedImageUri));
						insertOrUpdateProfilePic(bm);
					}
				}
			} else {
				CommonFunctions.showCustomToast(IMG_DATA, activity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	private boolean insertOrUpdateProfilePic(Bitmap photo) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
		profPhoto = stream.toByteArray();

		return false;
	}

	public void setSignUpInfo() {
		try {
			signUpData = new AddSignUpToDbImpl();
			userdata = new UserSignUp();
			userdata = signUpData.findById(Integer.parseInt(signupUserId));
			userdata.setCity(cityName);
			userdata.setEmailId(emailId);
			userdata.setPhone(phoneNo);
			userdata.setUserPhoto(profPhoto);
			signUpData.createOrUpdate(userdata);
			List<UserSignUp> sList = signUpData.getAllDetails();
			if (sList.size() != 0) {
				for (int j = 0; j < sList.size(); j++) {
					userdata = sList.get(j);
					String userId = userdata.getUserId();
					String cityname = userdata.getCity();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight) {
		Bitmap bm = null;
		try {
			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(uri), null, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bm = BitmapFactory.decodeStream(activity.getContentResolver().openInputStream(uri), null, options);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Toast.makeText(activity, e.toString(), Toast.LENGTH_LONG).show();
		} catch (Exception e) {

		}
		return bm;
	}
}
