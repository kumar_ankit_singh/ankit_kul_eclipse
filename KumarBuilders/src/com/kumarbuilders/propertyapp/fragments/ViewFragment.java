package com.kumarbuilders.propertyapp.fragments;

import java.util.ArrayList;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ViewFlipper;

public class ViewFragment extends Fragment {

	Activity activity;
	private ImageView defaultImageFlipper;
	private ViewFlipper viewFlipper;
	private int count, i = 0;
	private Bundle bundle;
	private String propertyName;
	private ArrayList<String> imgFlipper = new ArrayList<String>();
	private int propertyId;
	PropertyService propertyService;
	private ImageLoader imageLoader;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.viewfragment, container, false);

		viewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);
		defaultImageFlipper = (ImageView) view.findViewById(R.id.imageFlipper);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = getActivity();
		InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
		try {
			Intent intent = activity.getIntent();
			bundle = intent.getExtras();
			propertyName = bundle.getString("propertyName");
			imgFlipper = bundle.getStringArrayList("angular");
			propertyId = bundle.getInt("propertyId");

			imageLoader = new ImageLoader(activity.getApplicationContext());

			if (imgFlipper != null && imgFlipper.size() > 0) {
				if (imgFlipper.get(0).equals("") || imgFlipper.isEmpty()) {

					propertyService = new PropertyServiceImpl();
					PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
					String defaultimgUrl = property.getProperty_imgurl();
					Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
					defaultImageFlipper.setImageBitmap(bmp);
				} else {

					imageFlipper(imgFlipper);
				}

			} else {

				propertyService = new PropertyServiceImpl();
				PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
				String defaultimgUrl = property.getProperty_imgurl();
				Bitmap bmp = imageLoader.getBitmap(defaultimgUrl);
				defaultImageFlipper.setImageBitmap(bmp);
			}

			do {
				viewFlipper.setAutoStart(true);
				viewFlipper.setFlipInterval(2000);
				viewFlipper.startFlipping();
				i++;
				viewFlipper.startFlipping();
			} while (i <= count);
		} catch (Exception e) {

		}
	}

	public void imageFlipper(ArrayList<String> imgflipper) {
		ImageLoader imageLoader = new ImageLoader(activity.getApplicationContext());

		for (int i = 0; i < imgflipper.size(); i++) {
			try {
				ImageView image = new ImageView(activity.getApplicationContext());

				Bitmap bmp = imageLoader.getBitmap(imgflipper.get(i));
				image.setImageBitmap(bmp);

				viewFlipper.addView(image);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
