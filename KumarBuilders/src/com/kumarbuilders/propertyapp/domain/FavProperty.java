package com.kumarbuilders.propertyapp.domain;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class FavProperty implements Serializable, Parcelable {

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
	private Integer property_id;

	@DatabaseField
	private String userId;

	@DatabaseField
	private String property_description;
	@DatabaseField
	private String propertyimage;
	@DatabaseField
	private String propertyLocation;
	@DatabaseField
	private String propertyName;

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyimage() {
		return propertyimage;
	}

	public void setPropertyimage(String propertyimage) {
		this.propertyimage = propertyimage;
	}

	public String getPropertyLocation() {
		return propertyLocation;
	}

	public void setPropertyLocation(String propertyLocation) {
		this.propertyLocation = propertyLocation;
	}

	public Integer getProperty_id() {
		return property_id;
	}

	public void setProperty_id(Integer property_id) {
		this.property_id = property_id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProperty_description() {
		return property_description;
	}

	public void setProperty_description(String property_description) {
		this.property_description = property_description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}
}