package com.kumarbuilders.propertyapp.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class UserSignUp {


	@DatabaseField(id=true)
	private String userId;
	@DatabaseField
	private String username;
	@DatabaseField
	private String emailId;
	@DatabaseField
	private String city;
	@DatabaseField
	private String phone;
	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] userPhoto;
	
	private static UserSignUp userInfo;
	
	public static UserSignUp getInstance(){
		if(userInfo == null){
			userInfo = new UserSignUp();
		}
		return userInfo;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public byte[] getUserPhoto() {
		return userPhoto;
	}

	public void setUserPhoto(byte[] userPhoto) {
		this.userPhoto = userPhoto;
	}

	public static UserSignUp getUserInfo() {
		return userInfo;
	}

	public static void setUserInfo(UserSignUp userInfo) {
		UserSignUp.userInfo = userInfo;
	}

	
	
}
