package com.kumarbuilders.propertyapp.domain;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class PropertyCompare implements Serializable, Parcelable {

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
	private Integer property_id;

	@DatabaseField
	private String propertyName;

	@DatabaseField
	private String project_name;

	@DatabaseField
	private String visibility;

	@DatabaseField
	private String hospital;

	@DatabaseField
	private String school;

	@DatabaseField
	private String public_transport;

	@DatabaseField
	private String grocery_market;

	@DatabaseField
	private String developers;

	@DatabaseField
	private String flat_area;

	@DatabaseField
	private String rate_per_sqrt_foot;

	@DatabaseField
	private String floor_rise;

	@DatabaseField
	private String loading;

	@DatabaseField
	private String carpet_area;

	@DatabaseField
	private Integer propertycompare_id;

	@DatabaseField
	private String KulpropertyName;

	@DatabaseField
	private String apartment_type;
	@DatabaseField
	private String bank_approval;

	@DatabaseField
	private String property_type;

	public String getProperty_type() {
		return property_type;
	}

	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}

	public String getApartment_type() {
		return apartment_type;
	}

	public void setApartment_type(String apartment_type) {
		this.apartment_type = apartment_type;
	}

	public String getBank_approval() {
		return bank_approval;
	}

	public void setBank_approval(String bank_approval) {
		this.bank_approval = bank_approval;
	}

	public Integer getProperty_id() {
		return property_id;
	}

	public void setProperty_id(Integer property_id) {
		this.property_id = property_id;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public String getVisibility() {
		return visibility;
	}

	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getPublic_transport() {
		return public_transport;
	}

	public void setPublic_transport(String public_transport) {
		this.public_transport = public_transport;
	}

	public String getGrocery_market() {
		return grocery_market;
	}

	public void setGrocery_market(String grocery_market) {
		this.grocery_market = grocery_market;
	}

	public String getFlat_area() {
		return flat_area;
	}

	public void setFlat_area(String flat_area) {
		this.flat_area = flat_area;
	}

	public String getCarpet_area() {
		return carpet_area;
	}

	public void setCarpet_area(String carpet_area) {
		this.carpet_area = carpet_area;
	}

	public String getDevelopers() {
		return developers;
	}

	public void setDevelopers(String developers) {
		this.developers = developers;
	}

	public String getRate_per_sqrt_foot() {
		return rate_per_sqrt_foot;
	}

	public void setRate_per_sqrt_foot(String rate_per_sqrt_foot) {
		this.rate_per_sqrt_foot = rate_per_sqrt_foot;
	}

	public String getFloor_rise() {
		return floor_rise;
	}

	public void setFloor_rise(String floor_rise) {
		this.floor_rise = floor_rise;
	}

	public String getLoading() {
		return loading;
	}

	public void setLoading(String loading) {
		this.loading = loading;
	}

	public Integer getPropertycompare_id() {
		return propertycompare_id;
	}

	public void setPropertycompare_id(Integer propertycompare_id) {
		this.propertycompare_id = propertycompare_id;
	}

	public String getKulpropertyName() {
		return KulpropertyName;
	}

	public void setKulpropertyName(String kulpropertyName) {
		KulpropertyName = kulpropertyName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}
}