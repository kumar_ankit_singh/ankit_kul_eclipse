package com.kumarbuilders.propertyapp.domain;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class NotificationDB {

	@DatabaseField(canBeNull = false, generatedId = true)
	private Integer id;

	@DatabaseField
	private String message;
	@DatabaseField
	private String datetime;
	@DatabaseField
	private Boolean readUnreadFlag;
	@DatabaseField
	private Integer unreadNo;

	public Integer getUnreadNo() {
		return unreadNo;
	}

	public void setUnreadNo(Integer unreadNo) {
		this.unreadNo = unreadNo;
	}

	public Boolean getReadUnreadFlag() {
		return readUnreadFlag;
	}

	public void setReadUnreadFlag(Boolean readUnreadFlag) {
		this.readUnreadFlag = readUnreadFlag;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

}
