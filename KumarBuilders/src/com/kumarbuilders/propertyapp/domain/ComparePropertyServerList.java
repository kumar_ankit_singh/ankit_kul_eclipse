package com.kumarbuilders.propertyapp.domain;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class ComparePropertyServerList implements Serializable, Parcelable {

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
	private Integer property_id;

	@DatabaseField
	private String propertyName;

	@DatabaseField
	private Integer kulpropertycompare_id;

	public Integer getKulpropertycompare_id() {
		return kulpropertycompare_id;
	}

	public void setKulpropertycompare_id(Integer kulpropertycompare_id) {
		this.kulpropertycompare_id = kulpropertycompare_id;
	}

	public Integer getProperty_id() {
		return property_id;
	}

	public void setProperty_id(Integer property_id) {
		this.property_id = property_id;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}
}