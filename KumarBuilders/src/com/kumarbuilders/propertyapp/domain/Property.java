package com.kumarbuilders.propertyapp.domain;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;

public class Property {
	private String propertyName;
	private String propertyLocation;
	private String url;
	private String Description;
	private int propertyImage;
	private String propertyFile;
	private boolean propertyBook;
	private List<String>floorplanNames=new ArrayList<String>();
	private double latitude;
	private ArrayList<Integer>imagesforGallery=new ArrayList<Integer>();
	
	public ArrayList<Integer> getImagesforGallery() {
		return imagesforGallery;
	}

	public void setImagesforGallery(ArrayList<Integer> imagesforGallery) {
		this.imagesforGallery = imagesforGallery;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	private double longitude;
	public List<String> getFloorplanNames() {
		return floorplanNames;
	}

	public void setFloorplanNames(List<String> floorplanNames) {
		this.floorplanNames = floorplanNames;
	}

	public List<Drawable> getFloorDrawables() {
		return floorDrawables;
	}

	public void setFloorDrawables(List<Drawable> floorDrawables) {
		this.floorDrawables = floorDrawables;
	}

	private List<Drawable>floorDrawables=new ArrayList<Drawable>();
	
	public boolean isPropertyBook() {
		return propertyBook;
	}

	public void setPropertyBook(boolean propertyBook) {
		this.propertyBook = propertyBook;
	}

	public String getPropertyFile() {
		return propertyFile;
	}

	public void setPropertyFile(String propertyFile) {
		this.propertyFile = propertyFile;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public int getPropertyImage() {
		return propertyImage;
	}

	public void setPropertyImage(int propertyImage) {
		this.propertyImage = propertyImage;
	}



	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyLocation() {
		return propertyLocation;
	}

	public void setPropertyLocation(String propertyLocation) {
		this.propertyLocation = propertyLocation;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
