package com.kumarbuilders.propertyapp.domain;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class UserMyKul {

	@DatabaseField(id= true)
	private String userid;
	@DatabaseField
	private String email;
	@DatabaseField
	private String phone;
	@DatabaseField
	private String city;
	@DatabaseField
	private String updateTime;
	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] userPhoto;
	
	public byte[] getUserPhoto() {
		return userPhoto;
	}
	public void setUserPhoto(byte[] userPhoto) {
		this.userPhoto = userPhoto;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	
	
}
