package com.kumarbuilders.propertyapp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class PropertiesDetails implements Serializable, Parcelable {

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
	private Integer property_id;

	@DatabaseField
	private String property_name;

	@DatabaseField
	private String property_description;

	@DatabaseField
	private String property_address;

	@DatabaseField
	private String property_latitude;

	@DatabaseField
	private String property_longitude;

	@DatabaseField
	private String property_imgurl;

	@DatabaseField
	private String property_type;

	@DatabaseField
	private String updated_date;

	@DatabaseField
	private String city;

	@DatabaseField
	private String property_subtype;

	@DatabaseField
	private String price_range_max;

	@DatabaseField
	private String price_range_min;

	@DatabaseField
	private String updated_time;

	@DatabaseField
	private String amenities;

	// Comparison-
	@DatabaseField
	private String kulproject_name;

	@DatabaseField
	private String kulvisibility;
	
	@DatabaseField
	private String kulhospital;
	
	@DatabaseField
	private String kulschool;
	
	@DatabaseField
	private String kulpublic_transport;
	
	@DatabaseField
	private String kulgrocery_market;

	@DatabaseField
	private String kuldevelopers;

	@DatabaseField
	private String kulflat_area;

	@DatabaseField
	private String kulrate_per_sqrt_foot;

	@DatabaseField
	private String kulfloor_rise;

	@DatabaseField
	private String kulloading;

	@DatabaseField
	private String kulcarpet_area;

	@DatabaseField
	private String kulbank_approval;

	@DatabaseField
	private String price_pdf_url;

	@DatabaseField
	private String version;
	

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> amenitiesList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> galleryList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> projectPlanList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> buildingPlanList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> unitPlanList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> angularList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> specificationList;

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> constructionList;

	public String getVersion() {
		
		
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPrice_pdf_url() {
		return price_pdf_url;
	}

	public void setPrice_pdf_url(String price_pdf_url) {
		this.price_pdf_url = price_pdf_url;
	}

	public String getKulbank_approval() {
		return kulbank_approval;
	}

	public void setKulbank_approval(String kulbank_approval) {
		this.kulbank_approval = kulbank_approval;
	}

	public String getKulproject_name() {
		return kulproject_name;
	}

	public void setKulproject_name(String kulproject_name) {
		this.kulproject_name = kulproject_name;
	}

	public String getKulvisibility() {
		return kulvisibility;
	}

	public void setKulvisibility(String kulvisibility) {
		this.kulvisibility = kulvisibility;
	}

	public String getKulhospital() {
		return kulhospital;
	}

	public void setKulhospital(String kulhospital) {
		this.kulhospital = kulhospital;
	}

	public String getKulschool() {
		return kulschool;
	}

	public void setKulschool(String kulschool) {
		this.kulschool = kulschool;
	}

	public String getKulpublic_transport() {
		return kulpublic_transport;
	}

	public void setKulpublic_transport(String kulpublic_transport) {
		this.kulpublic_transport = kulpublic_transport;
	}

	public String getKulgrocery_market() {
		return kulgrocery_market;
	}

	public void setKulgrocery_market(String kulgrocery_market) {
		this.kulgrocery_market = kulgrocery_market;
	}

	public String getKulflat_area() {
		return kulflat_area;
	}

	public void setKulflat_area(String kulflat_area) {
		this.kulflat_area = kulflat_area;
	}

	public String getKulcarpet_area() {
		return kulcarpet_area;
	}

	public void setKulcarpet_area(String kulcarpet_area) {
		this.kulcarpet_area = kulcarpet_area;
	}

	public String getKuldevelopers() {
		return kuldevelopers;
	}

	public void setKuldevelopers(String kuldevelopers) {
		this.kuldevelopers = kuldevelopers;
	}

	public String getKulrate_per_sqrt_foot() {
		return kulrate_per_sqrt_foot;
	}

	public void setKulrate_per_sqrt_foot(String kulrate_per_sqrt_foot) {
		this.kulrate_per_sqrt_foot = kulrate_per_sqrt_foot;
	}

	public String getKulfloor_rise() {
		return kulfloor_rise;
	}

	public void setKulfloor_rise(String kulfloor_rise) {
		this.kulfloor_rise = kulfloor_rise;
	}

	public String getKulloading() {
		return kulloading;
	}

	public void setKulloading(String kulloading) {
		this.kulloading = kulloading;
	}




	public ArrayList<String> getSpecificationList() {
		return specificationList;

	}

	@DatabaseField(dataType = DataType.SERIALIZABLE)
	private ArrayList<String> floorPlanList;

	public ArrayList<String> getConstructionList() {
		return constructionList;
	}

	public void setConstructionList(ArrayList<String> constructionList) {
		this.constructionList = constructionList;
	}

	public ArrayList<String> getFloorPlanList() {
		return floorPlanList;
	}

	public void setFloorPlanList(ArrayList<String> floorPlanList) {
		this.floorPlanList = floorPlanList;
	}

	public void setSpecificationList(ArrayList<String> specificationList) {
		this.specificationList = specificationList;
	}

	public String getAmenities() {
		return amenities;
	}

	public void setAmenities(String amenities) {
		this.amenities = amenities;
	}

	public String getUpdated_time() {
		return updated_time;
	}

	public void setUpdated_time(String updated_time) {
		this.updated_time = updated_time;
	}

	public ArrayList<String> getBuildingPlanList() {
		return buildingPlanList;
	}

	public void setBuildingPlanList(ArrayList<String> buildingPlanList) {
		this.buildingPlanList = buildingPlanList;
	}

	public ArrayList<String> getUnitPlanList() {
		return unitPlanList;
	}

	public void setUnitPlanList(ArrayList<String> unitPlanList) {
		this.unitPlanList = unitPlanList;
	}

	public ArrayList<String> getAngularList() {
		return angularList;
	}

	public void setAngularList(ArrayList<String> angularList) {
		this.angularList = angularList;
	}

	public ArrayList<String> getProjectPlanList() {
		return projectPlanList;
	}

	public void setProjectPlanList(ArrayList<String> projectPlanList) {
		this.projectPlanList = projectPlanList;
	}

	public ArrayList<String> getGalleryList() {
		return galleryList;
	}

	public void setGalleryList(ArrayList<String> galleryList) {
		this.galleryList = galleryList;
	}

	public ArrayList<String> getAmenitiesList() {
		return amenitiesList;
	}

	public void setAmenitiesList(ArrayList<String> amenitiesList) {
		this.amenitiesList = amenitiesList;
	}

	public String getPrice_range_max() {
		return price_range_max;
	}

	public void setPrice_range_max(String price_range_max) {
		this.price_range_max = price_range_max;
	}

	public String getPrice_range_min() {
		return price_range_min;
	}

	public void setPrice_range_min(String price_range_min) {
		this.price_range_min = price_range_min;
	}

	/*
	 * @ForeignCollectionField private Collection<CommunityQuestion> questions;
	 */

	public Integer getProperty_id() {
		return property_id;
	}

	public void setProperty_id(Integer property_id) {
		this.property_id = property_id;
	}

	public String getProperty_name() {
		return property_name;
	}

	public void setProperty_name(String property_name) {
		this.property_name = property_name;
	}

	public String getProperty_description() {
		return property_description;
	}

	public void setProperty_description(String property_description) {
		this.property_description = property_description;
	}

	public String getProperty_address() {
		return property_address;
	}

	public void setProperty_address(String property_address) {
		this.property_address = property_address;
	}

	public String getProperty_latitude() {
		return property_latitude;
	}

	public void setProperty_latitude(String property_latitude) {
		this.property_latitude = property_latitude;
	}

	public String getProperty_longitude() {
		return property_longitude;
	}

	public void setProperty_longitude(String property_longitude) {
		this.property_longitude = property_longitude;
	}

	public String getProperty_imgurl() {
		return property_imgurl;
	}

	public void setProperty_imgurl(String property_imgurl) {
		this.property_imgurl = property_imgurl;
	}

	public String getProperty_type() {
		return property_type;
	}

	public void setProperty_type(String property_type) {
		this.property_type = property_type;
	}

	public String getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(String updated_date) {
		this.updated_date = updated_date;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProperty_subtype() {
		return property_subtype;
	}

	public void setProperty_subtype(String property_subtype) {
		this.property_subtype = property_subtype;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
