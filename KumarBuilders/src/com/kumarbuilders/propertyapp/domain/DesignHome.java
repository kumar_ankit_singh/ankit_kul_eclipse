package com.kumarbuilders.propertyapp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(daoClass = BaseDaoImpl.class)
public class DesignHome implements Serializable, Parcelable {

	@DatabaseField(canBeNull = false, id = true)
	private Integer id;

	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] smallbedroomphoto;

	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] masterbedroomphoto;

	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] hallphoto;

	@DatabaseField(dataType = DataType.BYTE_ARRAY)
	private byte[] kitchenphoto;

	@DatabaseField
	private Boolean hall3seatersofa;

	@DatabaseField
	private Boolean hall2seatersofa;

	@DatabaseField
	private Boolean lshapedsofa;

	@DatabaseField
	private Boolean hallroundtable;

	@DatabaseField
	private Boolean hallsquaretable;

	@DatabaseField
	private Boolean hallloungechair;

	@DatabaseField
	private Boolean hallaccentchair;

	@DatabaseField
	private Boolean hallrecliner;

	@DatabaseField
	private Boolean halltv;

	@DatabaseField
	private Boolean hallsmallbeanbag;

	@DatabaseField
	private Boolean halllargebeanbag;

	@DatabaseField
	private Boolean hallcornertable;

	@DatabaseField
	private Boolean hallSave;

	@DatabaseField
	private Boolean smallBrkingBed;

	@DatabaseField
	private Boolean smallBrqueenBed;

	@DatabaseField
	private Boolean smallBrsideTable;

	@DatabaseField
	private Boolean smallBrstudytable;

	@DatabaseField
	private Boolean smallBrloungechair;

	@DatabaseField
	private Boolean smallBraccentchair;

	@DatabaseField
	private Boolean foursquaredining;
	@DatabaseField
	private Boolean fourroundedining;
	@DatabaseField
	private Boolean fourrectdining;
	@DatabaseField
	private Boolean sixquaredining;
	@DatabaseField
	private Boolean sixovaldining;
	@DatabaseField
	private Boolean eightrectdining;

	@DatabaseField
	private Boolean eightrounddining;

	@DatabaseField
	private Boolean islandkitchen;

	@DatabaseField
	private Boolean cabinetkitchen;

	@DatabaseField
	private Boolean mbrkingBed;

	@DatabaseField
	private Boolean mbrqueenBed;

	@DatabaseField
	private Boolean mbrsideTable;

	@DatabaseField
	private Boolean mbrstudytable;

	@DatabaseField
	private Boolean mbrcoffeetable;

	@DatabaseField
	private Boolean mbrloungechair;

	@DatabaseField
	private Boolean mbraccentchair;

	@DatabaseField
	private Boolean mbrtv;

	@DatabaseField
	private Boolean mbrbunkbed;

	@DatabaseField
	private Boolean mbrwrdrobe;

	@DatabaseField
	private Boolean mbrrecliner;

	public Boolean getMbrbunkbed() {
		return mbrbunkbed;
	}

	public void setMbrbunkbed(Boolean mbrbunkbed) {
		this.mbrbunkbed = mbrbunkbed;
	}

	public Boolean getMbrwrdrobe() {
		return mbrwrdrobe;
	}

	public void setMbrwrdrobe(Boolean mbrwrdrobe) {
		this.mbrwrdrobe = mbrwrdrobe;
	}

	public Boolean getMbrrecliner() {
		return mbrrecliner;
	}

	public void setMbrrecliner(Boolean mbrrecliner) {
		this.mbrrecliner = mbrrecliner;
	}

	public Boolean getMbrkingBed() {
		return mbrkingBed;
	}

	public void setMbrkingBed(Boolean mbrkingBed) {
		this.mbrkingBed = mbrkingBed;
	}

	public Boolean getMbrqueenBed() {
		return mbrqueenBed;
	}

	public void setMbrqueenBed(Boolean mbrqueenBed) {
		this.mbrqueenBed = mbrqueenBed;
	}

	public Boolean getMbrsideTable() {
		return mbrsideTable;
	}

	public void setMbrsideTable(Boolean mbrsideTable) {
		this.mbrsideTable = mbrsideTable;
	}

	public Boolean getMbrstudytable() {
		return mbrstudytable;
	}

	public void setMbrstudytable(Boolean mbrstudytable) {
		this.mbrstudytable = mbrstudytable;
	}

	public Boolean getMbrcoffeetable() {
		return mbrcoffeetable;
	}

	public void setMbrcoffeetable(Boolean mbrcoffeetable) {
		this.mbrcoffeetable = mbrcoffeetable;
	}

	public Boolean getMbrloungechair() {
		return mbrloungechair;
	}

	public void setMbrloungechair(Boolean mbrloungechair) {
		this.mbrloungechair = mbrloungechair;
	}

	public Boolean getMbraccentchair() {
		return mbraccentchair;
	}

	public void setMbraccentchair(Boolean mbraccentchair) {
		this.mbraccentchair = mbraccentchair;
	}

	public Boolean getMbrtv() {
		return mbrtv;
	}

	public void setMbrtv(Boolean mbrtv) {
		this.mbrtv = mbrtv;
	}

	public Boolean getFoursquaredining() {
		return foursquaredining;
	}

	public void setFoursquaredining(Boolean foursquaredining) {
		this.foursquaredining = foursquaredining;
	}

	public Boolean getFourroundedining() {
		return fourroundedining;
	}

	public void setFourroundedining(Boolean fourroundedining) {
		this.fourroundedining = fourroundedining;
	}

	public Boolean getFourrectdining() {
		return fourrectdining;
	}

	public void setFourrectdining(Boolean fourrectdining) {
		this.fourrectdining = fourrectdining;
	}

	public Boolean getSixquaredining() {
		return sixquaredining;
	}

	public void setSixquaredining(Boolean sixquaredining) {
		this.sixquaredining = sixquaredining;
	}

	public Boolean getSixovaldining() {
		return sixovaldining;
	}

	public void setSixovaldining(Boolean sixovaldining) {
		this.sixovaldining = sixovaldining;
	}

	public Boolean getEightrectdining() {
		return eightrectdining;
	}

	public void setEightrectdining(Boolean eightrectdining) {
		this.eightrectdining = eightrectdining;
	}

	public Boolean getEightrounddining() {
		return eightrounddining;
	}

	public void setEightrounddining(Boolean eightrounddining) {
		this.eightrounddining = eightrounddining;
	}

	public Boolean getSmallBrkingBed() {
		return smallBrkingBed;
	}

	public void setSmallBrkingBed(Boolean smallBrkingBed) {
		this.smallBrkingBed = smallBrkingBed;
	}

	public Boolean getSmallBrqueenBed() {
		return smallBrqueenBed;
	}

	public void setSmallBrqueenBed(Boolean smallBrqueenBed) {
		this.smallBrqueenBed = smallBrqueenBed;
	}

	public Boolean getSmallBrsideTable() {
		return smallBrsideTable;
	}

	public void setSmallBrsideTable(Boolean smallBrsideTable) {
		this.smallBrsideTable = smallBrsideTable;
	}

	public Boolean getSmallBrstudytable() {
		return smallBrstudytable;
	}

	public void setSmallBrstudytable(Boolean smallBrstudytable) {
		this.smallBrstudytable = smallBrstudytable;
	}

	public Boolean getSmallBrloungechair() {
		return smallBrloungechair;
	}

	public void setSmallBrloungechair(Boolean smallBrloungechair) {
		this.smallBrloungechair = smallBrloungechair;
	}

	public Boolean getSmallBraccentchair() {
		return smallBraccentchair;
	}

	public void setSmallBraccentchair(Boolean smallBraccentchair) {
		this.smallBraccentchair = smallBraccentchair;
	}

	public Boolean getHallSave() {
		return hallSave;
	}

	public void setHallSave(Boolean hallSave) {
		this.hallSave = hallSave;
	}

	public Boolean getHall3seatersofa() {
		return hall3seatersofa;
	}

	public void setHall3seatersofa(Boolean hall3seatersofa) {
		this.hall3seatersofa = hall3seatersofa;
	}

	public Boolean getHall2seatersofa() {
		return hall2seatersofa;
	}

	public void setHall2seatersofa(Boolean hall2seatersofa) {
		this.hall2seatersofa = hall2seatersofa;
	}

	public Boolean getLshapedsofa() {
		return lshapedsofa;
	}

	public void setLshapedsofa(Boolean lshapedsofa) {
		this.lshapedsofa = lshapedsofa;
	}

	public Boolean getHallroundtable() {
		return hallroundtable;
	}

	public void setHallroundtable(Boolean hallroundtable) {
		this.hallroundtable = hallroundtable;
	}

	public Boolean getHallsquaretable() {
		return hallsquaretable;
	}

	public void setHallsquaretable(Boolean hallsquaretable) {
		this.hallsquaretable = hallsquaretable;
	}

	public Boolean getHallloungechair() {
		return hallloungechair;
	}

	public void setHallloungechair(Boolean hallloungechair) {
		this.hallloungechair = hallloungechair;
	}

	public Boolean getHallaccentchair() {
		return hallaccentchair;
	}

	public void setHallaccentchair(Boolean hallaccentchair) {
		this.hallaccentchair = hallaccentchair;
	}

	public Boolean getHalltv() {
		return halltv;
	}

	public void setHalltv(Boolean halltv) {
		this.halltv = halltv;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getSmallbedroomphoto() {
		return smallbedroomphoto;
	}

	public void setSmallbedroomphoto(byte[] smallbedroomphoto) {
		this.smallbedroomphoto = smallbedroomphoto;
	}

	public byte[] getMasterbedroomphoto() {
		return masterbedroomphoto;
	}

	public void setMasterbedroomphoto(byte[] masterbedroomphoto) {
		this.masterbedroomphoto = masterbedroomphoto;
	}

	public byte[] getHallphoto() {
		return hallphoto;
	}

	public void setHallphoto(byte[] hallphoto) {
		this.hallphoto = hallphoto;
	}

	public byte[] getKitchenphoto() {
		return kitchenphoto;
	}

	public void setKitchenphoto(byte[] kitchenphoto) {
		this.kitchenphoto = kitchenphoto;
	}

	public Boolean getHallrecliner() {
		return hallrecliner;
	}

	public void setHallrecliner(Boolean hallrecliner) {
		this.hallrecliner = hallrecliner;
	}

	public Boolean getHallsmallbeanbag() {
		return hallsmallbeanbag;
	}

	public void setHallsmallbeanbag(Boolean hallsmallbeanbag) {
		this.hallsmallbeanbag = hallsmallbeanbag;
	}

	public Boolean getHalllargebeanbag() {
		return halllargebeanbag;
	}

	public void setHalllargebeanbag(Boolean halllargebeanbag) {
		this.halllargebeanbag = halllargebeanbag;
	}

	public Boolean getHallcornertable() {
		return hallcornertable;
	}

	public void setHallcornertable(Boolean hallcornertable) {
		this.hallcornertable = hallcornertable;
	}

	public Boolean getIslandkitchen() {
		return islandkitchen;
	}

	public void setIslandkitchen(Boolean islandkitchen) {
		this.islandkitchen = islandkitchen;
	}

	public Boolean getCabinetkitchen() {
		return cabinetkitchen;
	}

	public void setCabinetkitchen(Boolean cabinetkitchen) {
		this.cabinetkitchen = cabinetkitchen;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
