package com.kumarbuilders.propertyapp.tasks;

import org.json.JSONObject;

import com.kumarbuilders.propertyapp.processor.UrlProcesser;

import android.app.Activity;
import android.os.AsyncTask;

public class AsyncHttpGET extends AsyncTask<JSONObject, String, String> {
	private Activity activity;
	private String url;
	private TaskResponse taskResponse;

	public AsyncHttpGET(Activity activity, String url, TaskResponse taskResponse) {
		this.url = url;
		this.activity = activity;
		this.taskResponse = taskResponse;
	}

	protected void onPreExecute() {
	}

	@Override
	protected String doInBackground(JSONObject... params) {
		String json = null;
		try {
			json = UrlProcesser.getUrl(url);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	protected void onPostExecute(String result) {
		try {
			if (result == null) {
				return;
			} else
				taskResponse.taskResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}