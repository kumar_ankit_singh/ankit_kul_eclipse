package com.kumarbuilders.propertyapp.tasks;

import org.json.JSONObject;

import com.kumarbuilders.propertyapp.processor.UrlProcesser;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

public class AsyncHttpPost extends AsyncTask<JSONObject, String, String> {
	private Activity activity;
	private String url;
	private TaskResponse taskResponse;
	private ProgressDialog pDialog;
	private boolean flagShow;

	public AsyncHttpPost(Activity activity, String url,
			TaskResponse taskResponse, boolean flagShow) {
		this.url = url;
		this.activity = activity;
		this.taskResponse = taskResponse;
		this.flagShow = flagShow;
	}

	protected void onPreExecute() {
		super.onPreExecute();

		if (flagShow) {
			pDialog = new ProgressDialog(activity);
			pDialog.setMessage("Please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
		}

	}

	@Override
	protected String doInBackground(JSONObject... params) {
		String json = null;
		try {
			json = UrlProcesser.postUrl(url, params[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	protected void onPostExecute(String result) {

		try {

			if (flagShow) {
				if (pDialog != null)
					pDialog.dismiss();
			}

			if (result == null) {
				return;
			} else
				taskResponse.taskResponse(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}