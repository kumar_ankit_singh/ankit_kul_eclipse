package com.kumarbuilders.propertyapp.databasehandler;


import android.content.Context;

public class ApplicationPreferences {

	private Context mContext;
	/**
	 * Stores the SharedPreference name
	 */
	private String mPreferenceName = "HealthAssistPreferences";
	/**
	 * Stores the mode to be used for opening an shared preference.
	 */

	public ApplicationPreferences(Context context) {

		mContext = context;
	}

	/**
	 * Used to save passed value in SharedPreference
	 * @param key
	 * @param value
	 */
	public void savePreference(String key, String value)
	{
		

		if(key != null)
			mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).edit().putString(key, value).commit();
	}

	/**
	 * Used to save passed value in SharedPreference
	 * @param key
	 * @param value
	 */
	public void savePreference_int(String key, int value)
	{
		String valueConverted = null;

	

		if(key != null)
			mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).edit().putString(key, valueConverted).commit();
	}
	
	
	
	
	
	public void savePreference_Boolean(String key, boolean value)
	{
		if(key != null)
			mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).edit().putBoolean(key, value).commit();
	}
	
	public boolean getPreference_Boolean(String key,boolean defaultValue)
	{
		if(((mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getBoolean(key, defaultValue))!=(defaultValue)))
		{
			return (mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getBoolean(key, defaultValue));
		}
		return defaultValue;
	}


	/**
	 * Used to get passed key from SharedPreference. If not present, the parameter defaultValue will be returned.
	 * @param key
	 * @param defaultValue
	 */
	public String getPreference(String key, String defaultValue)
	{
		String tempKey = key;


		
		if(defaultValue != null)
		{
			if(!(mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue).equalsIgnoreCase(defaultValue)))
			{
				if(tempKey.equals("userIdForAuthentication") || tempKey.equals("Dialog_Status") || tempKey.equals("FromDate") || tempKey.equals("ToDate") || tempKey.equals("userEmail") || tempKey.equals("password") || tempKey.equals("deviceImei") || tempKey.equals("applicationMode"))//||key.equals("Sync_Sms_Date_Time_Stamp")||key.equals("Sync_Call_Date_Time_Stamp"))
				{
					return mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue);
				}
				else
				{
					return mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue);
				}
			}
			else
			{
				return defaultValue;
			}
		}
		else
		{
			if((mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue) != defaultValue))
			{
				if(tempKey.equals("userIdForAuthentication") || tempKey.equals("Dialog_Status") || tempKey.equals("FromDate") || tempKey.equals("ToDate") || tempKey.equals("userEmail") || tempKey.equals("password") || tempKey.equals("deviceImei") || tempKey.equals("applicationMode"))//||key.equals("Sync_Sms_Date_Time_Stamp")||key.equals("Sync_Call_Date_Time_Stamp"))
				{
					return mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue);
				}
				else
				{
					return mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, defaultValue);
				}
			}
			else
			{
				return null;
			}
		}
	}

	/**
	 * Used to get passed key from SharedPreference. If not present, the parameter defaultValue will be returned.
	 * @param key
	 * @param defaultValue
	 */
	public int getPreference_int(String key, int defaultValue)
	{

		if(!(mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, ""+defaultValue).equalsIgnoreCase(""+defaultValue)))
		{
			return Integer.parseInt(mContext.getSharedPreferences(mPreferenceName, mContext.MODE_PRIVATE).getString(key, ""+defaultValue));
		}
		else
		{
			return defaultValue;
		}
	}
}