package com.kumarbuilders.propertyapp.imageloader;

import java.io.InputStream;
import java.io.OutputStream;

import android.graphics.Bitmap;

public class Utils 
{
	private static Utils instance = null;
	public static boolean isbitmap = false;
	private Bitmap mbitmap ;
    public Bitmap getbitmap() {
		return mbitmap;
	}
	public void setbitmap(Bitmap mbitmap) {
		this.mbitmap = mbitmap;
	}
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }
    /**
	 * Called to get this class object
	 * @return
	 */
	public static Utils getInstance()
	{
		if (instance==null)
		{
			instance = new Utils();
		}
		return instance;
	}
	
}