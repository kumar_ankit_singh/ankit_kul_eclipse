package com.kumarbuilders.propertyapp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.widget.Toast;

public class CustomValidators {

	Activity activity;

	// for name validation
	public boolean isValidName(String name) {
		String NAME_PATTERN = "[a-zA-z]+([ '-][a-zA-Z]+)*";
		Pattern pattern = Pattern.compile(NAME_PATTERN);
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();

	}

	// for Email validation
	public boolean isValidEmail(String email) {
		String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	// for Password validation
	public boolean isValidPassword(String password) {
		if (password != null && (password.length() > 3 && password.length() < 12)) {
			return true;
		}
		return false;
	}

	// for Phone Validation
	public boolean isValidPhone(String no) {
		if (no != null && no.length() == 10) {
			return true;
		}
		return false;
	}

	private void showToast(String msg) {
		Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
	}

}
