package com.kumarbuilders.propertyapp.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Vibrator;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

public class CommonFunctions {
	public static Bitmap decodeFile(int id, Activity activity) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			// BitmapFactory.decodeStream(new FileInputStream(f),null,o);
			BitmapFactory.decodeResource(activity.getResources(), id, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 70;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory
					.decodeResource(activity.getResources(), id, o2);
		} catch (Exception e) {
		}
		return null;
	}

	public static void vibratephone(Context context1) {
		try {
			Vibrator v = (Vibrator) context1
					.getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(500);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int currentApplicationVersion(Context context) {
		int version = 0;
		try {
			version = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;

	}

	public static void showCustomToast(String message, Activity activity) {
		Toast toast = Toast.makeText(activity, message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public static void phoneNoLimitWithAutoSoftKeyClose(int count,
			Activity activity) {
		if (count >= 10) {
			InputMethodManager imm = (InputMethodManager) activity
					.getSystemService(Service.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(activity.getCurrentFocus()
					.getWindowToken(), 0);// imm.hideSoftInputFromWindow(editPhone.getWindowToken(),
											// 0);
		}
	}

	public static boolean deleteDirectory(File path) {
		try {
			if (path.exists()) {
				File[] files = path.listFiles();
				if (files == null) {
					return true;
				}
				for (int i = 0; i < files.length; i++) {
					if (files[i].isDirectory()) {
						deleteDirectory(files[i]);
					} else {
						files[i].delete();
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (path.delete());
	}

	/* get current date method */
	public static String getCurrentDate() {
		String dateValue;
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yyyy");
		dateValue = dateformat.format(c.getTime());
		return dateValue;
	}

	/* get current time method */
	public static String getCurrentTime() {
		String timeValue;
		Calendar c = Calendar.getInstance();
		SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm aa");
		timeValue = dateformat.format(c.getTime());
		return timeValue;
	}

}
