package com.kumarbuilders.propertyapp.utils;

public interface UrlConstantUtils {

	// public static String DOMAIN_URL = "http://dev.geekyworks.com/kul/api/";
	// Production-
	public static String DOMAIN_URL = "http://www.kul.co.in/mobile/api/";

	public static String REGISTARTION = DOMAIN_URL + "register.php?format=json";
	public static String POST_IMAGE = DOMAIN_URL + "my-kul.php?format=json";
	public static String GET_PROPERTIES_DATA1 = DOMAIN_URL + "properties?format=json";
	public static String GET_PROPERTIES_DATA2 = DOMAIN_URL + "gallery?format=json";

	public static String FAV_PROPERTY = DOMAIN_URL + "favourite-property?format=json";
	public static String MYKUL_URL = DOMAIN_URL + "my-kul.php?format=json";
	public static String FUTURE_REQUIREMENT_URL = DOMAIN_URL + "requirements?format=json";

	public static String PROPERTY_COMPARE = DOMAIN_URL + "property-comparison?format=json";

	public static String USER_COMPARE_PROPERTY = DOMAIN_URL + "add-comparison-property?format=json";// http://dev.geekyworks.com/kul/api/add-comparison-property?format=json
	public static String GET_VERSION = DOMAIN_URL + "check-version?format=json";
	public static String GET_APP_VERSION = DOMAIN_URL + "check-app-store-version?format=json";

}
