package com.kumarbuilders.propertyapp.utils;

import com.facebook.android.Facebook;


public interface StringConstant {
	public static String error="error";
	public static String create="create";
	
	//Coupon Details
	public static String id="id";
	public static final String JSON_CHAR_SET = "application/json;charset=UTF-8";
	public static final String JSON_TYPE = "application/json";
	public static final String TEXT_ACCEPT = "Accept";
	public static final String APPLICATION_JSON = "application/json";
	public static final String STATUS = "STATUS";
	
	//login
	public static final String LOG_USERNAME = "username";
	public static final String LOG_PASSWORD = "password";
	public static final String LOG_USERID = "userid";
	
	//registration
	public static final String REG_USERNAME = "name";
	public static final String REG_MAIL = "email";
	public static final String REG_PWD = "password";
	public static final String REG_CPWD = "cpassword";
	public static final String REG_CITY = "city";
	public static final String REG_PHONE = "phone";
	
	//method type
	public static final String METHOD_TYPE = "METHOD";
	public static final String POST_METHOD = "POST";
	public static final String GET_METHOD = "GET";
	public static final String METHOD = "method";
	
	//myKul
	public static final String KUL_USERID = "userid";
	public static final String KUL_MAIL = "email";
	public static final String KUL_CITY = "city";
	public static final String KUL_PHONE = "contact_number";
	public static final String KUL_PWD = "password";
	public static final String KUL_UPDATE = "updated_time"; 
	public static final String KUL_POST_EMAIL = "user_email";
	/*{"userid":"1","method":"POST","city":"PUNE","contact_number":"9011","user_email":"naphadevilas@gmail.com"}*/
	
	//change password
	public static final String CPWD_USERID = "userid";
	public static final String CPWD_PWD = "password";
	public static final String CPWD_NPWD = "new_password";
	public static final String CPWD_CPWD = "confirm_password";
	public static final String MESSAGES = "message";
	
	//future requirement 
	public static final String REQ_LOGID = "loginId";
	public static final String REQ_DUR = "duration";
	public static final String REQ_PPT = "property_type";
	public static final String REQ_BHK = "bhk_type";
	public static final String REQ_CITY = "city";
	public static final String REQ_LOC = "location";
	
	//custom message
	public static final String FAILED_LOGIN_MSG = "Invalid Username & Password";
	public static final String NULL_RESPONSE = "something went wrong or response is null";
	public static final String IMG_DATA = "Image not available";
	//custom exception message
	public static final String SUPER_EXCEPTION = "Exception occured";
	public static final String NULL_POINTER = "Exception occur in NullPointerException";
	public static final String NOT_FOUND = "activity or fragment not found exception";
	public static final String JSON_EXCEP = "Exception occured in JSONException";
	public static final String UNKNOWN_HOST = "Unable to resolve host name";
	public static final String IMAG_CROP = "This device doesn't support the crop action!";
	public static final String VIEW_EXCEPTION = "Exception occur in create view";
	public static final String ILLEGAL_EXC = "IllegalState Exception occurred in onActivityCreate";
	
	
	// facebook
	public static String APP_ID = "297126590498106";
	public static final String ACCESS_TOKEN = "access_token";
	public static final String ACCESS_EXPIRES = "access_expires";
	public static Facebook facebook = new Facebook(APP_ID);
	
	
	//twitter 
	/* Shared preference keys */
	public static final String PREF_NAME = "sample_twitter_pref";
	public static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
	public static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
	public static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
	public static final String PREF_USER_NAME = "twitter_user_name";

	/* Any number for uniquely distinguish your request */
	public static final int WEBVIEW_REQUEST_CODE = 100;
	public static String EXTRA_URL = "extra_url";
	// Twitter oauth urls
    public static final String URL_TWITTER_AUTH = "auth_url";
    public static final String URL_TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    public static final String URL_TWITTER_OAUTH_TOKEN = "oauth_token";
    
    /*XMPP Connection*/
    public final static String CHAT_HOST = "dev.geekyworks.com";
    public final static int CHAT_PORT = 9098;
    public final static String CHAT_SERVICE = "@dev.geekyworks.com";
    public final static String CHAT_PASSWORD = "kul_user@123";

}
