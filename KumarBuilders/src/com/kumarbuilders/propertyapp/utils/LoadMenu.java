package com.kumarbuilders.propertyapp.utils;

import java.util.Collections;
import java.util.List;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class LoadMenu {
	private GridAdapter gridadapter;
	private Intent email = new Intent(Intent.ACTION_SEND);
	private Activity activity;

	public void loadGridItems(String propertyName, String propertyLocation,
			String propertyUrl, Activity activity) {
		final Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
		WMLP.gravity = Gravity.CENTER;
		this.activity = activity;
		dialog.getWindow().setAttributes(WMLP);
		dialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.setCanceledOnTouchOutside(true);
		dialog.setContentView(R.layout.sharepopup);
		dialog.setCancelable(true);
		GridView gridview = (GridView) dialog.findViewById(R.id.gridview);
		PackageManager pm = activity.getPackageManager();
		email.putExtra(Intent.EXTRA_EMAIL, new String[] { " " });
		email.putExtra(Intent.EXTRA_SUBJECT, propertyName + ""
				+ propertyLocation);
		email.putExtra(Intent.EXTRA_TEXT, propertyName + "" + propertyUrl);
		email.setType("text/plain");

		List<ResolveInfo> launchables = pm.queryIntentActivities(email, 0);
		Collections
				.sort(launchables, new ResolveInfo.DisplayNameComparator(pm));
		setAdapter(gridview, pm, launchables, activity);
		dialog.show();
	}

	public void setAdapter(GridView gridview, PackageManager pakagemanager,
			List<ResolveInfo> list, final Activity activity) {
		gridadapter = new GridAdapter(pakagemanager, list);
		gridview.setAdapter(gridadapter);
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				ResolveInfo launchable = gridadapter.getItem(position);
				ActivityInfo activityinfo = launchable.activityInfo;
				ComponentName name = new ComponentName(
						activityinfo.applicationInfo.packageName,
						activityinfo.name);
				email.addCategory(Intent.CATEGORY_LAUNCHER);
				email.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
						| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				email.setComponent(name);
				activity.startActivity(email);
			}
		});
	}

	class GridAdapter extends ArrayAdapter<ResolveInfo> {
		private PackageManager pm = null;

		GridAdapter(PackageManager pm, List<ResolveInfo> apps) {
			super(activity, R.layout.griditems, apps);
			this.pm = pm;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = newView(parent);
			}

			bindView(position, convertView);

			return (convertView);
		}

		private View newView(ViewGroup parent) {
			return (activity.getLayoutInflater().inflate(R.layout.griditems,
					parent, false));
		}

		private void bindView(int position, View row) {
			// TextView itemName = (TextView) row.findViewById(R.id.item_text);
			// itemName.setText(getItem(position).loadLabel(pm));
			ImageView itemIcon = (ImageView) row.findViewById(R.id.item_image);
			itemIcon.setImageDrawable(getItem(position).loadIcon(pm));
		}
	}
}
