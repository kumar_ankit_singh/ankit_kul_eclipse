//**************************************************************************************************
//**************************************************************************************************
//                                      Copyright (c) 2010 by PURPLETALK INC
//                                              ALL RIGHTS RESERVED
//**************************************************************************************************
//**************************************************************************************************
//
//      Project name                    		: LabCheck Go
//      Class Name                              : CheckNetwork
//      Date                                    : Apr-26-2011
//      Author                                  : A Praveena
//      Version                                 : 1.0
//
//***************************************************************************************************
//      Class Description:
// CheckNetwork is used to check the network and get the result number which is displayed in bottom bar
//
//***************************************************************************************************
//      Update history:
//      Date :                          Developer Name :Praveena A     Modification Comments :
//
//***************************************************************************************************

package com.kumarbuilders.propertyapp.utils;

import com.kumarbuilders.propertyapp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;

/**
 * This CheckNetwork class is used to check the Internet connection is available
 * or not .
 * 
 * @version
 * @CodeReview Praveena
 */
public class CheckNetwork {

	public static boolean isInternetPresent(final Activity activity) {
		boolean isResult = Boolean.FALSE;
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			for (NetworkInfo networkInfo : connectivity.getAllNetworkInfo()) {
				if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
					isResult = true;
					break;
				}
			}
		}
		return isResult;
	}

	public static void showNetworkConnectionMessage(Activity activity) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		alertDialog.setTitle("Instructions");
		alertDialog.setMessage("Internet connection is not available. Please check your internet connection settings.");
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alertDialog.show();
	}

	public static void showNetworkTimeOutMessage(Activity activity) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		alertDialog.setTitle("Instructions");
		alertDialog.setMessage("Internet connection is too slow.");
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alertDialog.show();
	}

	public static void showDataNotFoundMessage(Activity activity) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		alertDialog.setTitle("Instructions");
		alertDialog.setIcon(R.drawable.appiconpopup);
		alertDialog.setMessage("Data Not Found");
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alertDialog.show();
	}

	public static void showPopupMessage(Activity activity, String message) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		alertDialog.setTitle("Instructions");
		alertDialog.setIcon(R.drawable.appiconpopup);
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alertDialog.show();
	}

	public static void showAlertMessage(Activity activity, String message) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);
		alertDialog.setTitle("Error");
		alertDialog.setIcon(R.drawable.appiconpopup);
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alertDialog.show();
	}

	
	public static void vibratephone(Context context1) {
		try {
			Vibrator v = (Vibrator) context1.getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(500);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
