package com.kumarbuilders.propertyapp.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.StringConstant;

public final class UrlProcesser {
	private static final BasicHeader BASIC_HEADER = new BasicHeader(
			HTTP.CONTENT_TYPE, StringConstant.JSON_CHAR_SET);
	private static final String CONTENT_TYPE = StringConstant.JSON_CHAR_SET;

	private UrlProcesser() {
	}

	private static HttpClient getHttpClient() {
		HttpParams httpParameters = new BasicHttpParams();
		try {

			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			HttpConnectionParams.setSoTimeout(httpParameters, 10000);

		} catch (Exception e) {
			System.out.println("Net is slow");
		}
		return new DefaultHttpClient(httpParameters);
	}

	private static void setHeaders(final HttpRequestBase httpRequestBase) {
		httpRequestBase.setHeader("data-encrypt", "true");
		httpRequestBase.setHeader(StringConstant.TEXT_ACCEPT,
				StringConstant.APPLICATION_JSON);
	}

	public static String postUrl(final String url, JSONObject request) {
		String timeOut = "0";
		StringBuilder sb = new StringBuilder();
		try {
			HttpPost httppost = new HttpPost(url);

			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is
			// established.
			// The default value is zero, that means the timeout is not used.
			int timeoutConnection = 4000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			// Set the default socket timeout (SO_TIMEOUT)
			// in milliseconds which is the timeout for waiting for data.
			int timeoutSocket = 6000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

			List<NameValuePair> nvps2 = new ArrayList<NameValuePair>();
			nvps2.add(new BasicNameValuePair("data", request.toString()));
			httppost.setEntity(new UrlEncodedFormEntity(nvps2, HTTP.UTF_8));

			HttpResponse response = httpClient.execute(httppost);
			InputStream is = response.getEntity().getContent();
			byte[] buffer = new byte[512];
			int bytes_read = 0;

			while ((bytes_read = is.read(buffer, 0, buffer.length)) > 0) {
				sb.append(new String(buffer, 0, bytes_read));
			}

			Log.v("server response", sb.toString());
		} catch (ConnectTimeoutException e) {
			// Here Connection TimeOut excepion
			return timeOut;

		} catch (Exception e) {
			Log.v("exception in service handler class", "" + e.toString());
		}

		return sb.toString();
	}

	public static String processRequest(InputStream inputStream) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}

			Log.v("server response", sb.toString());
		} catch (IOException e) {
		} finally {
			try {
				reader.close();
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	public static String getUrl(final String url) {
		InputStream is = null;
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			HttpGet httpGet = new HttpGet(url);
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
			return processRequest(is).toString();
		} catch (Exception exception) {
		} finally {
			httpClient.getConnectionManager().closeExpiredConnections();
			httpClient.getConnectionManager().shutdown();
		}
		return processRequest(is).toString();

	}

}
