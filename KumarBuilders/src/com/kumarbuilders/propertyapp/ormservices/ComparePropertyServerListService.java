package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;

public interface ComparePropertyServerListService extends BaseService {

	public List<ComparePropertyServerList> getPropertyCompareServerDatabyName(String fieldName, Object fieldValue);

	public List<ComparePropertyServerList> getPropertyCompareDatabyId(String string, int propertyId);

}
