package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;

import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.DesignHome;


public final class DesignHomeServiceImpl extends BaseServiceImpl implements DesignHomeService {

	private static DesignHomeServiceImpl designhomeServiceImpl;

	public DesignHomeServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler.getConnectionSource(),
				DesignHome.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static DesignHomeServiceImpl getInstance() {
		if (designhomeServiceImpl == null) {
			try {
				designhomeServiceImpl = new DesignHomeServiceImpl();
			} catch (SQLException e) {
			}
		}
		return designhomeServiceImpl;
	}

}
