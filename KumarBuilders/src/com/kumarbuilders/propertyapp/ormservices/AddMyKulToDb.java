package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.kumarbuilders.propertyapp.domain.UserMyKul;

public interface AddMyKulToDb extends BaseService{
	
	
	public void addMyKulData(List<UserMyKul> c);
	public List<UserMyKul> getAllDetails();
	public UserMyKul findById(Integer id);

}
