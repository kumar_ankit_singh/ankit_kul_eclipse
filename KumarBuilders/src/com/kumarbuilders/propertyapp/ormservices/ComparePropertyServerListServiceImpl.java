package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;

public final class ComparePropertyServerListServiceImpl extends BaseServiceImpl
		implements ComparePropertyServerListService {

	private static ComparePropertyServerListServiceImpl favpropertyServiceImpl;
	private ComparePropertyServerList pptcompareList;
	private List<ComparePropertyServerList> propertyInfo;

	public ComparePropertyServerListServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler.getConnectionSource(),
				ComparePropertyServerList.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static ComparePropertyServerListServiceImpl getInstance() {
		if (favpropertyServiceImpl == null) {
			try {
				favpropertyServiceImpl = new ComparePropertyServerListServiceImpl();
			} catch (SQLException e) {
			}
		}
		return favpropertyServiceImpl;
	}

	@Override
	public List<ComparePropertyServerList> getPropertyCompareServerDatabyName(String fieldName, Object fieldValue) {
		try {
			return super.dao.queryForEq(fieldName, fieldValue);
		} catch (SQLException e) {

		}
		return null;
	}

	@Override
	public List<ComparePropertyServerList> getPropertyCompareDatabyId(String fieldName, int propertyId) {
		try {
			return super.dao.queryForEq(fieldName, propertyId);
		} catch (SQLException e) {

		}
		return null;
	}

}
