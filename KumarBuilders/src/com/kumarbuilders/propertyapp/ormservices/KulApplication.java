package com.kumarbuilders.propertyapp.ormservices;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Application;

public class KulApplication extends Application {
	public DatabaseHandler databaseHandler = null;
	private static KulApplication singleton;

	public KulApplication() {
	}

	public static KulApplication getInstance() {
		return singleton;
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
		try {
			databaseHandler = DatabaseHandler.getDatabaseHandler(getApplicationContext(), "");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			System.setProperty("current.date", dateFormat.format(new Date()));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
