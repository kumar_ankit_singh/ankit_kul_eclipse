package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;

import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.FavProperty;

public final class FavPropertyServiceImpl extends BaseServiceImpl implements
		FavPropertyService {

	private static FavPropertyServiceImpl favpropertyServiceImpl;

	public FavPropertyServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler
				.getConnectionSource(), FavProperty.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static FavPropertyServiceImpl getInstance() {
		if (favpropertyServiceImpl == null) {
			try {
				favpropertyServiceImpl = new FavPropertyServiceImpl();
			} catch (SQLException e) {
			}
		}
		return favpropertyServiceImpl;
	}

}
