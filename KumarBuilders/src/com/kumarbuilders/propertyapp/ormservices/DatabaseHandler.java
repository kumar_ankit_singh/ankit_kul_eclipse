package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.DesignHome;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.NotificationDB;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;
import com.kumarbuilders.propertyapp.domain.UserMyKul;
import com.kumarbuilders.propertyapp.domain.UserSignUp;

public final class DatabaseHandler extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "KulDB";
	private static final int DATABASE_VERSION = 7;
	private ConnectionSource connectionSource;
	public static DatabaseHandler databaseHandler;
	private Dao<UserSignUp, Integer> userSignUpData = null;
	private Dao<UserMyKul, Integer> userMyKulData = null;
	private Dao<PropertyCompare, Integer> propertyCompareData = null;

	private DatabaseHandler(Context context, String dataBaseFolder) {
		super(context, dataBaseFolder + DATABASE_NAME, null, DATABASE_VERSION);
		this.getWritableDatabase();
		this.connectionSource = getConnectionSource();
	}

	public static DatabaseHandler getDatabaseHandler(Context context, String dataBaseFolderName) {
		if (databaseHandler == null) {
			databaseHandler = new DatabaseHandler(context, dataBaseFolderName);
		}
		return databaseHandler;
	}

	@Override
	public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
		try {
			this.connectionSource = getConnectionSource();
			createOrDrop(connectionSource, "create");

		} catch (SQLException e) {
			Log.e(DatabaseHandler.class.getName(), "Unable to create datbases", e);
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
		try {
			createOrDrop(connectionSource, "drop");
			onCreate(sqliteDatabase, connectionSource);
		} catch (SQLException e) {
			Log.e(DatabaseHandler.class.getName(),
					"Unable to upgrade database from version " + oldVer + " to new " + newVer, e);
		}
	}

	public <T> void clearTable(Class<T> entityClass) {
		try {
			if (connectionSource == null) {
				connectionSource = getConnectionSource();
			}
			TableUtils.clearTable(connectionSource, entityClass);

		} catch (SQLException e) {
		}

	}

	public void createOrDrop(ConnectionSource connectionSource, final String createOrDrop) throws SQLException {
		if (createOrDrop.equals("create")) {
			TableUtils.createTableIfNotExists(connectionSource, PropertiesDetails.class);
			TableUtils.createTableIfNotExists(connectionSource, UserSignUp.class);
			TableUtils.createTableIfNotExists(connectionSource, FavProperty.class);
			TableUtils.createTable(connectionSource, UserMyKul.class);
			TableUtils.createTable(connectionSource, DesignHome.class);
			TableUtils.createTable(connectionSource, PropertyCompare.class);
			TableUtils.createTable(connectionSource, ComparePropertyServerList.class);
			TableUtils.createTable(connectionSource, NotificationDB.class);

		} else {
			TableUtils.dropTable(connectionSource, PropertiesDetails.class, true);
			TableUtils.dropTable(connectionSource, UserSignUp.class, true);
			TableUtils.dropTable(connectionSource, FavProperty.class, true);
			TableUtils.dropTable(connectionSource, UserMyKul.class, true);
			TableUtils.dropTable(connectionSource, DesignHome.class, true);
			TableUtils.dropTable(connectionSource, PropertyCompare.class, true);
			TableUtils.dropTable(connectionSource, ComparePropertyServerList.class, true);
			TableUtils.dropTable(connectionSource, NotificationDB.class, true);
		}
	}

	public Dao<UserSignUp, Integer> getUserSignUpData() {
		if (userSignUpData == null) {
			try {
				userSignUpData = getDao(UserSignUp.class);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return userSignUpData;
	}

	public Dao<UserMyKul, Integer> getuserMyKulData() {
		if (userMyKulData == null) {
			try {
				userMyKulData = getDao(UserMyKul.class);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return userMyKulData;
	}

	/*
	 * public Dao<PropertyCompare, Integer> getPropertyCompareData() { if
	 * (propertyCompareData == null) { try { propertyCompareData =
	 * getDao(PropertyCompare.class); } catch (SQLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } } return
	 * propertyCompareData; }
	 */

	public ConnectionSource getConnectionsource() {
		return connectionSource;
	}

	public void setConnectionsource(ConnectionSource connectionsource) {
		this.connectionSource = connectionsource;
	}

}
