package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.kumarbuilders.propertyapp.domain.UserSignUp;

public interface AddSignUpToDb extends BaseService{
	
public void addUserSignUpInfo(List<UserSignUp> c);
public List<UserSignUp> getAllDetails();
public UserSignUp findById(Integer id);
}
