package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;

import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.NotificationDB;


public final class NotificationServiceImpl extends BaseServiceImpl implements
		NotificationService {

	private static NotificationServiceImpl notificationServiceImpl;

	public NotificationServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler
				.getConnectionSource(), NotificationDB.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static NotificationServiceImpl getInstance() {
		if (notificationServiceImpl == null) {
			try {
				notificationServiceImpl = new NotificationServiceImpl();
			} catch (SQLException e) {
			}
		}
		return notificationServiceImpl;
	}

}
