package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;

public final class PropertyCompareServiceImpl extends BaseServiceImpl implements
		PropertyCompareService {

	private static PropertyCompareServiceImpl favpropertyServiceImpl;
	private PropertyCompare pptcompareList;
	private List<PropertyCompare> propertyInfo;

	public PropertyCompareServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler
				.getConnectionSource(), PropertyCompare.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static PropertyCompareServiceImpl getInstance() {
		if (favpropertyServiceImpl == null) {
			try {
				favpropertyServiceImpl = new PropertyCompareServiceImpl();
			} catch (SQLException e) {
			}
		}
		return favpropertyServiceImpl;
	}

	@Override
	public List<PropertyCompare> getPropertyCompareDatabyName(String fieldName,
			Object fieldValue) {
		try {
			return super.dao.queryForEq(fieldName, fieldValue);
		} catch (SQLException e) {

		}
		return null;
	}

	@Override
	public List<PropertyCompare> getPropertyCompareDatabyId(String fieldName,
			int propertyId) {
		try {
			return super.dao.queryForEq(fieldName, propertyId);
		} catch (SQLException e) {

		}
		return null;
	}

	/*
	 * @Override public List<PropertyCompare> getAllDetails() { try {
	 * propertyInfo =
	 * KulApplication.getInstance().databaseHandler.getPropertyCompareData
	 * ().queryForAll(); } catch (Exception e) { e.printStackTrace(); } return
	 * propertyInfo; }
	 * 
	 * @Override public PropertyCompare findById(Integer id) { try {
	 * pptcompareList =
	 * KulApplication.getInstance().databaseHandler.getPropertyCompareData
	 * ().queryForId(id); } catch (Exception e) { e.printStackTrace(); } return
	 * pptcompareList; }
	 */
}
