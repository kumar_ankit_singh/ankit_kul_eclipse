package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.UserMyKul;

public class AddMyKulToDbImpl extends BaseServiceImpl implements AddMyKulToDb{

	private List<UserMyKul> kulList;
	private UserMyKul userkulInfo;
	
	public AddMyKulToDbImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler.getConnectionsource(), UserMyKul.class));
	}

	@Override
	public void addMyKulData(List<UserMyKul> c) {

		for(int i=0;i<=c.size();i++){
			try {
				KulApplication.getInstance().databaseHandler.getuserMyKulData().createOrUpdate(c.get(i));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<UserMyKul> getAllDetails() {
		try {
			kulList = KulApplication.getInstance().databaseHandler.getuserMyKulData().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return kulList;
	}

	@Override
	public UserMyKul findById(Integer id) {
		try {
			userkulInfo = KulApplication.getInstance().databaseHandler.getuserMyKulData().queryForId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userkulInfo;
	}

}
