package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.List;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.kumarbuilders.propertyapp.domain.UserSignUp;

public class AddSignUpToDbImpl extends BaseServiceImpl implements AddSignUpToDb{

	private UserSignUp userList;
	private List<UserSignUp> userInfo;
	private static AddSignUpToDbImpl addsignupserviceimpl;
	
	public AddSignUpToDbImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler.getConnectionsource(), UserSignUp.class));
	}

	@Override
	public void addUserSignUpInfo(List<UserSignUp> c) {
		for(int i=0; i<c.size();i++){
			try {
				KulApplication.getInstance().databaseHandler.getUserSignUpData().createOrUpdate(c.get(i));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	
	public static AddSignUpToDbImpl getInstance() {
		if (addsignupserviceimpl == null) {
			try {
				addsignupserviceimpl = new AddSignUpToDbImpl();
				Log.v("instance", ""+addsignupserviceimpl);
			} catch (SQLException e) {
			}
		}
		return addsignupserviceimpl;
	}

	@Override
	public List<UserSignUp> getAllDetails() {
		try {
			userInfo = KulApplication.getInstance().databaseHandler.getUserSignUpData().queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userInfo;
	}

	@Override
	public UserSignUp findById(Integer id) {
		try {
			userList = KulApplication.getInstance().databaseHandler.getUserSignUpData().queryForId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}
	

}
