package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
public interface BaseService<T> {
   public T findById(Integer id) throws Exception ;
   public T getFirst() throws Exception ;
   public long count() throws Exception ;
   public List<T> getAllData() throws Exception ;
   CreateOrUpdateStatus createOrUpdate(T t) throws Exception;
   int deleteById(Object object) throws Exception;
   public T createIfNotExists(T t) throws Exception;
   
}
