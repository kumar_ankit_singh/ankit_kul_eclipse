package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.List;

import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;

public class BaseServiceImpl<T> implements BaseService {
	protected Dao dao = null;

	public <T> BaseServiceImpl(Dao<T, ?> dao) {
		this.dao = dao;
	}

	@Override
	public CreateOrUpdateStatus createOrUpdate(Object t) throws Exception {
		try {
			return dao.createOrUpdate(t);
		} catch (Exception exception) {
			throw new Exception(exception);
		}
	}

	@Override
	public T findById(Integer id) throws Exception {
		try {
			return (T) dao.queryForId(id);
		} catch (SQLException e) {
		}
		return null;
	}

	@Override
	public T getFirst() throws Exception {
		try {
			List<T> list = dao.queryForAll();
			if (list != null && list.size() > 0)
				return (T) list.get(0);
		} catch (SQLException e) {
		}
		return null;
	}

	@Override
	public long count() throws Exception {
		try {
			return dao.countOf();
		} catch (SQLException e) {
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.zero.hour.services.orm.BaseService #getAllData()
	 */
	@Override
	public List<T> getAllData() throws Exception {
		try {
			return (List<T>) dao.queryForAll();

		} catch (SQLException e) {
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.zero.hour.services.orm.BaseService #deleteById(java.lang.Object)
	 */
	@Override
	public int deleteById(Object object) throws Exception {
		try {
			return dao.deleteById(object);
		} catch (SQLException e) {
		}
		return 0;
	}

	@Override
	public Object createIfNotExists(Object object) throws Exception {
		try {
			return dao.createIfNotExists(object);
		} catch (SQLException e) {
		}
		return 0;
	}

}
