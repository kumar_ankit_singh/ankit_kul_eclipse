package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;

public interface PropertyCompareService extends BaseService {

	public List<PropertyCompare> getPropertyCompareDatabyName(String fieldName,
			Object fieldValue);
	public List<PropertyCompare> getPropertyCompareDatabyId(String string, int propertyId);

}
