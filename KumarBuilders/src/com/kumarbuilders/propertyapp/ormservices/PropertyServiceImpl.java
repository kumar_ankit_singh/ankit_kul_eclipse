package com.kumarbuilders.propertyapp.ormservices;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.widget.Toast;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;

public final class PropertyServiceImpl extends BaseServiceImpl implements PropertyService {

	private static PropertyServiceImpl propertyServiceImpl;
	private PropertyService propertyService;
	private ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	List<PropertiesDetails> propertyFilterList = new ArrayList<PropertiesDetails>();
	List<PropertiesDetails> propertyFilterListAdd = new ArrayList<PropertiesDetails>();
	int maxPriceRangeInt, minPriceRangeInt;
	private int mMinValue, mMaxValue, dbmaxPrice, dbminPrice;

	public PropertyServiceImpl() throws SQLException {
		super(DaoManager.createDao(KulApplication.getInstance().databaseHandler.getConnectionSource(),
				PropertiesDetails.class));
	}

	/**
	 * @return create singleton of UserServiceImpl
	 */
	public static PropertyServiceImpl getInstance() {
		if (propertyServiceImpl == null) {
			try {
				propertyServiceImpl = new PropertyServiceImpl();
			} catch (SQLException e) {
			}
		}
		return propertyServiceImpl;
	}

	@Override
	public List<PropertiesDetails> getPropertyByCondition(String ameneities, String updatedate, String city,
			String maxprice, String minprice, String propertytype, Activity activity)
					throws android.database.SQLException {

		try {
			Boolean maxFlag = false, cityFlag = false;
			
			
			propertyService = propertyServiceImpl.getInstance();

			try {
				propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
				propertyFilterList.clear();
				propertyFilterListAdd.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
			int condCount = 0;
			for (int i = 0; i < propertyList.size(); i++) {

				PropertiesDetails property = propertyList.get(i);
				dbmaxPrice = Integer.parseInt(property.getPrice_range_max());
				dbminPrice = Integer.parseInt(property.getPrice_range_min());
				if (maxprice.length() > 0) {
					String maxPriceRange = maxprice.replaceAll("\\D+", "");

					String MinRange = maxPriceRange.substring(0, 2);
					System.out.println(MinRange);

					String MaxRange = maxPriceRange.substring(2, 4);
					System.out.println(MaxRange);

					minPriceRangeInt = Integer.parseInt(MinRange);
					maxPriceRangeInt = Integer.parseInt(MaxRange);

					String minRangeValue1 = MinRange.substring(0, 1);
					String minRangeValue2 = MinRange.substring(1, 2);

					String MaxRangeValue1 = MaxRange.substring(0, 1);
					String MaxRangeValue2 = MaxRange.substring(1, 2);

					if (minRangeValue1.equals("0")) {
						mMinValue = minPriceRangeInt * 100;
					} else {
						mMinValue = minPriceRangeInt;
					}

					if (MaxRangeValue1.equals("0")) {
						mMaxValue = maxPriceRangeInt * 100;
					} else {
						mMaxValue = maxPriceRangeInt;
					}

					if (city.equalsIgnoreCase(property.getCity())) {
						cityFlag = true;
					} else if (city.equals("Any")) {
						cityFlag = true;
					} else {
						cityFlag = false;
					}
				}

				QueryBuilder<PropertiesDetails, Integer> queryBuilder = dao.queryBuilder();
				Where<PropertiesDetails, Integer> where = queryBuilder.where();

				try {
					if (

					cityFlag.equals(true) && ((mMinValue <= dbminPrice) && (dbmaxPrice <= mMaxValue))
							&& (propertytype.equalsIgnoreCase(property.getProperty_subtype())
									|| (propertytype.equalsIgnoreCase("Any")))) {
						where.eq("city", property.getCity());
						where.and();
						// where.eq("amenities", property.getAmenities());
						// where.eq("updated_date", property.getUpdated_date());
						where.eq("price_range_max", property.getPrice_range_max());
						where.and();
						where.eq("price_range_min", property.getPrice_range_min());
						where.and();
						where.eq("property_subtype", property.getProperty_subtype());
						where.and();
						condCount++;

						propertyFilterListAdd.add(property);
					}

					if (condCount > 0)
						propertyFilterList = queryBuilder.query();
					System.out.println("Length=" + propertyFilterList.size());

				} catch (Exception e) {
					// CheckNetwork.showDataNotFoundMessage(activity);
				}

			}

			if (propertyFilterListAdd.size() > 0 && propertyFilterListAdd != null) {
				System.out.println("Added Items in List=" + propertyFilterListAdd.size());
				propertyFilterList.clear();
				propertyFilterList = propertyFilterListAdd;
				System.out.println("Length=" + propertyFilterList.size());
			} else {
				propertyList = new ArrayList<PropertiesDetails>();

				propertyFilterList.clear();
				propertyFilterList = propertyList;
				System.out.println("Length=" + propertyFilterList.size());

			}

		} catch (Exception e) {
			throw new android.database.SQLException();
		}
		return propertyFilterList;

	}

	@Override
	public Boolean deleteAllData(Class<PropertiesDetails> class1) {
		try {
			KulApplication.getInstance().databaseHandler.clearTable(class1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
