package com.kumarbuilders.propertyapp.ormservices;

import java.util.List;

import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;

import android.app.Activity;
import android.database.SQLException;
import android.graphics.Bitmap;

public interface PropertyService extends BaseService {

	List<PropertiesDetails> getPropertyByCondition(String ameneities,
			String updatedate, String city, String maxprice, String minprice,
			String propertytype,Activity activity) throws SQLException;
	
	 Boolean deleteAllData(Class<PropertiesDetails> class1);

}
