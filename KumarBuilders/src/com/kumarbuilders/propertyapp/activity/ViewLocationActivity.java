//**************************************************************************************************
//**************************************************************************************************
//                                    Copyright (c) 2010 by PURPLETALK INC//                                              ALL RIGHTS RESERVED
//**************************************************************************************************
//**************************************************************************************************
//
//    Project name                    		: Heath Assist
//    Class Name                              : HealthAssist
//    Date                                    : June-24-2011
//    Author                                  : A .Praveena
//    Version                                 : 1.1
//
//***************************************************************************************************
//    Class Description: This is the splash screen 
//
//***************************************************************************************************
//    Update history:
//    Date :                          Developer Name :Praveena A     Modification Comments :
//
//***************************************************************************************************

package com.kumarbuilders.propertyapp.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kumarbuilders.propertyapp.R;

public class ViewLocationActivity extends FragmentActivity {

	private GoogleMap googleMap;
	private MarkerOptions markerOptions;
	private LatLng latLng;
	private SupportMapFragment supportMapFragment;
	private Activity activity;
	private ImageView imgBack;
	private String latitude, longitude;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.viewlocation);

			activity = ViewLocationActivity.this;

			supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map);
			imgBack = (ImageView) findViewById(R.id.imgback);

			googleMap = supportMapFragment.getMap();

			Intent intent = activity.getIntent();
			Bundle bundle = intent.getExtras();
			String location = bundle.getString("propertyLocation");
			latitude = bundle.getString("latitude");
			longitude = bundle.getString("longitude");


			googleMap.clear();

			latLng = new LatLng(Double.parseDouble(latitude),
					Double.parseDouble(longitude));

			markerOptions = new MarkerOptions();
			markerOptions.position(latLng);
			markerOptions.title(location);

			googleMap.addMarker(markerOptions);

			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					latLng, 30);
			googleMap.animateCamera(cameraUpdate);

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					activity.finish();

				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

}
