package com.kumarbuilders.propertyapp.activity;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.DesignHome;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeService;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeServiceImpl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class HomeItemSelctActivity extends Activity {

	private Button btn_Apply;
	private String type_home;
	private CheckBox chkTv, chkmbrSideTable, chkmbrStudyTable, chkmbrCoffeeTable, chkmbrTv, chkmbrWardrobe,
			chksbrSideTable, chksbrStudyTable, rbIslandKitchen, rbCabinetKitchen, chkhallCornerTable;
	private RadioGroup radioGroup, radioGroupCenterTable;
	private RadioButton rb3seater, rb2seater, rblsofa, rbroundTable, rbSquareTable, rbLoungeChair, rbAccentChair;
	private RadioButton rb4RoundDining, rb4RectDining, rb4SquareDining, rb6OvalDining, rb6SquareDining, rb8RectDining,
			rb8RoundtDining;
	private RadioButton rbmbrKingBed, rbrmbrQueenBed, rbmbrLoungeChair, rbmbrAccentChair, rbmbrRecliner, rbmbrBunkBed,
			rbsbrKingBed, rbrsbrQueenBed, rbsbrLoungeChair, rbsbrAccentChair, rbhallrecliner, rbhallbeanbagsmall,
			rbhallbeanbaglarge;
	private ImageView imgBack;
	private DesignHomeService designHomeService;
	private DesignHome designHome;
	private RadioGroup rgroupHallDining, rgroupKitchenDining;
	private TextView txthallDiningtext, txtKitchenDiningtext;
	private SharedPreferences sharedpreferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);

			Bundle bundle = getIntent().getExtras();
			type_home = bundle.getString("HomeType");

			if (type_home.equals("hall") || type_home.equals("hallSelect")) {
				setContentView(R.layout.hallitemselect);

			} else if (type_home.equals("kitchen")) {
				setContentView(R.layout.kitchenitemselect);
			} else if (type_home.equals("masterbed")) {
				setContentView(R.layout.masterbritemselect);
			} else if (type_home.equals("smallbed")) {
				setContentView(R.layout.smallrbritemselect);
			}

			else {
				setContentView(R.layout.kitchenitemselect);
			}

			btn_Apply = (Button) findViewById(R.id.btn_apply);
			addListenerOnButton();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addListenerOnButton() {

		init();

		try {
			designHomeService = new DesignHomeServiceImpl();

			if (DesginHomeActivity2.temp != 0) {
				designHome = (DesignHome) designHomeService.findById(DesginHomeActivity2.temp);

			}

			// Hall Items-

			sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
			Boolean flag = sharedpreferences.getBoolean("kitchendiningtable", false);
			Boolean halldiningflag = sharedpreferences.getBoolean("halldiningtable", false);

			txthallDiningtext = (TextView) findViewById(R.id.dininglivingtxt);
			rgroupHallDining = (RadioGroup) findViewById(R.id.dininglivinggroup);

			if (flag.equals(true)) {
				rgroupHallDining.setVisibility(View.GONE);
				txthallDiningtext.setVisibility(View.GONE);
			} else {
				rgroupHallDining.setVisibility(View.VISIBLE);
				txthallDiningtext.setVisibility(View.VISIBLE);
			}

			if (halldiningflag.equals(true)) {
				rgroupHallDining.setVisibility(View.VISIBLE);
				txthallDiningtext.setVisibility(View.VISIBLE);
			}

			if (designHome.getHall3seatersofa() == true) {
				rb3seater.setChecked(true);
			} else if (designHome.getHall2seatersofa() == true) {
				rb2seater.setChecked(true);
			} else if (designHome.getLshapedsofa() == true) {
				rblsofa.setChecked(true);
			}

			if (designHome.getHallroundtable() == true) {
				rbroundTable.setChecked(true);
			} else if (designHome.getHallsquaretable() == true) {
				rbSquareTable.setChecked(true);
			}

			if (designHome.getHallaccentchair() == true) {
				rbAccentChair.setChecked(true);
			} else if (designHome.getHallloungechair() == true) {
				rbLoungeChair.setChecked(true);
			} else if (designHome.getHallrecliner() == true) {
				rbhallrecliner.setChecked(true);
			}

			try {
				if (designHome.getHalltv() == true) {
					chkTv.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getHallcornertable() == true) {
					chkhallCornerTable.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (designHome.getHallsmallbeanbag() == true) {
				rbhallbeanbagsmall.setChecked(true);
			} else if (designHome.getHalllargebeanbag() == true) {
				rbhallbeanbaglarge.setChecked(true);
			}

			try {
				if (designHome.getFoursquaredining() == true) {
					rb4SquareDining.setChecked(true);
				} else if (designHome.getFourroundedining() == true) {
					rb4RoundDining.setChecked(true);
				} else if (designHome.getFourrectdining() == true) {
					rb4RectDining.setChecked(true);
				} else if (designHome.getSixovaldining() == true) {
					rb6OvalDining.setChecked(true);
				} else if (designHome.getSixquaredining() == true) {
					rb6SquareDining.setChecked(true);
				} else if (designHome.getEightrectdining() == true) {
					rb8RectDining.setChecked(true);
				} else if (designHome.getEightrounddining() == true) {
					rb8RoundtDining.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Small Bedroom Items-
		try {

			if (designHome.getSmallBrkingBed() == true) {
				rbsbrKingBed.setChecked(true);
			} else if (designHome.getSmallBrqueenBed() == true) {
				rbrsbrQueenBed.setChecked(true);
			}
			if (designHome.getSmallBrloungechair() == true) {
				rbsbrLoungeChair.setChecked(true);
			} else if (designHome.getSmallBraccentchair() == true) {
				rbsbrAccentChair.setChecked(true);
			}

			try {
				if (designHome.getSmallBrsideTable() == true) {
					chksbrSideTable.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getSmallBrstudytable() == true) {
					chksbrStudyTable.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// kitchen
		try {

			Boolean flag2 = sharedpreferences.getBoolean("halldiningtable", false);
			Boolean flagkitchen = sharedpreferences.getBoolean("kitchendiningtable", false);

			rgroupKitchenDining = (RadioGroup) findViewById(R.id.rgkitchendining);
			txtKitchenDiningtext = (TextView) findViewById(R.id.kitchendiningtext);

			if (flag2.equals(true)) {
				rgroupKitchenDining.setVisibility(View.GONE);
				txtKitchenDiningtext.setVisibility(View.GONE);
			} else {
				rgroupKitchenDining.setVisibility(View.VISIBLE);
				txtKitchenDiningtext.setVisibility(View.VISIBLE);
			}

			if (flagkitchen.equals(true)) {
				rgroupKitchenDining.setVisibility(View.VISIBLE);
				txtKitchenDiningtext.setVisibility(View.VISIBLE);
			}

			if (designHome.getCabinetkitchen() == true) {
				rbCabinetKitchen.setChecked(true);
			} else {
				rbCabinetKitchen.setChecked(false);
			}
			if (designHome.getIslandkitchen() == true) {
				rbIslandKitchen.setChecked(true);
			} else {
				rbIslandKitchen.setChecked(false);
			}

			if (designHome.getFoursquaredining() == true) {
				rb4SquareDining.setChecked(true);
			} else if (designHome.getFourroundedining() == true) {
				rb4RoundDining.setChecked(true);
			} else if (designHome.getFourrectdining() == true) {
				rb4RectDining.setChecked(true);
			} else if (designHome.getSixovaldining() == true) {
				rb6OvalDining.setChecked(true);
			} else if (designHome.getSixquaredining() == true) {
				rb6SquareDining.setChecked(true);
			} else if (designHome.getEightrectdining() == true) {
				rb8RectDining.setChecked(true);
			} else if (designHome.getEightrounddining() == true) {
				rb8RoundtDining.setChecked(true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// MBR-

		try {

			if (designHome.getMbrkingBed() == true) {
				rbmbrKingBed.setChecked(true);
			} else if (designHome.getMbrqueenBed() == true) {
				rbrmbrQueenBed.setChecked(true);
			} else if (designHome.getMbrbunkbed() == true) {
				rbmbrBunkBed.setChecked(true);
			}

			if (designHome.getMbrloungechair() == true) {
				rbmbrLoungeChair.setChecked(true);
			} else if (designHome.getMbraccentchair() == true) {
				rbmbrAccentChair.setChecked(true);
			} else if (designHome.getMbrrecliner() == true) {
				rbmbrRecliner.setChecked(true);
			}

			try {
				if (designHome.getMbrsideTable() == true) {
					chkmbrSideTable.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getMbrstudytable() == true) {
					chkmbrStudyTable.setChecked(true);
				} else {
					chkmbrStudyTable.setChecked(false);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getMbrcoffeetable() == true) {
					chkmbrCoffeeTable.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getMbrtv() == true) {
					chkmbrTv.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if (designHome.getMbrwrdrobe() == true) {
					chkmbrWardrobe.setChecked(true);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		btn_Apply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				try {

					if (type_home.equals("hall") || type_home.equals("hallSelect")) {

						int selectedId = radioGroup.getCheckedRadioButtonId();
						radioGroupCenterTable.getCheckedRadioButtonId();

						Intent intent = new Intent(HomeItemSelctActivity.this, DesginHomeActivity2.class);
						intent.putExtra("type", "hall");
						intent.putExtra("3seatersofa", rb3seater.isChecked());
						intent.putExtra("2seatersofa", rb2seater.isChecked());
						intent.putExtra("lshapesofa", rblsofa.isChecked());
						intent.putExtra("roundtable", rbroundTable.isChecked());
						intent.putExtra("squaretable", rbSquareTable.isChecked());
						intent.putExtra("loungechair", rbLoungeChair.isChecked());
						intent.putExtra("accentchair", rbAccentChair.isChecked());
						intent.putExtra("hallrecliner", rbhallrecliner.isChecked());
						intent.putExtra("tv", chkTv.isChecked());
						intent.putExtra("hallcornertable", chkhallCornerTable.isChecked());
						intent.putExtra("hallsmallbeanbag", rbhallbeanbagsmall.isChecked());
						intent.putExtra("halllargebeanbag", rbhallbeanbaglarge.isChecked());
						intent.putExtra("4rounddining", rb4RoundDining.isChecked());
						intent.putExtra("4rectdining", rb4RectDining.isChecked());
						intent.putExtra("4squaredining", rb4SquareDining.isChecked());
						intent.putExtra("6ovaldining", rb6OvalDining.isChecked());
						intent.putExtra("6squaredining", rb6SquareDining.isChecked());
						intent.putExtra("8rectdining", rb8RectDining.isChecked());
						intent.putExtra("8rounddining", rb8RoundtDining.isChecked());

						designHome.setHall2seatersofa(rb2seater.isChecked());
						designHome.setHall3seatersofa(rb3seater.isChecked());
						designHome.setLshapedsofa(rblsofa.isChecked());
						designHome.setHallroundtable(rbroundTable.isChecked());
						designHome.setHallsquaretable(rbSquareTable.isChecked());
						designHome.setHallloungechair(rbLoungeChair.isChecked());
						designHome.setHallaccentchair(rbAccentChair.isChecked());
						designHome.setHallrecliner(rbhallrecliner.isChecked());
						designHome.setHallsmallbeanbag(rbhallbeanbagsmall.isChecked());
						designHome.setHalllargebeanbag(rbhallbeanbaglarge.isChecked());
						designHome.setHalltv(chkTv.isChecked());
						designHome.setHallcornertable(chkhallCornerTable.isChecked());
						designHomeService.createOrUpdate(designHome);

						sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
						Boolean kitchenflag = sharedpreferences.getBoolean("kitchendiningtable", false);
						if (kitchenflag.equals(true)) {

						} else {

							if (rb4RoundDining.isChecked() == true || rb4RectDining.isChecked() == true
									|| rb4SquareDining.isChecked() == true || rb6OvalDining.isChecked() == true
									|| rb6SquareDining.isChecked() == true || rb8RectDining.isChecked()
									|| rb8RoundtDining.isChecked()) {

								Editor editor = sharedpreferences.edit();
								editor.putBoolean("halldiningtable", true);
								editor.putBoolean("kitchendiningtable", false);
								editor.commit();
							}

						}

						startActivity(intent);
						finish();

					} else if (type_home.equals("kitchen")) {

						Intent intent = new Intent(HomeItemSelctActivity.this, DesginHomeActivity2.class);
						intent.putExtra("type", "kitchen");
						intent.putExtra("CabinetKitchen", rbCabinetKitchen.isChecked());
						intent.putExtra("IslandKitchen", rbIslandKitchen.isChecked());
						intent.putExtra("4rounddining", rb4RoundDining.isChecked());
						intent.putExtra("4rectdining", rb4RectDining.isChecked());
						intent.putExtra("4squaredining", rb4SquareDining.isChecked());
						intent.putExtra("6ovaldining", rb6OvalDining.isChecked());
						intent.putExtra("6squaredining", rb6SquareDining.isChecked());
						intent.putExtra("8rectdining", rb8RectDining.isChecked());
						intent.putExtra("8rounddining", rb8RoundtDining.isChecked());
						intent.putExtra("item", "kitchenItem");

						designHome.setCabinetkitchen(rbCabinetKitchen.isChecked());
						designHome.setIslandkitchen(rbIslandKitchen.isChecked());
						designHomeService.createOrUpdate(designHome);

						sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);

						Boolean hallflag = sharedpreferences.getBoolean("halldiningtable", false);

						if (hallflag.equals(true)) {

						} else {

							if (rb4RoundDining.isChecked() == true || rb4RectDining.isChecked() == true
									|| rb4SquareDining.isChecked() == true || rb6OvalDining.isChecked() == true
									|| rb6SquareDining.isChecked() == true || rb8RectDining.isChecked()
									|| rb8RoundtDining.isChecked()) {

								Editor editor = sharedpreferences.edit();
								editor.putBoolean("kitchendiningtable", true);
								editor.putBoolean("halldiningtable", false);
								editor.commit();
							}

						}

						startActivity(intent);
						finish();

					} else if (type_home.equals("masterbed")) {

						Intent intent = new Intent(HomeItemSelctActivity.this, DesginHomeActivity2.class);
						intent.putExtra("type", "masterbed");
						intent.putExtra("mbrkingbed", rbmbrKingBed.isChecked());
						intent.putExtra("mbrqueenbed", rbrmbrQueenBed.isChecked());
						intent.putExtra("mbrbunkbed", rbmbrBunkBed.isChecked());
						intent.putExtra("mbrtv", chkmbrTv.isChecked());
						intent.putExtra("mbrloungechair", rbmbrLoungeChair.isChecked());
						intent.putExtra("mbraccentchair", rbmbrAccentChair.isChecked());
						intent.putExtra("mbrrecliner", rbmbrRecliner.isChecked());
						intent.putExtra("mbrsidetable", chkmbrSideTable.isChecked());
						intent.putExtra("mbrstudytable", chkmbrStudyTable.isChecked());
						intent.putExtra("mbrcoffeetable", chkmbrCoffeeTable.isChecked());
						intent.putExtra("mbrwardrobe", chkmbrWardrobe.isChecked());

						designHome.setMbrkingBed(rbmbrKingBed.isChecked());
						designHome.setMbrqueenBed(rbrmbrQueenBed.isChecked());
						designHome.setMbrbunkbed(rbmbrBunkBed.isChecked());
						designHome.setMbrsideTable(chkmbrSideTable.isChecked());
						designHome.setMbrstudytable(chkmbrStudyTable.isChecked());
						designHome.setMbrcoffeetable(chkmbrCoffeeTable.isChecked());
						designHome.setMbrtv(chkmbrTv.isChecked());
						designHome.setMbrloungechair(rbmbrLoungeChair.isChecked());
						designHome.setMbraccentchair(rbmbrAccentChair.isChecked());
						designHome.setMbrrecliner(rbmbrRecliner.isChecked());
						designHome.setMbrwrdrobe(chkmbrWardrobe.isChecked());
						designHomeService.createOrUpdate(designHome);
						startActivity(intent);
						finish();

					} else if (type_home.equals("smallbed")) {

						Intent intent = new Intent(HomeItemSelctActivity.this, DesginHomeActivity2.class);
						intent.putExtra("type", "smallbed");
						intent.putExtra("sbrkingbed", rbsbrKingBed.isChecked());
						intent.putExtra("sbrqueenbed", rbrsbrQueenBed.isChecked());
						intent.putExtra("sbrloungechair", rbsbrLoungeChair.isChecked());
						intent.putExtra("sbraccentchair", rbsbrAccentChair.isChecked());
						intent.putExtra("sbrsidetable", chksbrSideTable.isChecked());
						intent.putExtra("sbrstudytable", chksbrStudyTable.isChecked());

						designHome.setSmallBrkingBed(rbsbrKingBed.isChecked());
						designHome.setSmallBrqueenBed(rbrsbrQueenBed.isChecked());
						designHome.setSmallBrsideTable(chksbrSideTable.isChecked());
						designHome.setSmallBrstudytable(chksbrStudyTable.isChecked());
						designHome.setSmallBrloungechair(rbsbrLoungeChair.isChecked());
						designHome.setSmallBraccentchair(rbsbrAccentChair.isChecked());
						designHomeService.createOrUpdate(designHome);

						startActivity(intent);
						finish();

					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		imgBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(HomeItemSelctActivity.this, DesginHomeActivity2.class);
					intent.putExtra("type", type_home);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					HomeItemSelctActivity.this.finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	private void init() {

		try {
			imgBack = (ImageView) findViewById(R.id.imgback);

			// hall Items
			chkTv = (CheckBox) findViewById(R.id.chtv);
			chkhallCornerTable = (CheckBox) findViewById(R.id.chcornertable);

			radioGroup = (RadioGroup) findViewById(R.id.myRadioGroup);
			radioGroupCenterTable = (RadioGroup) findViewById(R.id.rgcentertable);

			rb3seater = (RadioButton) findViewById(R.id.rb3seater);
			rb2seater = (RadioButton) findViewById(R.id.rb2seater);
			rblsofa = (RadioButton) findViewById(R.id.rblsofa);

			rbroundTable = (RadioButton) findViewById(R.id.rbroundtable);
			rbSquareTable = (RadioButton) findViewById(R.id.rbsquaretable);

			rbLoungeChair = (RadioButton) findViewById(R.id.rbloungechair);
			rbAccentChair = (RadioButton) findViewById(R.id.rbaccentchair);
			rbhallrecliner = (RadioButton) findViewById(R.id.rbhallrecliner);

			rbhallbeanbagsmall = (RadioButton) findViewById(R.id.rbbeanbagsmall);
			rbhallbeanbaglarge = (RadioButton) findViewById(R.id.rbbeanbaglarge);

			// Kitchen Items
			rb4RoundDining = (RadioButton) findViewById(R.id.rb4rounddining);
			rb4RectDining = (RadioButton) findViewById(R.id.rb4rectdining);
			rb4SquareDining = (RadioButton) findViewById(R.id.rb4squaredining);
			rb6OvalDining = (RadioButton) findViewById(R.id.rb6ovaldining);
			rb6SquareDining = (RadioButton) findViewById(R.id.rb6squaredining);
			rb8RectDining = (RadioButton) findViewById(R.id.rb8rectdining);
			rb8RoundtDining = (RadioButton) findViewById(R.id.rb8rounddining);
			rbIslandKitchen = (CheckBox) findViewById(R.id.rbIslandkitchen);
			rbCabinetKitchen = (CheckBox) findViewById(R.id.rbcabinetkitchen);

			// MasterBedroom Items
			rbmbrKingBed = (RadioButton) findViewById(R.id.rbmbrkingbed);
			rbrmbrQueenBed = (RadioButton) findViewById(R.id.rbmbrqueenbed);
			chkmbrSideTable = (CheckBox) findViewById(R.id.chmbrsidetable);
			chkmbrStudyTable = (CheckBox) findViewById(R.id.chmbrstudytable);
			chkmbrCoffeeTable = (CheckBox) findViewById(R.id.chmbrcoffeetable);
			rbmbrAccentChair = (RadioButton) findViewById(R.id.rbmbraccentchair);
			rbmbrLoungeChair = (RadioButton) findViewById(R.id.rbmbrloungechair);
			rbmbrRecliner = (RadioButton) findViewById(R.id.rbmbrreclainer);
			rbmbrBunkBed = (RadioButton) findViewById(R.id.rbmbrbunkbed);
			chkmbrTv = (CheckBox) findViewById(R.id.chmbrtv);
			chkmbrWardrobe = (CheckBox) findViewById(R.id.chmbrwrdrobe);

			// SmallBedroom Items

			rbsbrKingBed = (RadioButton) findViewById(R.id.rbsbrkingbed);
			rbrsbrQueenBed = (RadioButton) findViewById(R.id.rbsbrqueenbed);
			chksbrSideTable = (CheckBox) findViewById(R.id.chsbrsidetable);
			chksbrStudyTable = (CheckBox) findViewById(R.id.chsbrstudytable);
			rbsbrAccentChair = (RadioButton) findViewById(R.id.rbsbraccentchair);
			rbsbrLoungeChair = (RadioButton) findViewById(R.id.rbsbrloungechair);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			finish();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}