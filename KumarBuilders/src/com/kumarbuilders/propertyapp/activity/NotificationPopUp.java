package com.kumarbuilders.propertyapp.activity;

import java.util.List;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.NotificationDB;
import com.kumarbuilders.propertyapp.ormservices.NotificationService;
import com.kumarbuilders.propertyapp.ormservices.NotificationServiceImpl;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class NotificationPopUp extends FragmentActivity {
	private Activity activity = NotificationPopUp.this;
	public static String sendername;
	private int id, notificationid;
	protected NotificationManager notificationmanager = null;
	private NotificationService notificationService;
	private int unreadNotifiNo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {

			notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			activity = NotificationPopUp.this;
			Intent i = getIntent();
			Bundle extras = i.getExtras();
			String message = extras.getString("message");
			int id = extras.getInt("id");
			notificationid = extras.getInt("notificationId");
			NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			mNM.cancel(id);
			mNM.cancelAll();

			final Dialog dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			WindowManager.LayoutParams WMLP = dialog.getWindow().getAttributes();
			WMLP.gravity = Gravity.CENTER;
			dialog.getWindow().setAttributes(WMLP);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#F2000000")));
			dialog.setCanceledOnTouchOutside(true);
			dialog.setContentView(R.layout.notification_dialog);
			dialog.setCancelable(true);

			TextView notificationText = (TextView) dialog.findViewById(R.id.message);
			Button offerOkButton = (Button) dialog.findViewById(R.id.dialogOkBtn);

			notificationText.setText(message);
			offerOkButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					updateNotificationDB();
					dialog.dismiss();
					activity.finish();

				}
			});

			dialog.show();

		} catch (Exception e) {
		}

	}

	public void updateNotificationDB() {
		try {
			notificationService = new NotificationServiceImpl();
			NotificationDB notifylist = new NotificationDB();
			List<NotificationDB> notificationList = notificationService.getAllData();
			if (notificationList.size() != 0 || notificationList != null) {
				for (int i = 0; i < notificationList.size(); i++) {
					notifylist = notificationList.get(i);
					unreadNotifiNo = notifylist.getUnreadNo();
				}
			}
			notifylist = (NotificationDB) notificationService.findById(notificationid);
			notifylist.setReadUnreadFlag(true);
			notifylist.setUnreadNo(--unreadNotifiNo);

			notificationService.createOrUpdate(notifylist);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
