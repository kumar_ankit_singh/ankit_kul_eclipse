package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.fragments.AmenitiesFragment;
import com.kumarbuilders.propertyapp.fragments.PropertyDetailsFragment;
import com.kumarbuilders.propertyapp.fragments.PropertyFloorPlanFragment;
import com.kumarbuilders.propertyapp.fragments.SpecificationFragment;
import com.kumarbuilders.propertyapp.fragments.ViewFragment;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyCompareService;
import com.kumarbuilders.propertyapp.ormservices.PropertyCompareServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

public class PropertyDetailsActivity extends FragmentActivity implements
		TaskResponse {

	private ImageView imgBack;
	private LinearLayout lldetails, llamenities, llplan, llgallery,
			llspecification, ll360views, llpropertyCompare;
	private View viewdetails, viewamenities, viewplan, viewgallery,
			viewspecification, view360views, viewCompareProperty;
	public static TextView txtHeader;
	private Bundle extras;
	private String data;
	private ArrayList<String> drawables;
	private ArrayList<String> imgFlipper = new ArrayList<String>();
	private Boolean favFlag = false;
	private Activity activity;
	private String propertyId;
	private PropertyService propertyService;
	private PropertyCompareService propertyCompareService;
	private PropertiesDetails propertyDsignupDataetails;
	private static HashMap<Integer, Integer> mRandomNumStorage = new HashMap<Integer, Integer>();
	private String kul_prop_name;
	private String signupUserId;
	private UserSignUp signUpInfo;
	public AddSignUpToDbImpl signupData;
	private Integer galleryPropertyId;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.propertydetails);

		try {

			initialization();
			activity = PropertyDetailsActivity.this;

			extras = getIntent().getExtras();
			new Bundle();

			if (extras != null) {
				data = "present";
				Log.v("bundle", "" + extras.getInt("propertyId"));
				drawables = extras.getStringArrayList("gallery");
				favFlag = extras.getBoolean("favlist");
				kul_prop_name = extras.getString("propertyName");
				galleryPropertyId = extras.getInt("propertyId");

			} else {
				data = "absent";
			}

			getLoginData();
			// Call Comparison-
			new AsyncHttpPost(activity, UrlConstantUtils.PROPERTY_COMPARE,
					PropertyDetailsActivity.this, true)
					.execute(property_compare("" + extras.getInt("propertyId"),
							signupUserId));
			//

			fragmentLoad(new PropertyDetailsFragment(), data);
			txtHeader.setText("Property Details");
			tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE,
					View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
					View.INVISIBLE);

			lldetails.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
					fragmentLoad(new PropertyDetailsFragment(), data);

					tabViewsVisibility(View.VISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE);
					txtHeader.setText("Property Details");

				}
			});

			llamenities.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					getWindow()
							.setSoftInputMode(
									WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

					Fragment fragment = new AmenitiesFragment();
					FragmentManager fragmentmanager = getSupportFragmentManager();
					FragmentTransaction ftComplaint = fragmentmanager
							.beginTransaction();
					ftComplaint.add(R.id.frame_details, fragment);
					fragment.setArguments(extras);
					ftComplaint.addToBackStack(null);
					ftComplaint.commit();

					tabViewsVisibility(View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
							View.VISIBLE, View.INVISIBLE);
					txtHeader.setText("Amenities");

				}
			});
			llspecification.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					fragmentLoad(new SpecificationFragment(), data);
					tabViewsVisibility(View.INVISIBLE, View.VISIBLE,
							View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE);
					txtHeader.setText("Specification");
				}
			});
			llplan.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					fragmentLoad(new PropertyFloorPlanFragment(), data);
					tabViewsVisibility(View.INVISIBLE, View.INVISIBLE,
							View.VISIBLE, View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE);
					txtHeader.setText("Plans");
				}
			});
			llgallery.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					tabViewsVisibility(View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.VISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE);

					Intent intent = new Intent(PropertyDetailsActivity.this,
							GalleryActivity.class);
					intent.putExtra("activityname", "gallery");
					intent.putExtra("propertyId", galleryPropertyId);

					intent.putExtras(extras);
					startActivity(intent);
					txtHeader.setText("Gallery");
					finish();

				}
			});
			ll360views.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					fragmentLoad(new ViewFragment(), data);
					tabViewsVisibility(View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE, View.VISIBLE,
							View.INVISIBLE, View.INVISIBLE);
					txtHeader.setText("360� Views");

				}
			});

			llpropertyCompare.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					tabViewsVisibility(View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.INVISIBLE, View.INVISIBLE,
							View.INVISIBLE, View.VISIBLE);

					// Intent intent = new Intent(PropertyDetailsActivity.this,
					// PropertyCompareListActivity.class);
					Intent intent = new Intent(PropertyDetailsActivity.this,
							PropertyCompareListActivity.class);//earlier ComparePropertiesActivity
					intent.putExtras(extras);
					startActivity(intent);
					txtHeader.setText("Property Compare");
					finish();

				}
			});

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					if (favFlag.equals(true)) {
						Intent intent = new Intent(
								PropertyDetailsActivity.this,
								ProjectListNewActivity.class);
						intent.putExtra("favlist", true);
						// intent.putExtra("logout", true);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						startActivity(intent);
						PropertyDetailsActivity.this.finish();

					} else {
						Intent intent = new Intent(
								PropertyDetailsActivity.this,
								ProjectListNewActivity.class);
						// intent.putExtra("logout", true);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						PropertyDetailsActivity.this.finish();
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void initialization() {
		try {
			lldetails = (LinearLayout) findViewById(R.id.lldetails);
			llspecification = (LinearLayout) findViewById(R.id.llspecification);
			llplan = (LinearLayout) findViewById(R.id.llplan);
			llamenities = (LinearLayout) findViewById(R.id.llamenities);
			llgallery = (LinearLayout) findViewById(R.id.llgallery);
			ll360views = (LinearLayout) findViewById(R.id.ll360);
			llpropertyCompare = (LinearLayout) findViewById(R.id.llcompare);

			view360views = findViewById(R.id.view360);
			viewamenities = findViewById(R.id.viewamenities);
			viewdetails = findViewById(R.id.viewdetails);
			viewgallery = findViewById(R.id.viewgallery);
			viewplan = findViewById(R.id.viewplan);
			viewspecification = findViewById(R.id.viewspecification);
			viewCompareProperty = findViewById(R.id.viewcompare);

			imgBack = (ImageView) findViewById(R.id.imgback);
			txtHeader = (TextView) findViewById(R.id.txtheader);
		} catch (Exception e) {

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void fragmentLoad(Fragment fragment, String data) {

		try {
			if (data.equals("present")) {
				FragmentManager fragmentmanager = getSupportFragmentManager();
				FragmentTransaction ftComplaint = fragmentmanager
						.beginTransaction();
				ftComplaint.add(R.id.frame_details, fragment);
				fragment.setArguments(extras);
				ftComplaint.addToBackStack(null);
				ftComplaint.commit();

			} else {
				FragmentManager fragmentmanager = getSupportFragmentManager();
				FragmentTransaction ftComplaint = fragmentmanager
						.beginTransaction();
				ftComplaint.add(R.id.frame_details, fragment);
				ftComplaint.addToBackStack(null);
				ftComplaint.commit();
			}
		} catch (Exception e) {

		}
	}

	private void tabViewsVisibility(int vd, int vs, int vp, int vg, int v360,
			int va, int vc) {
		viewdetails.setVisibility(vd);
		viewspecification.setVisibility(vs);
		viewplan.setVisibility(vp);
		viewgallery.setVisibility(vg);
		view360views.setVisibility(v360);
		viewamenities.setVisibility(va);
		viewCompareProperty.setVisibility(vc);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
	}

	@Override
	protected void onDestroy() {

		try {
			super.onDestroy();
			FragmentManager fm = getSupportFragmentManager();
			Fragment fragment = (fm.findFragmentById(R.id.frame_details));
			FragmentTransaction ft = fm.beginTransaction();
			ft.remove(fragment);
			ft.commit();
		} catch (Exception e) {

		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		fragmentLoad(new PropertyDetailsFragment(), data);

		tabViewsVisibility(View.VISIBLE, View.INVISIBLE, View.INVISIBLE,
				View.INVISIBLE, View.INVISIBLE, View.INVISIBLE, View.INVISIBLE);
		txtHeader.setText("Property Details");
	}

	@Override
	protected void onStop() {

		super.onStop();
	}

	public static JSONObject property_compare(String property_id, String user_id) {
		JSONObject compare = new JSONObject();
		try {
			compare.put("property_id", property_id);
			compare.put("user_id", user_id);

		} catch (JSONException e) {
		}
		return compare;
	}

	@Override
	public void taskResponse(String response) {
		String status = null;
		if (response != null) {

			try {
				JSONObject jObject = new JSONObject(response.toString());
				status = jObject.getString("STATUS");

				Log.v("status", "" + status);
				if (status.trim().equals("SUCCESS")) {

					JSONObject jsonObject = jObject.getJSONObject("data");
					propertyId = jsonObject.getString("property_id");

					JSONObject jsonkulObject = jsonObject.getJSONObject("kul");
					Log.v("kulObject", "" + jsonkulObject);

					try {

						JSONArray jsonarray;

						jsonarray = jsonObject.getJSONArray("other_properties");
						Log.v("KulOtherArray", "" + jsonarray);
						PropertyCompare compare = new PropertyCompare();
						propertyService = new PropertyServiceImpl();
						propertyCompareService = new PropertyCompareServiceImpl();

						PropertiesDetails property = (PropertiesDetails) propertyService
								.findById(Integer.parseInt(propertyId));
						property.setKulproject_name(jsonkulObject
								.getString("project_name"));

						try {
							property.setKulvisibility(jsonkulObject
									.getString("visibility"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulhospital(jsonkulObject
									.getString("hospital"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulschool(jsonkulObject
									.getString("school"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulpublic_transport(jsonkulObject
									.getString("public_transport"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulgrocery_market(jsonkulObject
									.getString("grocery_market"));
						} catch (Exception e) {
							e.printStackTrace();
						}

						try {
							property.setKuldevelopers(jsonkulObject
									.getString("developers"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulflat_area(jsonkulObject
									.getString("flat_area"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulrate_per_sqrt_foot(jsonkulObject
									.getString("rate"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulfloor_rise(jsonkulObject
									.getString("floor_rise"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulloading(jsonkulObject
									.getString("loading_area"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulcarpet_area(jsonkulObject
									.getString("carpet_area"));
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							property.setKulbank_approval(jsonkulObject
									.getString("bank_approval"));
						} catch (Exception e) {
							e.printStackTrace();
						}

						for (int i = 0; i < jsonarray.length(); i++) {
							new HashMap<String, String>();

							JSONObject jsonotherObject = jsonarray
									.getJSONObject(i);

							compare.setProperty_id(Integer.parseInt(jsonotherObject
									.getString("comparison_property_id")));
							compare.setPropertycompare_id(Integer
									.parseInt(propertyId));
							System.out.println("propertycompare id = "
									+ Integer.parseInt(propertyId));
							try {
								compare.setPropertyName(jsonotherObject
										.getString("property_name"));
							} catch (Exception e) {
								e.printStackTrace();
							}

							try {
								compare.setProperty_type(jsonotherObject
										.getString("property_type"));
							} catch (Exception e) {
								e.printStackTrace();
							}

							try {
								compare.setProject_name(jsonotherObject
										.getString("project_name"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setVisibility(jsonotherObject
										.getString("visibility"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setHospital(jsonotherObject
										.getString("hospital"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setSchool(jsonotherObject
										.getString("school"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setPublic_transport(jsonotherObject
										.getString("public_transport"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setGrocery_market(jsonotherObject
										.getString("grocery_market"));
							} catch (Exception e) {
								e.printStackTrace();
							}

							try {
								compare.setDevelopers(jsonotherObject
										.getString("developers"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setFlat_area(jsonotherObject
										.getString("flat_area"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setRate_per_sqrt_foot(jsonotherObject
										.getString("rate"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setFloor_rise(jsonotherObject
										.getString("floor_rise"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setLoading(jsonotherObject
										.getString("loading_area"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setCarpet_area(jsonotherObject
										.getString("carpet_area"));
							} catch (Exception e) {
								e.printStackTrace();
							}
							try {
								compare.setBank_approval(jsonotherObject
										.getString("bank_approval"));
							} catch (Exception e) {
								e.printStackTrace();
							}

							propertyCompareService.createOrUpdate(compare);

						}
						propertyService.createOrUpdate(property);

					} catch (NumberFormatException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}

	}

	public int randomBox() {

		Random rand = new Random();
		int pickedNumber = rand.nextInt(100);

		if (mRandomNumStorage.containsKey(pickedNumber)
				|| mRandomNumStorage.containsValue(pickedNumber)) {
			randomBox();
		} else {
			mRandomNumStorage.put(pickedNumber, pickedNumber);

		}
		return pickedNumber;

	}

	private void getLoginData() {
		try {
			signupData = new AddSignUpToDbImpl();

			List<UserSignUp> signuplist = signupData.getAllDetails();
			if (signuplist != null && signuplist.size() != 0) {

				signUpInfo = new UserSignUp();
				if (signuplist.size() != 0) {
					for (int i = 0; i < signuplist.size(); i++) {
						signUpInfo = signuplist.get(i);
						signupUserId = signUpInfo.getUserId();
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
