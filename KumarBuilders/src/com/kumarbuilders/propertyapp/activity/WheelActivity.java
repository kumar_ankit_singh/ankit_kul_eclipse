package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.ArrayWheelAdapter;
import com.kumarbuilders.propertyapp.utils.OnWheelChangedListener;
import com.kumarbuilders.propertyapp.utils.OnWheelScrollListener;
import com.kumarbuilders.propertyapp.utils.WheelView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WheelActivity extends Activity {
	private WheelView wheel, wheel2;
	private ImageView imgBack;
	private TextView txtheader;
	private String filter_item;
	private int listposition;
	private String select_item;
	private ArrayList<String> Filterlist = new ArrayList<String>();
	public static String min_Bedroom = "not Specified", min_Area = "not Specified", max_Price = "20 lac-05 Cr";

	final String Bedrooms[] = new String[] { "1", "2", "3", "4", "5" };
	final String max_Prices1[] = new String[] { "20 lac", "30 lac", "40 lac", "50 lac", "60 lac", "70 lac", "80 lac",
			"90 lac", "01 Cr", "02 Cr", "03 Cr", "04 Cr", "05 Cr" };
	final String max_Prices2[] = new String[] { "20 lac", "30 lac", "40 lac", "50 lac", "60 lac", "70 lac", "80 lac",
			"90 lac", "01 Cr", "02 Cr", "03 Cr", "04 Cr", "05 Cr" };
	final String min_Area1[] = new String[] { "700 Sqft", "800 Sqft", "900 Sqft", "1000 Sqft", "1200 Sqft" };
	final String min_Area2[] = new String[] { "1000 Sqft", "1200 Sqft", "1300 Sqft", "1400 Sqft", "1500 Sqft" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.wheelactivity);
			wheel = getWheel(R.id.feet);
			wheel2 = getWheel(R.id.inch);

			Bundle bundle = getIntent().getExtras();
			Filterlist = bundle.getStringArrayList("listitem");
			filter_item = bundle.getString("filterlistitem");
			listposition = bundle.getInt("position");

			imgBack = (ImageView) findViewById(R.id.imgback);
			txtheader = (TextView) findViewById(R.id.txtheader);

			txtheader.setText("" + filter_item);

			if (filter_item.equals("Min-Max Price")) {

				// setValueRange(1, 10, 70, 90, "lac", "lac");
				initWheel(max_Prices1, max_Prices2);
			} else if (filter_item.equals("Bedrooms")) {
				// setValueRange(1, 3, 2, 5, "from", "to");
				initWheel(Bedrooms, Bedrooms);

			} else if (filter_item.equals("Area")) {
				initWheel(min_Area1, min_Area2);
				// setValueRange(700, 1500, 1500, 2500, "sqft", "sqft");
			}

			Button done = (Button) findViewById(R.id.done);
			done.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					if (filter_item.equals("Min-Max Price")) {
						max_Price = max_Prices1[wheel.getCurrentItem()] + "-" + max_Prices2[wheel2.getCurrentItem()];
						select_item = max_Price;
					} else if (filter_item.equals("Bedrooms")) {
						min_Bedroom = Bedrooms[wheel.getCurrentItem()] + "-" + Bedrooms[wheel2.getCurrentItem()];
						;
						select_item = min_Bedroom;
					} else if (filter_item.equals("Area")) {
						min_Area = min_Area1[wheel.getCurrentItem()] + "-" + min_Area2[wheel2.getCurrentItem()];
						select_item = min_Area;
					}

					Intent intent = new Intent(WheelActivity.this, ProjectListNewActivity.class);
					intent.putExtra("filterlistitem", filter_item);
					intent.putExtra("list_selected_item", select_item);
					intent.putExtra("filter", true);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();

				}
			});

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					// finish();
					Intent intent = new Intent(WheelActivity.this, ProjectListNewActivity.class);
					intent.putExtra("filter", true);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					finish();

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private WheelView getWheel(int id) {
		return (WheelView) findViewById(id);
	}

	// Wheel changed listener
	private OnWheelChangedListener changedListener = new OnWheelChangedListener() {
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			if (!wheelScrolled) {
				// updateStatus();
			}
		}
	};

	// Wheel scrolled flag
	private boolean wheelScrolled = false;

	// Wheel scrolled listener
	OnWheelScrollListener scrolledListener = new OnWheelScrollListener() {
		public void onScrollingStarted(WheelView wheel) {
			wheelScrolled = true;
		}

		public void onScrollingFinished(WheelView wheel) {
			wheelScrolled = false;
			// updateStatus();

			/*
			 * Toast.makeText(getApplicationContext(),
			 * Bedrooms[wheel.getCurrentItem()], Toast.LENGTH_LONG).show();
			 */
		}
	};

	private void initWheel(String arr1[], String arr2[]) {

		ArrayWheelAdapter<String> adapter = new ArrayWheelAdapter<String>(this, arr1);

		wheel.setViewAdapter(adapter);
		wheel.setCurrentItem((int) (Math.random() * 10));
		wheel.setVisibleItems(5);

		wheel.addChangingListener(changedListener);
		wheel.addScrollingListener(scrolledListener);
		wheel.setCyclic(true);
		wheel.setEnabled(true);

		ArrayWheelAdapter<String> adapter2 = new ArrayWheelAdapter<String>(this, arr2);

		wheel2.setViewAdapter(adapter2);
		wheel2.setCurrentItem((int) (Math.random() * 10));
		wheel2.setVisibleItems(5);

		wheel2.addChangingListener(changedListener);
		wheel2.addScrollingListener(scrolledListener);
		wheel2.setCyclic(true);
		wheel2.setEnabled(true);
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(WheelActivity.this, ProjectListNewActivity.class);
		intent.putExtra("filter", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		finish();
	}

}
