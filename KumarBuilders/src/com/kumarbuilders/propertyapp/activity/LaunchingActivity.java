//**************************************************************************************************
//**************************************************************************************************
//                                    Copyright (c) 2010 by PURPLETALK INC//                                              ALL RIGHTS RESERVED
//**************************************************************************************************
//**************************************************************************************************
//
//    Project name                    		: Heath Assist
//    Class Name                              : HealthAssist
//    Date                                    : June-24-2011
//    Author                                  : A .Praveena
//    Version                                 : 1.1
//
//***************************************************************************************************
//    Class Description: This is the splash screen 
//
//***************************************************************************************************
//    Update history:
//    Date :                          Developer Name :Praveena A     Modification Comments :
//
//***************************************************************************************************

package com.kumarbuilders.propertyapp.activity;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.chat.ChatConnectionService;
import com.kumarbuilders.propertyapp.chat.LocalBinder;
import com.kumarbuilders.propertyapp.databasehandler.ApplicationPreferences;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyService;
import com.kumarbuilders.propertyapp.ormservices.FavPropertyServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpGET;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.FileDownloader;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

public class LaunchingActivity extends Activity implements TaskResponse {

	private Activity activity;
	private PropertyService propertyService;
	private PropertiesDetails propertyDetails;
	private FavPropertyService favpropertyService;
	public AddSignUpToDbImpl signupData;
	private UserSignUp signUpInfo;
	private String signupUserId;
	private Boolean isInternetPresent;
	ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	String dbVersion;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {

			activity = LaunchingActivity.this;
			setContentView(R.layout.startup_activity);
			propertyService = new PropertyServiceImpl();
			isInternetPresent = CheckNetwork.isInternetPresent(activity);

			if (isInternetPresent) {

				try {
					ApplicationPreferences preferences = new ApplicationPreferences(getApplicationContext());
					String userId = preferences.getPreference("kulUserId", "0");
					if(userId != null){
						doBindService();
					}else{
						// do nothing
					}

					// Checking version to update DB-
					propertyService = new PropertyServiceImpl();
					getallproperties();

					if (getallproperties().size() != 0 || getallproperties() != null) {
						dbVersion = propertyList.get(0).getVersion();
						new AsyncHttpGET(activity, UrlConstantUtils.GET_VERSION, LaunchingActivity.this).execute();
					}

					File pdfFile = new File(Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/");
					Boolean flagDeleted = CommonFunctions.deleteDirectory(pdfFile);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// GET PROPERTIES DATA 1 WEB SERVICE CALL

				new AsyncHttpGET(activity, UrlConstantUtils.GET_PROPERTIES_DATA1, LaunchingActivity.this).execute();

				if (signupUserId != null) {

					new AsyncHttpPost(activity, UrlConstantUtils.FAV_PROPERTY, LaunchingActivity.this, false)
							.execute(favPropertyObject(Integer.parseInt(signupUserId), "GET"));
				}

			} else {
				// CheckNetwork.showNetworkConnectionMessage(activity);
			}

			if (isInternetPresent) {
				// Checking App Version to update apk from playstore-
				String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				new AsyncHttpPost(activity, UrlConstantUtils.GET_APP_VERSION, LaunchingActivity.this, false)
						.execute(appVersionObject(versionName));

			} else {

				startThread();
				getLoginData();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void startThread() {
		Thread splashThread = new Thread() {
			@Override
			public void run() {

				try {
					Thread.sleep(1000);
					int waited = 0;
					while (waited < 1) {

						waited += 1;
					}

				} catch (Exception e) {
					handler.sendEmptyMessage(0);
				} finally {

					handler.sendEmptyMessage(1);

				}
			}

		};
		splashThread.start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			try {
				if (msg.what == 0)
					Thread.sleep(10000);
				else if (msg.what == 1) {
					if (signupUserId != null) {

						Intent i = new Intent(LaunchingActivity.this, ProjectListNewActivity.class);
						startActivity(i);
						finish();
					} else {

						Intent i = new Intent(LaunchingActivity.this, SignUpActivity.class);
						startActivity(i);
						finish();

					}

				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	};

	@Override
	protected void onStop() {
		super.onStop();

	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}

	private void getLoginData() {
		try {
			signupData = new AddSignUpToDbImpl();

			List<UserSignUp> signuplist = signupData.getAllDetails();
			if (signuplist != null && signuplist.size() != 0) {

				signUpInfo = new UserSignUp();
				if (signuplist.size() != 0) {
					for (int i = 0; i < signuplist.size(); i++) {
						signUpInfo = signuplist.get(i);
						signupUserId = signUpInfo.getUserId();
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONObject favPropertyObject(int userid, String method) {
		JSONObject favObject = new JSONObject();
		try {
			favObject.put("user_id", userid);
			favObject.put("method", method);

		} catch (JSONException e) {
		}
		return favObject;
	}

	public static JSONObject appVersionObject(String version) {
		JSONObject appObject = new JSONObject();
		try {
			appObject.put("version_number", version);

		} catch (JSONException e) {
		}
		return appObject;
	}

	@Override
	public void taskResponse(String response) {

		try {
			String mResponse = null;

			if (response != null) {
				PropertiesDetails propertyDetails = new PropertiesDetails();

				JSONObject jsonobject = new JSONObject(response);

				String status = jsonobject.getString("STATUS");
				mResponse = jsonobject.getString("RESPONSE");

				if (mResponse.equals("APP_VERSION")) {
					if (status.equals("SUCCESS")) {
						String flag = jsonobject.getString("FLAG");

						if (flag.equals("1")) {
							UpdateAppDialog();
						} else {
							startThread();
							getLoginData();

						}

					}
				}

				else if (mResponse.equals("VERSION")) {
					if (status.equals("SUCCESS")) {
						String serverVersion = jsonobject.getString("version");
						if (!dbVersion.equals(serverVersion)) {
							// GET PROPERTIES DATA 1 WEB SERVICE CALL
							try {
								propertyService.deleteAllData(PropertiesDetails.class);
								File pdfFile = new File(
										Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/");
								Boolean flagDeleted = CommonFunctions.deleteDirectory(pdfFile);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				} else if (mResponse.equals("PROPERTIES")) {
					if (status.equals("SUCCESS")) {

						JSONArray jsonarray;
						String version = jsonobject.getString("version");
						jsonarray = jsonobject.getJSONArray("data");
						propertyService = new PropertyServiceImpl();

						for (int i = 0; i < jsonarray.length(); i++) {
							new HashMap<String, String>();

							jsonobject = jsonarray.getJSONObject(i);
							propertyDetails.setVersion(version);
							propertyDetails.setProperty_id(jsonobject.getInt("property_id"));
							propertyDetails.setProperty_name(jsonobject.getString("property_name"));
							propertyDetails.setProperty_description(jsonobject.getString("property_desc"));
							propertyDetails.setProperty_address(jsonobject.getString("propery_add"));
							propertyDetails.setProperty_latitude(jsonobject.getString("latitude"));
							propertyDetails.setProperty_longitude(jsonobject.getString("longitude"));
							propertyDetails.setProperty_imgurl(jsonobject.getString("image_url"));
							propertyDetails.setProperty_type(jsonobject.getString("property_type"));
							propertyDetails.setAmenities(jsonobject.getString("amenities"));
							propertyDetails.setUpdated_date(jsonobject.getString("updated_date"));
							propertyDetails.setCity(jsonobject.getString("city"));
							propertyDetails.setProperty_subtype(jsonobject.getString("property_sub_type"));
							propertyDetails.setPrice_range_max(jsonobject.getString("price_range_max"));
							propertyDetails.setPrice_range_min(jsonobject.getString("price_range_min"));
							propertyDetails.setPrice_pdf_url(jsonobject.getString("price_pdf_url"));

							if (jsonobject.getString("price_pdf_url") != null
									|| jsonobject.getString("price_pdf_url").length() != 0) {

								downloadPdf(jsonobject.getString("price_pdf_url"));
							}

							// getIconAminities(jsonobject);

							JSONArray specarray;
							ArrayList<String> speclist = new ArrayList<String>();

							specarray = jsonobject.getJSONArray("property_specification");

							for (int j = 0; j < specarray.length(); j++) {

								speclist.add(specarray.getString(j));

							}
							propertyDetails.setSpecificationList(speclist);

							propertyService.createOrUpdate(propertyDetails);

						}
					}
				} else if (mResponse.equals("FAVOURITE")) {

					if (status.equals("FAVOURITE_LIST")) {

						try {
							JSONArray jsonarray;
							favpropertyService = new FavPropertyServiceImpl();
							jsonarray = jsonobject.getJSONArray("data");
							for (int i = 0; i < jsonarray.length(); i++) {
								new HashMap<String, String>();
								FavProperty favpropertyDetails = new FavProperty();
								jsonobject = jsonarray.getJSONObject(i);

								PropertiesDetails property = (PropertiesDetails) propertyService
										.findById(jsonobject.getInt("property_id"));

								favpropertyDetails.setProperty_id(property.getProperty_id());
								favpropertyDetails.setPropertyName(property.getProperty_name());
								favpropertyDetails.setPropertyimage(property.getProperty_imgurl());
								favpropertyDetails.setPropertyLocation(property.getProperty_address());
								favpropertyDetails.setUserId(signupUserId);
								favpropertyService.createOrUpdate(favpropertyDetails);

							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}

			}

			else {
				Toast.makeText(activity, "Internet connection is too slow", Toast.LENGTH_LONG).show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void downloadPdf(String costUrl) {

		String phrase = costUrl;
		// change url production or development-

		String delims = "http://www.kul.co.in/mobile/price-details-pdf/";
		// String delims = "http://dev.geekyworks.com/kul/price-details-pdf/";
		String pdfName = phrase.replace(delims, "");

		File pdfFile = new File(Environment.getExternalStorageDirectory() + "/kulCostPdfFiles/" + pdfName);

		new DownloadFile().execute(costUrl, pdfName);
		Uri path = Uri.fromFile(pdfFile);

	}

	private class DownloadFile extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... strings) {
			String fileUrl = strings[0]; // ->
											// http://maven.apache.org/maven-1.x/maven.pdf
			String fileName = strings[1]; // -> maven.pdf
			String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			File folder = new File(extStorageDirectory, "kulCostPdfFiles");
			folder.mkdir();

			File pdfFile = new File(folder, fileName);

			try {
				pdfFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileDownloader.downloadFile(fileUrl, pdfFile);
			return null;
		}
	}

	private List<PropertiesDetails> getallproperties() {

		try {
			propertyService = new PropertyServiceImpl();
			propertyList.clear();
			propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
			if (propertyList.size() <= 0) {

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

	private void UpdateAppDialog() {

		new AlertDialog.Builder(this).setIcon(R.drawable.appiconpopup).setTitle("Kumar Builder")
				.setMessage("New version of app is available. Update your app ")
				.setPositiveButton("Update", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						try {
							startActivity(new Intent(Intent.ACTION_VIEW,
									Uri.parse("market://details?id=" + getPackageName())));
						} catch (ActivityNotFoundException e) {
							// TODO: handle exception

							startActivity(new Intent(Intent.ACTION_VIEW,
									Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));

						}
					}
				}).setNegativeButton("Ignore", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						startThread();
						getLoginData();
					}
				}).show();

	}
	
	/* Code for connecting through xxmp server*/
	
	 private ChatConnectionService mService;
	    private boolean mBounded;
	    private final ServiceConnection mConnection = new ServiceConnection() {

	        @SuppressWarnings("unchecked")
	        @Override
	        public void onServiceConnected(final ComponentName name,
	                                       final IBinder service) {
	            mService = ((LocalBinder<ChatConnectionService>) service).getService();
	            mBounded = true;
	            Log.d("service connected", "onServiceConnected");
	        }

	        @Override
	        public void onServiceDisconnected(final ComponentName name) {
	            mService = null;
	            mBounded = false;
	            Log.d("service connected", "onServiceDisconnected");
	        }
	    };

	    void doBindService() {
	        bindService(new Intent(this, ChatConnectionService.class), mConnection,
	                Context.BIND_AUTO_CREATE);
	    }

	    void doUnbindService() {
	        if (mConnection != null) {
	            unbindService(mConnection);
	        }
	    }

	    public ChatConnectionService getmService() {
	        return mService;
	    }

}
