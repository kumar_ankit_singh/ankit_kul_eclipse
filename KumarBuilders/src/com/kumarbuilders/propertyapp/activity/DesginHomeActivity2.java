package com.kumarbuilders.propertyapp.activity;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.DesignHome;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeService;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeServiceImpl;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.RotateAnimation;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.AbsoluteLayout.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class DesginHomeActivity2 extends Activity implements OnTouchListener {
	/** Called when the activity is first created. */
	private ImageView img = null, imgSofa, imgTv, imgSmallBed, imgKitchen, imgDining, imgKitchenPlatform,
			imgKitchenPlatform2;
	private ImageView imghallcorner, imgmbrsidebox, imgmbrcorner, imgmbrsidebox2, imgsbrsidebox, imgHallChair1,
			imgHallChair2, imgsbrstudytable, imgmbrtv, imgmbrchair, imgsbrchair, imgmbrWardrobe, imghallbeanbag,
			imghallcornertable, imgLRDining;
	private AbsoluteLayout aLayoutmasterbed, aLayouthall, aLayoutsmallbed, aLayoutKitchen, aLayoutGallery;
	int status = 0, flag = 0, tvflag = 0, cornerflag = 0, kitchenFlag = 0, plant1flag = 0, plant2flag = 0,
			hallcornerflag = 0, statusmbrsidebox = 0, statusmbrcorner = 0, statusmbrsidebox2 = 0, statussbrsidebox = 0,
			statussbrsofa = 0, hallChair1flag = 0, hallChair2Flag = 0, hallSofaFlag = 0, statusmbrtv = 0,
			statusmbrchair = 0, statussbrchair = 0, statusmbrWardrobe = 0, statuskitchenplatform = 0,
			statuskitchenplatform2 = 0, statushallbeanbag = 0, statushallcornertable = 0, statushalldining = 0;
	private Bitmap bmpImg;
	public static byte[] byteArray;
	private String type_home, slectedSpinnerName;
	private double mCurrAngle = 0;
	private double mPrevAngle = 0;
	private ImageView rotateButton, btn_save;
	private ImageView iv;
	private int pos;
	private int position;
	private LinearLayout spinner;
	private DesignHomeService designHomeService;
	private DesignHome designHome;
	public static int temp;

	private static Boolean bedFlag, sofarotateflag, tvrotateflag, cornerrotatebedroom, mbrrotatecorner,
			mbrrotatesidebox, mbrrotatesidebox2, mbrrotatetv, mbrrotatechair, sbrrotatesidebox, sbrrotatesofa,
			hallrotatecorner, hallrotatechair1, hallrotatechair2, kitchenrotatediningtable, sbrrotatechair,
			mbrrotatewardrobe, kitchenrotateplatform, kitchenrotateplatform2, hallrotatebeanbag, hallrotatecornertable,
			hallrotatediningtable;

	private ArrayAdapter<String> adapter;
	private ArrayList<String> selectedItemsSpinner = new ArrayList<String>();
	private Boolean hallSofaboolean = false;
	private Activity activity = DesginHomeActivity2.this;
	private SharedPreferences sharedpreferences;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {

			setContentView(R.layout.designhomeactivity);
			aLayoutmasterbed = (AbsoluteLayout) findViewById(R.id.absLayoutmasterbed);
			aLayouthall = (AbsoluteLayout) findViewById(R.id.absLayouthall);
			aLayoutsmallbed = (AbsoluteLayout) findViewById(R.id.absLayoutsmallbedroom);
			aLayoutKitchen = (AbsoluteLayout) findViewById(R.id.absLayoutkitchen);
			aLayoutGallery = (AbsoluteLayout) findViewById(R.id.absLayoutgallery);

			// Matster Bedroom-
			img = (ImageView) findViewById(R.id.imgbed);
			imgmbrsidebox = (ImageView) findViewById(R.id.imgmbrbedsidebox);
			imgmbrcorner = (ImageView) findViewById(R.id.imgmbrcorner);
			imgmbrsidebox2 = (ImageView) findViewById(R.id.imgmbrbedsidebox2);
			imgmbrtv = (ImageView) findViewById(R.id.imgmbrtv);
			imgmbrchair = (ImageView) findViewById(R.id.imgmbrchair);
			imgmbrWardrobe = (ImageView) findViewById(R.id.imgmbrwardobe);

			// Small Bedroom-

			imgSmallBed = (ImageView) findViewById(R.id.imgsmallbed);
			imgsbrsidebox = (ImageView) findViewById(R.id.imgsbrsidebox);
			imgsbrstudytable = (ImageView) findViewById(R.id.imgsbrsofa);
			imgsbrchair = (ImageView) findViewById(R.id.imgsbrchair);

			// Hall--

			imgSofa = (ImageView) findViewById(R.id.imgsofa);
			imgTv = (ImageView) findViewById(R.id.imgtv);
			imghallcorner = (ImageView) findViewById(R.id.imghallcorner);
			imgHallChair1 = (ImageView) findViewById(R.id.imghallchair);
			imgHallChair2 = (ImageView) findViewById(R.id.imghallchair2);
			imghallbeanbag = (ImageView) findViewById(R.id.imghallbeanbag);
			imghallcornertable = (ImageView) findViewById(R.id.imghallcornertable);
			imgLRDining = (ImageView) findViewById(R.id.imglrdining);

			// Kitchen--

			imgDining = (ImageView) findViewById(R.id.imgdining);
			imgKitchenPlatform = (ImageView) findViewById(R.id.imgcabintekitchen);
			imgKitchenPlatform2 = (ImageView) findViewById(R.id.imgkitchenplatform2);
			rotateButton = (ImageView) findViewById(R.id.btn_rotate);

			btn_save = (ImageView) findViewById(R.id.btn_save);

			Bundle bundle = getIntent().getExtras();
			type_home = bundle.getString("type");

			// shared pref-
			sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
			Boolean flag = sharedpreferences.getBoolean("HelpFlag", false);
			if (flag.equals(false)) {

				sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
				final Dialog dialog = new Dialog(activity, android.R.style.Theme_Black_NoTitleBar);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

				dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
				dialog.setCancelable(false);
				dialog.setContentView(R.layout.designhomehelp);

				Button dialogButton = (Button) dialog.findViewById(R.id.btnhelpok);
				dialogButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {

						Editor editor = sharedpreferences.edit();
						editor.putBoolean("HelpFlag", true);
						editor.commit();

						dialog.dismiss();
					}
				});

				dialog.show();

			}

			designHomeService = new DesignHomeServiceImpl();

			// designHome = new DesignHome();
			ArrayList<DesignHome> designList = new ArrayList<DesignHome>();

			if (temp != 0) {

				designHome = (DesignHome) designHomeService.findById(temp);

			} else {

				designHome = new DesignHome();
				designList.clear();

				if (designList != null && designList.size() != 0) {

					for (int i = 0; i < designList.size(); i++) {

						temp = designList.get(i).getId();
						if (temp == 0 || temp == 1) {

							temp = ++temp;

						} else {

							temp = temp;

						}

					}

				} else {
					temp = 1;
				}
			}

			if (type_home.equals("hall")) {
				aLayouthall.setVisibility(View.VISIBLE);

				imgSofa.setVisibility(View.VISIBLE);
				// imgTv.setVisibility(View.VISIBLE);
				imghallbeanbag.setVisibility(View.VISIBLE);
				imgHallChair2.setVisibility(View.VISIBLE);
				imghallcorner.setVisibility(View.VISIBLE);

				try {

					if (bundle.getBoolean("2seatersofa") == true) {
						imgSofa.setImageResource(R.drawable.twoseatersofa);
						designHome.setHall2seatersofa(true);
						designHome.setLshapedsofa(false);
						designHome.setHall3seatersofa(false);

					} else if (bundle.getBoolean("lshapesofa") == true) {
						imgSofa.setImageResource(R.drawable.lsofa);
						imgHallChair1.setVisibility(View.INVISIBLE);
						designHome.setLshapedsofa(true);
						designHome.setHall2seatersofa(false);
						designHome.setHall3seatersofa(false);

					} else if (bundle.getBoolean("3seatersofa") == true) {
						imgSofa.setImageResource(R.drawable.threeseatersofa);
						designHome.setHall3seatersofa(true);
						designHome.setHall2seatersofa(false);
						designHome.setLshapedsofa(false);
					} else {
						// write code here for db if 1st time value-

						try {
							if (designHome.getHall2seatersofa() == true) {
								imgSofa.setImageResource(R.drawable.twoseatersofa);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {

							if (designHome.getHall3seatersofa() == true) {
								imgSofa.setImageResource(R.drawable.threeseatersofa);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {

							if (designHome.getLshapedsofa() == true) {
								imgSofa.setImageResource(R.drawable.lsofa);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					if (bundle.getBoolean("roundtable") == true) {
						imghallcorner.setVisibility(View.VISIBLE);
						imghallcorner.setImageResource(R.drawable.roundtable);
						designHome.setHallroundtable(true);
						designHome.setHallsquaretable(false);

					} else if (bundle.getBoolean("squaretable") == true) {
						imghallcorner.setVisibility(View.VISIBLE);
						imghallcorner.setImageResource(R.drawable.squaretable);
						designHome.setHallsquaretable(true);
						designHome.setHallroundtable(false);
					} else {

						try {

							if (designHome.getHallroundtable() == true) {
								imghallcorner.setVisibility(View.VISIBLE);
								imghallcorner.setImageResource(R.drawable.roundtable);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							if (designHome.getHallsquaretable() == true) {
								imghallcorner.setVisibility(View.VISIBLE);
								imghallcorner.setImageResource(R.drawable.squaretable);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("loungechair") == true) {
						imgHallChair2.setImageResource(R.drawable.loungechair);
						imgHallChair2.setVisibility(View.VISIBLE);
						designHome.setHallloungechair(true);
						designHome.setHallrecliner(false);
						designHome.setHallaccentchair(false);
					} else if (bundle.getBoolean("accentchair") == true) {
						imgHallChair2.setImageResource(R.drawable.accentchair);
						imgHallChair2.setVisibility(View.VISIBLE);
						designHome.setHallaccentchair(true);
						designHome.setHallloungechair(false);
						designHome.setHallrecliner(false);
					} else if (bundle.getBoolean("hallrecliner") == true) {
						imgHallChair2.setImageResource(R.drawable.recliner);
						imgHallChair2.setVisibility(View.VISIBLE);
						designHome.setHallrecliner(true);
						designHome.setHallaccentchair(false);
						designHome.setHallloungechair(false);
					}

					else {

						try {

							if (designHome.getHallloungechair() == true) {
								imgHallChair2.setVisibility(View.VISIBLE);
								imgHallChair2.setImageResource(R.drawable.loungechair);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						try {
							if (designHome.getHallaccentchair() == true) {
								imgHallChair2.setVisibility(View.VISIBLE);
								imgHallChair2.setImageResource(R.drawable.accentchair);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						try {
							if (designHome.getHallrecliner() == true) {
								imgHallChair2.setVisibility(View.VISIBLE);
								imgHallChair2.setImageResource(R.drawable.recliner);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

					if (bundle.getBoolean("hallsmallbeanbag") == true) {
						imghallbeanbag.setVisibility(View.VISIBLE);
						imghallbeanbag.setImageResource(R.drawable.beanbagsmall);
						designHome.setHallsmallbeanbag(true);
						designHome.setHalllargebeanbag(false);

					} else if (bundle.getBoolean("halllargebeanbag") == true) {
						imghallbeanbag.setVisibility(View.VISIBLE);
						imghallbeanbag.setImageResource(R.drawable.beanbaglarge);
						designHome.setHallsmallbeanbag(false);
						designHome.setHalllargebeanbag(true);
					} else {

						try {

							if (designHome.getHallsmallbeanbag() == true) {
								imghallbeanbag.setVisibility(View.VISIBLE);
								imghallbeanbag.setImageResource(R.drawable.beanbagsmall);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							if (designHome.getHalllargebeanbag() == true) {
								imghallbeanbag.setVisibility(View.VISIBLE);
								imghallbeanbag.setImageResource(R.drawable.beanbaglarge);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("tv") == true) {
						imgTv.setVisibility(View.VISIBLE);
						imgTv.setImageResource(R.drawable.tv);
						designHome.setHalltv(true);
					} else {

						try {
							if (designHome.getHalltv() == true && bundle.getBoolean("tv") == false) {

								imgTv.setVisibility(View.VISIBLE);
								imgTv.setImageResource(R.drawable.tv);
								designHome.setHalltv(true);

							} else if (bundle.getBoolean("tv") == false) {
								designHome.setHalltv(false);
								imgTv.setVisibility(View.INVISIBLE);
							} else {
								designHome.setHalltv(false);
								imgTv.setVisibility(View.INVISIBLE);
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("hallcornertable") == true) {
						imghallcornertable.setVisibility(View.VISIBLE);
						imghallcornertable.setImageResource(R.drawable.cornertable);
						designHome.setHallcornertable(true);
					} else {

						try {
							if (designHome.getHallcornertable() == true
									&& bundle.getBoolean("hallcornertable") == false) {
								imghallcornertable.setVisibility(View.VISIBLE);
								imghallcornertable.setImageResource(R.drawable.cornertable);
								designHome.setHallcornertable(true);
							} else if (bundle.getBoolean("hallcornertable") == false) {
								imghallcornertable.setVisibility(View.INVISIBLE);
								designHome.setHallcornertable(false);

							} else {
								imghallcornertable.setVisibility(View.INVISIBLE);
								designHome.setHallcornertable(false);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					// diningtable in Hall-
					try {

						sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
						Boolean flagKitchen = sharedpreferences.getBoolean("kitchendiningtable", false);

						if (flagKitchen.equals(true)) {

							imgLRDining.setVisibility(View.GONE);
						} else {

							if (bundle.getBoolean("4rounddining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.fourrounddining);
								designHome.setFourroundedining(true);
								designHome.setFourrectdining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixovaldining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrectdining(false);
								designHome.setEightrounddining(false);

							} else if (bundle.getBoolean("4rectdining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.rectfourseater);
								designHome.setFourrectdining(true);
								designHome.setFourroundedining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixovaldining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrectdining(false);
								designHome.setEightrounddining(false);
							} else if (bundle.getBoolean("4squaredining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.foursquaredining);
								designHome.setFoursquaredining(true);
								designHome.setFourroundedining(false);
								designHome.setFourrectdining(false);
								designHome.setSixovaldining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrectdining(false);
								designHome.setEightrounddining(false);
							} else if (bundle.getBoolean("6ovaldining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.sixovaldining);
								designHome.setSixovaldining(true);
								designHome.setFourroundedining(false);
								designHome.setFourrectdining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrectdining(false);
								designHome.setEightrounddining(false);
							} else if (bundle.getBoolean("6squaredining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.sixrectdining);
								designHome.setSixquaredining(true);
								designHome.setFourroundedining(false);
								designHome.setFourrectdining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixovaldining(false);
								designHome.setEightrectdining(false);
								designHome.setEightrounddining(false);
							} else if (bundle.getBoolean("8rectdining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.eightrectdining);
								designHome.setEightrectdining(true);
								designHome.setFourroundedining(false);
								designHome.setFourrectdining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixovaldining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrounddining(false);
							} else if (bundle.getBoolean("8rounddining") == true) {
								imgLRDining.setVisibility(View.VISIBLE);
								imgLRDining.setImageResource(R.drawable.eightrounddining);
								designHome.setEightrounddining(true);
								designHome.setFourroundedining(false);
								designHome.setFourrectdining(false);
								designHome.setFoursquaredining(false);
								designHome.setSixovaldining(false);
								designHome.setSixquaredining(false);
								designHome.setEightrectdining(false);
							} else {

								try {

									if (designHome.getFourrectdining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.rectfourseater);
									} else if (designHome.getFourroundedining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.fourrounddining);
									} else if (designHome.getFoursquaredining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.foursquaredining);
									} else if (designHome.getSixovaldining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.sixovaldining);
									} else if (designHome.getSixquaredining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.sixrectdining);
									} else if (designHome.getEightrectdining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.eightrectdining);
									} else if (designHome.getEightrounddining() == true) {
										imgLRDining.setVisibility(View.VISIBLE);
										imgLRDining.setImageResource(R.drawable.eightrounddining);
									}

								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					designHome.setId(temp);
					designHomeService.createOrUpdate(designHome);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			else if (type_home.equals("masterbed")) {

				aLayoutmasterbed.setVisibility(View.VISIBLE);

				img.setVisibility(View.VISIBLE);
				imgmbrchair.setVisibility(View.VISIBLE);

				try {
					if (bundle.getBoolean("mbrkingbed") == true) {
						img.setVisibility(View.VISIBLE);
						img.setImageResource(R.drawable.kingbed);
						designHome.setMbrkingBed(true);
						designHome.setMbrqueenBed(false);
						designHome.setMbrbunkbed(false);
					} else if (bundle.getBoolean("mbrqueenbed") == true) {
						img.setVisibility(View.VISIBLE);
						img.setImageResource(R.drawable.queenbed);
						designHome.setMbrqueenBed(true);
						designHome.setMbrkingBed(false);
						designHome.setMbrbunkbed(false);
					} else if (bundle.getBoolean("mbrbunkbed") == true) {
						img.setVisibility(View.VISIBLE);
						img.setImageResource(R.drawable.bunkbed);
						designHome.setMbrbunkbed(true);
						designHome.setMbrqueenBed(false);
						designHome.setMbrkingBed(false);
					}

					else {
						try {

							if (designHome.getMbrkingBed() == true) {
								img.setVisibility(View.VISIBLE);
								img.setImageResource(R.drawable.kingbed);
							} else if (designHome.getMbrqueenBed() == true) {
								img.setVisibility(View.VISIBLE);
								img.setImageResource(R.drawable.queenbed);
							} else if (designHome.getMbrbunkbed() == true) {
								img.setVisibility(View.VISIBLE);
								img.setImageResource(R.drawable.bunkbed);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrtv") == true) {
						imgmbrtv.setVisibility(View.VISIBLE);
						imgmbrtv.setImageResource(R.drawable.tv);
						designHome.setMbrtv(true);
					} else {
						try {
							if (designHome.getMbrtv() == true && bundle.getBoolean("mbrtv") == false) {
								imgmbrtv.setVisibility(View.VISIBLE);
								imgmbrtv.setImageResource(R.drawable.tv);
								designHome.setMbrtv(true);
							} else if (bundle.getBoolean("mbrtv") == false) {
								imgmbrtv.setVisibility(View.INVISIBLE);
								designHome.setMbrtv(false);
							} else {
								imgmbrtv.setVisibility(View.INVISIBLE);
								designHome.setMbrtv(false);
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrloungechair") == true) {
						imgmbrchair.setVisibility(View.VISIBLE);
						imgmbrchair.setImageResource(R.drawable.loungechair);
						designHome.setMbrloungechair(true);
						designHome.setMbraccentchair(false);
						designHome.setMbrrecliner(false);
					} else if (bundle.getBoolean("mbraccentchair") == true) {
						imgmbrchair.setVisibility(View.VISIBLE);
						imgmbrchair.setImageResource(R.drawable.accentchair);
						designHome.setMbraccentchair(true);
						designHome.setMbrloungechair(false);
						designHome.setMbrrecliner(false);
					} else if (bundle.getBoolean("mbrrecliner") == true) {
						imgmbrchair.setVisibility(View.VISIBLE);
						imgmbrchair.setImageResource(R.drawable.recliner);
						designHome.setMbrrecliner(true);
						designHome.setMbraccentchair(false);
						designHome.setMbrloungechair(false);
					} else {
						try {

							if (designHome.getMbrloungechair() == true) {
								imgmbrchair.setVisibility(View.VISIBLE);
								imgmbrchair.setImageResource(R.drawable.loungechair);
							} else if (designHome.getMbraccentchair() == true) {
								imgmbrchair.setVisibility(View.VISIBLE);
								imgmbrchair.setImageResource(R.drawable.accentchair);
							} else if (designHome.getMbrrecliner() == true) {
								imgmbrchair.setVisibility(View.VISIBLE);
								imgmbrchair.setImageResource(R.drawable.recliner);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrsidetable") == true) {
						imgmbrsidebox2.setVisibility(View.VISIBLE);
						imgmbrsidebox2.setImageResource(R.drawable.bedsidebox);
						designHome.setMbrsideTable(true);
					} else {
						try {
							if (designHome.getMbrsideTable() == true && bundle.getBoolean("mbrsidetable") == false) {
								imgmbrsidebox2.setVisibility(View.VISIBLE);
								imgmbrsidebox2.setImageResource(R.drawable.bedsidebox);
								designHome.setMbrsideTable(true);
							} else if (bundle.getBoolean("mbrsidetable") == false) {
								imgmbrsidebox2.setVisibility(View.INVISIBLE);
								designHome.setMbrsideTable(false);
							} else {
								imgmbrsidebox2.setVisibility(View.INVISIBLE);
								designHome.setMbrsideTable(false);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrstudytable") == true) {
						imgmbrsidebox.setVisibility(View.VISIBLE);
						imgmbrsidebox.setImageResource(R.drawable.studytable);
						designHome.setMbrstudytable(true);
					} else {
						try {
							if (designHome.getMbrstudytable() == true && bundle.getBoolean("mbrstudytable") == false) {
								imgmbrsidebox.setVisibility(View.VISIBLE);
								imgmbrsidebox.setImageResource(R.drawable.studytable);
								designHome.setMbrstudytable(true);
							} else if (bundle.getBoolean("mbrstudytable") == false) {
								imgmbrsidebox.setVisibility(View.INVISIBLE);
								designHome.setMbrstudytable(false);
							} else {
								imgmbrsidebox.setVisibility(View.INVISIBLE);
								designHome.setMbrstudytable(false);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrcoffeetable") == true) {
						imgmbrcorner.setVisibility(View.VISIBLE);
						imgmbrcorner.setImageResource(R.drawable.coffeetable);
						designHome.setMbrcoffeetable(true);
					} else {
						try {
							if (designHome.getMbrcoffeetable() == true
									&& bundle.getBoolean("mbrcoffeetable") == false) {
								imgmbrcorner.setVisibility(View.VISIBLE);
								imgmbrcorner.setImageResource(R.drawable.coffeetable);
								designHome.setMbrcoffeetable(true);
							} else if (bundle.getBoolean("mbrcoffeetable") == false) {
								imgmbrcorner.setVisibility(View.INVISIBLE);
								designHome.setMbrcoffeetable(false);
							} else {
								imgmbrcorner.setVisibility(View.INVISIBLE);
								designHome.setMbrcoffeetable(false);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (bundle.getBoolean("mbrwardrobe") == true) {
						imgmbrWardrobe.setVisibility(View.VISIBLE);
						imgmbrWardrobe.setImageResource(R.drawable.wardrobe);
						designHome.setMbrwrdrobe(true);
					} else {
						try {
							if (designHome.getMbrwrdrobe() == true && bundle.getBoolean("mbrwardrobe") == false) {
								imgmbrWardrobe.setVisibility(View.VISIBLE);
								imgmbrWardrobe.setImageResource(R.drawable.wardrobe);
								designHome.setMbrwrdrobe(true);
							} else if (bundle.getBoolean("mbrwardrobe") == false) {
								imgmbrWardrobe.setVisibility(View.INVISIBLE);
								designHome.setMbrwrdrobe(false);
							} else {
								imgmbrWardrobe.setVisibility(View.INVISIBLE);
								designHome.setMbrwrdrobe(false);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					designHome.setId(temp);
					designHomeService.createOrUpdate(designHome);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (type_home.equals("smallbed")) {

				// BedRoom Items-

				aLayoutsmallbed.setVisibility(View.VISIBLE);
				imgSmallBed.setVisibility(View.VISIBLE);
				imgsbrchair.setVisibility(View.VISIBLE);
				try {
					if (bundle.getBoolean("sbrkingbed") == true) {
						imgSmallBed.setVisibility(View.VISIBLE);
						imgSmallBed.setImageResource(R.drawable.sbrkingbed);
						designHome.setSmallBrkingBed(true);
						designHome.setSmallBrqueenBed(false);
					} else if (bundle.getBoolean("sbrqueenbed") == true) {
						imgSmallBed.setVisibility(View.VISIBLE);
						imgSmallBed.setImageResource(R.drawable.sbrqueenbed);
						designHome.setSmallBrqueenBed(true);
						designHome.setSmallBrkingBed(false);
					} else {

						try {

							if (designHome.getSmallBrkingBed() == true) {
								imgSmallBed.setVisibility(View.VISIBLE);
								imgSmallBed.setImageResource(R.drawable.sbrkingbed);
							} else if (designHome.getSmallBrqueenBed() == true) {
								imgSmallBed.setVisibility(View.VISIBLE);
								imgSmallBed.setImageResource(R.drawable.sbrqueenbed);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					if (bundle.getBoolean("sbrloungechair") == true) {
						imgsbrchair.setVisibility(View.VISIBLE);
						imgsbrchair.setImageResource(R.drawable.loungechair);
						designHome.setSmallBrloungechair(true);
						designHome.setSmallBraccentchair(false);
					} else if (bundle.getBoolean("sbraccentchair") == true) {
						imgsbrchair.setVisibility(View.VISIBLE);
						imgsbrchair.setImageResource(R.drawable.accentchair);
						designHome.setSmallBraccentchair(true);
						designHome.setSmallBrloungechair(false);

					} else {

						try {

							if (designHome.getSmallBrloungechair() == true) {
								imgsbrchair.setVisibility(View.VISIBLE);
								imgsbrchair.setImageResource(R.drawable.loungechair);
							} else if (designHome.getSmallBraccentchair() == true) {
								imgsbrchair.setVisibility(View.VISIBLE);
								imgsbrchair.setImageResource(R.drawable.accentchair);

							}
						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					if (bundle.getBoolean("sbrsidetable") == true) {
						imgsbrsidebox.setVisibility(View.VISIBLE);
						imgsbrsidebox.setImageResource(R.drawable.bedsidebox);
						designHome.setSmallBrsideTable(true);
					} else {

						try {
							if (designHome.getSmallBrsideTable() == true
									&& bundle.getBoolean("sbrsidetable") == false) {
								imgsbrsidebox.setVisibility(View.VISIBLE);
								imgsbrsidebox.setImageResource(R.drawable.bedsidebox);
								designHome.setSmallBrsideTable(true);
							} else if (bundle.getBoolean("sbrsidetable") == false) {
								imgsbrsidebox.setVisibility(View.INVISIBLE);
								designHome.setSmallBrsideTable(false);
							} else {
								imgsbrsidebox.setVisibility(View.INVISIBLE);
								designHome.setSmallBrsideTable(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					if (bundle.getBoolean("sbrstudytable") == true) {
						imgsbrstudytable.setVisibility(View.VISIBLE);
						imgsbrstudytable.setImageResource(R.drawable.studytable);
						designHome.setSmallBrstudytable(true);
					} else {
						try {
							if (designHome.getSmallBrstudytable() == true
									&& bundle.getBoolean("sbrstudytable") == false) {
								imgsbrstudytable.setVisibility(View.VISIBLE);
								imgsbrstudytable.setImageResource(R.drawable.studytable);
								designHome.setSmallBrstudytable(true);
							} else if (bundle.getBoolean("sbrstudytable") == false) {
								imgsbrstudytable.setVisibility(View.INVISIBLE);
								designHome.setSmallBrstudytable(false);
							} else {
								imgsbrstudytable.setVisibility(View.INVISIBLE);
								designHome.setSmallBrstudytable(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					designHome.setId(temp);
					designHomeService.createOrUpdate(designHome);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (type_home.equals("kitchen")) {

				aLayoutKitchen.setVisibility(View.VISIBLE);
				// imgKitchenPlatform.setVisibility(View.VISIBLE);
				/*
				 * designHome.setCabinetkitchen(true);
				 * designHomeService.createOrUpdate(designHome);
				 */
				// imgDining.setVisibility(View.VISIBLE);
				try {

					sharedpreferences = getSharedPreferences("Help", Context.MODE_PRIVATE);
					Boolean flaghalldining = sharedpreferences.getBoolean("halldiningtable", false);

					if (bundle.getBoolean("CabinetKitchen") == true) {
						imgKitchenPlatform.setVisibility(View.VISIBLE);
						imgKitchenPlatform.setImageResource(R.drawable.cabinetkitchen);
						designHome.setCabinetkitchen(true);
					} else {
						try {

							if (designHome.getCabinetkitchen() == true
									&& bundle.getBoolean("CabinetKitchen") == false) {
								imgKitchenPlatform.setVisibility(View.VISIBLE);
								imgKitchenPlatform.setImageResource(R.drawable.cabinetkitchen);
								designHome.setCabinetkitchen(true);
							} else if (bundle.getBoolean("CabinetKitchen") == false) {
								imgKitchenPlatform.setVisibility(View.INVISIBLE);
								designHome.setCabinetkitchen(false);
							} else {
								imgKitchenPlatform.setVisibility(View.INVISIBLE);
								designHome.setCabinetkitchen(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					}
					//

					if (bundle.getBoolean("IslandKitchen") == true) {
						imgKitchenPlatform2.setVisibility(View.VISIBLE);
						imgKitchenPlatform2.setImageResource(R.drawable.islandkitchen);
						designHome.setIslandkitchen(true);
					} else {
						try {

							if (designHome.getIslandkitchen() == true && bundle.getBoolean("IslandKitchen") == false) {
								imgKitchenPlatform2.setVisibility(View.VISIBLE);
								imgKitchenPlatform2.setImageResource(R.drawable.islandkitchen);
								designHome.setIslandkitchen(true);
							} else if (bundle.getBoolean("IslandKitchen") == false) {
								imgKitchenPlatform2.setVisibility(View.INVISIBLE);
								designHome.setIslandkitchen(false);
							} else {
								imgKitchenPlatform2.setVisibility(View.INVISIBLE);
								designHome.setIslandkitchen(false);
							}

						} catch (Exception e) {
							e.printStackTrace();
						}

					}

					if (flaghalldining.equals(true)) {
						imgDining.setVisibility(View.GONE);
					} else {

						if (bundle.getBoolean("4rounddining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.fourrounddining);
							designHome.setFourroundedining(true);
							designHome.setFourrectdining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixovaldining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrectdining(false);
							designHome.setEightrounddining(false);

						} else if (bundle.getBoolean("4rectdining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.rectfourseater);
							designHome.setFourrectdining(true);
							designHome.setFourroundedining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixovaldining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrectdining(false);
							designHome.setEightrounddining(false);
						} else if (bundle.getBoolean("4squaredining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.foursquaredining);
							designHome.setFoursquaredining(true);
							designHome.setFourroundedining(false);
							designHome.setFourrectdining(false);
							designHome.setSixovaldining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrectdining(false);
							designHome.setEightrounddining(false);
						} else if (bundle.getBoolean("6ovaldining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.sixovaldining);
							designHome.setSixovaldining(true);
							designHome.setFourroundedining(false);
							designHome.setFourrectdining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrectdining(false);
							designHome.setEightrounddining(false);
						} else if (bundle.getBoolean("6squaredining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.sixrectdining);
							designHome.setSixquaredining(true);
							designHome.setFourroundedining(false);
							designHome.setFourrectdining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixovaldining(false);
							designHome.setEightrectdining(false);
							designHome.setEightrounddining(false);
						} else if (bundle.getBoolean("8rectdining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.eightrectdining);
							designHome.setEightrectdining(true);
							designHome.setFourroundedining(false);
							designHome.setFourrectdining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixovaldining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrounddining(false);
						} else if (bundle.getBoolean("8rounddining") == true) {
							imgDining.setVisibility(View.VISIBLE);
							imgDining.setImageResource(R.drawable.eightrounddining);
							designHome.setEightrounddining(true);
							designHome.setFourroundedining(false);
							designHome.setFourrectdining(false);
							designHome.setFoursquaredining(false);
							designHome.setSixovaldining(false);
							designHome.setSixquaredining(false);
							designHome.setEightrectdining(false);
						} else {

							try {

								if (designHome.getFourrectdining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.rectfourseater);
								} else if (designHome.getFourroundedining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.fourrounddining);
								} else if (designHome.getFoursquaredining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.foursquaredining);
								} else if (designHome.getSixovaldining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.sixovaldining);
								} else if (designHome.getSixquaredining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.sixrectdining);
								} else if (designHome.getEightrectdining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.eightrectdining);
								} else if (designHome.getEightrounddining() == true) {
									imgDining.setVisibility(View.VISIBLE);
									imgDining.setImageResource(R.drawable.eightrounddining);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					designHome.setId(temp);
					designHomeService.createOrUpdate(designHome);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (type_home.equals("gallery")) {

				aLayoutGallery.setVisibility(View.VISIBLE);

			}

			spinner = (LinearLayout) findViewById(R.id.llayoutspinner);

			spinner.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					try {
						Intent intent = new Intent(DesginHomeActivity2.this, HomeItemSelctActivity.class);
						intent.putExtra("HomeType", type_home);
						startActivity(intent);
						finish();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			btn_save.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {

					try {
						designHomeService = new DesignHomeServiceImpl();
						designHome = new DesignHome();
						bmpImg = getAllViewItems();
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bmpImg = getResizedBitmap(bmpImg, 140);
						bmpImg.compress(Bitmap.CompressFormat.PNG, 100, stream);
						byteArray = stream.toByteArray();

						Intent intent = new Intent();
						intent.putExtra("Image1", byteArray);
						setResult(22, intent);
						finish();

					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			rotateButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						rotateImage();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			// Master Bedroom Img Click-

			img.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					status = 1;
					masterBrRotateItems(true, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			imgmbrsidebox.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrsidebox = 1;

					masterBrRotateItems(false, false, true, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			imgmbrsidebox2.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrsidebox2 = 1;

					masterBrRotateItems(false, false, false, true, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});
			imgmbrtv.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrtv = 1;

					masterBrRotateItems(false, false, false, false, true, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});
			imgmbrWardrobe.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrWardrobe = 1;

					masterBrRotateItems(false, false, false, false, false, false, true);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			imgmbrcorner.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrcorner = 1;

					masterBrRotateItems(false, true, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			imgmbrchair.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statusmbrchair = 1;

					masterBrRotateItems(false, false, false, false, false, true, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			/// Small BedRooms Items-

			imgSmallBed.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					cornerflag = 1;
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(true, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					tvrotateflag = false;
					sofarotateflag = false;

					return false;
				}
			});

			imgsbrsidebox.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statussbrsidebox = 1;
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, true, false, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					tvrotateflag = false;
					sofarotateflag = false;

					return false;
				}
			});

			imgsbrstudytable.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statussbrsofa = 1;
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, true, false);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});
			imgsbrchair.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statussbrchair = 1;
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, true);
					hallRotateItems(false, false, false, false, false, false, false, false);

					return false;
				}
			});

			// Hall--

			imgSofa.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					hallSofaFlag = 1;
					hallRotateItems(true, false, false, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);

					return false;
				}
			});

			imgTv.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					tvflag = 1;
					Log.i("ImageStatus", "" + status);
					hallRotateItems(false, false, false, true, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);

					return false;

				}
			});

			imghallcorner.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					hallcornerflag = 1;
					hallRotateItems(false, false, false, false, true, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					return false;
				}
			});

			imgHallChair1.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					hallChair1flag = 1;
					hallRotateItems(false, true, false, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					return false;
				}
			});

			imgHallChair2.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					hallChair2Flag = 1;
					hallRotateItems(false, false, true, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					return false;
				}
			});

			imghallbeanbag.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statushallbeanbag = 1;

					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, true, false);

					return false;
				}
			});

			imghallcornertable.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statushallcornertable = 1;

					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, true, false, false);

					return false;
				}
			});

			imgLRDining.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statushalldining = 1;

					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);
					hallRotateItems(false, false, false, false, false, false, false, true);

					return false;
				}
			});

			// Kitchen--

			imgDining.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					kitchenFlag = 1;
					kitchenrotatediningtable = true;
					kitchenrotateplatform = false;
					kitchenrotateplatform2 = false;
					hallRotateItems(false, false, false, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);

					return false;
				}
			});

			imgKitchenPlatform.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statuskitchenplatform = 1;
					kitchenrotateplatform = true;
					kitchenrotatediningtable = false;
					kitchenrotateplatform2 = false;

					hallRotateItems(false, false, false, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);

					return false;
				}
			});
			imgKitchenPlatform2.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {

					statuskitchenplatform2 = 1;
					kitchenrotateplatform2 = true;
					kitchenrotateplatform = false;
					kitchenrotatediningtable = false;

					hallRotateItems(false, false, false, false, false, false, false, false);
					masterBrRotateItems(false, false, false, false, false, false, false);
					smallBrRotateItems(false, false, false, false);

					return false;
				}
			});

			aLayoutmasterbed.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Log.i("touch", "" + event);
					try {

						if (status == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - img.getWidth() / 2, (int) event.getY() - img.getHeight() / 2);
							img.setLayoutParams(lp);

						}
						if (statusmbrsidebox == 1) // any event from down and
													// move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrsidebox.getWidth() / 2,
									(int) event.getY() - imgmbrsidebox.getHeight() / 2);
							imgmbrsidebox.setLayoutParams(lp);

						}
						if (statusmbrsidebox2 == 1) // any event from down and
													// move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrsidebox2.getWidth() / 2,
									(int) event.getY() - imgmbrsidebox2.getHeight() / 2);
							imgmbrsidebox2.setLayoutParams(lp);

						}

						if (statusmbrcorner == 1) // any event from down and
													// move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrcorner.getWidth() / 2,
									(int) event.getY() - imgmbrcorner.getHeight() / 2);
							imgmbrcorner.setLayoutParams(lp);

						}

						if (statusmbrtv == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrtv.getWidth() / 2,
									(int) event.getY() - imgmbrtv.getHeight() / 2);
							imgmbrtv.setLayoutParams(lp);

						}
						if (statusmbrchair == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrchair.getWidth() / 2,
									(int) event.getY() - imgmbrchair.getHeight() / 2);
							imgmbrchair.setLayoutParams(lp);

						}
						if (statusmbrWardrobe == 1) // any event from down and
													// move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgmbrWardrobe.getWidth() / 2,
									(int) event.getY() - imgmbrWardrobe.getHeight() / 2);
							imgmbrWardrobe.setLayoutParams(lp);

						}

						if (event.getAction() == MotionEvent.ACTION_UP) {
							status = 0;
							statusmbrsidebox = 0;
							statusmbrsidebox2 = 0;
							statusmbrcorner = 0;
							statusmbrtv = 0;
							statusmbrchair = 0;
							statusmbrWardrobe = 0;
							img.setBackgroundColor(Color.TRANSPARENT);
							imgmbrsidebox.setBackgroundColor(Color.TRANSPARENT);
							imgmbrsidebox2.setBackgroundColor(Color.TRANSPARENT);
							imgmbrcorner.setBackgroundColor(Color.TRANSPARENT);
							imgmbrtv.setBackgroundColor(Color.TRANSPARENT);
							imgmbrchair.setBackgroundColor(Color.TRANSPARENT);
							imgmbrWardrobe.setBackgroundColor(Color.TRANSPARENT);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;

				}
			});

			aLayouthall.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Log.i("touch", "" + event);
					try {

						if (hallSofaFlag == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgSofa.getWidth() / 2,
									(int) event.getY() - imgSofa.getHeight() / 2);
							imgSofa.setLayoutParams(lp);

						}
						if (tvflag == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgTv.getWidth() / 2,
									(int) event.getY() - imgTv.getHeight() / 2);
							imgTv.setLayoutParams(lp);

						}
						if (hallcornerflag == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imghallcorner.getWidth() / 2,
									(int) event.getY() - imghallcorner.getHeight() / 2);
							imghallcorner.setLayoutParams(lp);

						}
						if (hallChair1flag == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgHallChair1.getWidth() / 2,
									(int) event.getY() - imgHallChair1.getHeight() / 2);
							imgHallChair1.setLayoutParams(lp);

						}

						if (hallChair2Flag == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgHallChair2.getWidth() / 2,
									(int) event.getY() - imgHallChair2.getHeight() / 2);
							imgHallChair2.setLayoutParams(lp);

						}
						if (statushallbeanbag == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imghallbeanbag.getWidth() / 2,
									(int) event.getY() - imghallbeanbag.getHeight() / 2);
							imghallbeanbag.setLayoutParams(lp);

						}
						if (statushallcornertable == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imghallcornertable.getWidth() / 2,
									(int) event.getY() - imghallcornertable.getHeight() / 2);
							imghallcornertable.setLayoutParams(lp);

						}
						if (statushalldining == 1) {
							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgLRDining.getWidth() / 2,
									(int) event.getY() - imgLRDining.getHeight() / 2);
							imgLRDining.setLayoutParams(lp);

						}

						if (event.getAction() == MotionEvent.ACTION_UP) {
							hallSofaFlag = 0;
							tvflag = 0;
							hallcornerflag = 0;
							hallChair1flag = 0;
							hallChair2Flag = 0;
							statushallbeanbag = 0;
							statushallcornertable = 0;
							statushalldining = 0;
							imgSofa.setBackgroundColor(Color.TRANSPARENT);
							imgTv.setBackgroundColor(Color.TRANSPARENT);
							imghallcorner.setBackgroundColor(Color.TRANSPARENT);
							imgHallChair1.setBackgroundColor(Color.TRANSPARENT);
							imgHallChair2.setBackgroundColor(Color.TRANSPARENT);
							imghallbeanbag.setBackgroundColor(Color.TRANSPARENT);
							imghallcornertable.setBackgroundColor(Color.TRANSPARENT);
							imgLRDining.setBackgroundColor(Color.TRANSPARENT);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;

				}
			});

			aLayoutsmallbed.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub
					Log.i("touch", "" + event);
					try {

						if (cornerflag == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgSmallBed.getWidth() / 2,
									(int) event.getY() - imgSmallBed.getHeight() / 2);
							imgSmallBed.setLayoutParams(lp);

						}

						if (statussbrsidebox == 1) // any event from down and
													// move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgsbrsidebox.getWidth() / 2,
									(int) event.getY() - imgsbrsidebox.getHeight() / 2);
							imgsbrsidebox.setLayoutParams(lp);

						}
						if (statussbrsofa == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgsbrstudytable.getWidth() / 2,
									(int) event.getY() - imgsbrstudytable.getHeight() / 2);
							imgsbrstudytable.setLayoutParams(lp);

						}
						if (statussbrchair == 1) // any event from down and move
						{

							LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
									(int) event.getX() - imgsbrchair.getWidth() / 2,
									(int) event.getY() - imgsbrchair.getHeight() / 2);
							imgsbrchair.setLayoutParams(lp);

						}

						if (event.getAction() == MotionEvent.ACTION_UP) {
							cornerflag = 0;
							statussbrsidebox = 0;
							statussbrsofa = 0;
							statussbrchair = 0;
							imgSmallBed.setBackgroundColor(Color.TRANSPARENT);
							imgsbrsidebox.setBackgroundColor(Color.TRANSPARENT);
							imgsbrstudytable.setBackgroundColor(Color.TRANSPARENT);
							imgsbrchair.setBackgroundColor(Color.TRANSPARENT);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				}
			});

			aLayoutKitchen.setOnTouchListener(new OnTouchListener() {

				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// TODO Auto-generated method stub

					if (kitchenFlag == 1) // any event from down and move
					{

						LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
								(int) event.getX() - imgDining.getWidth() / 2,
								(int) event.getY() - imgDining.getHeight() / 2);
						imgDining.setLayoutParams(lp);

					}
					if (statuskitchenplatform == 1) // any event from down and
													// move
					{

						LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
								(int) event.getX() - imgKitchenPlatform.getWidth() / 2,
								(int) event.getY() - imgKitchenPlatform.getHeight() / 2);
						imgKitchenPlatform.setLayoutParams(lp);

					}
					if (statuskitchenplatform2 == 1) // any event from down and
					// move
					{

						LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT,
								(int) event.getX() - imgKitchenPlatform2.getWidth() / 2,
								(int) event.getY() - imgKitchenPlatform2.getHeight() / 2);
						imgKitchenPlatform2.setLayoutParams(lp);

					}
					if (event.getAction() == MotionEvent.ACTION_UP) {
						kitchenFlag = 0;
						statuskitchenplatform = 0;
						statuskitchenplatform2 = 0;
						imgDining.setBackgroundColor(Color.TRANSPARENT);
						imgKitchenPlatform.setBackgroundColor(Color.TRANSPARENT);
						imgKitchenPlatform2.setBackgroundColor(Color.TRANSPARENT);
					}

					return true;

				}
			});

		} catch (Exception e)

		{
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	// Rotate Flags for Master Bedroom items-
	private void masterBrRotateItems(Boolean bed, Boolean corner, Boolean sideBox, Boolean sideBox2, Boolean tv,
			Boolean chair, Boolean wardrobe) {

		bedFlag = bed;
		mbrrotatecorner = corner;
		mbrrotatesidebox = sideBox;
		mbrrotatesidebox2 = sideBox2;
		mbrrotatetv = tv;
		mbrrotatechair = chair;
		mbrrotatewardrobe = wardrobe;

	}

	private void smallBrRotateItems(Boolean bed, Boolean sidebox, Boolean sofa, Boolean chair) {

		cornerrotatebedroom = bed;
		sbrrotatesidebox = sidebox;
		sbrrotatesofa = sofa;
		sbrrotatechair = chair;

	}

	private void hallRotateItems(Boolean sofa, Boolean chair1, Boolean chair2, Boolean tv, Boolean corner,
			Boolean cornertable, Boolean beanbag, Boolean halldining) {
		sofarotateflag = sofa;
		tvrotateflag = tv;
		hallrotatecorner = corner;
		hallrotatechair1 = chair1;
		hallrotatechair2 = chair2;
		hallrotatecornertable = cornertable;
		hallrotatebeanbag = beanbag;
		hallrotatediningtable = halldining;
	}

	Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 0) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}

	// check to convert imageview to bitmap

	public Bitmap getAllViewItems() {

		Bitmap bm = null;
		FrameLayout savedImage = null;
		savedImage = (FrameLayout) findViewById(R.id.framelayout2);
		savedImage.setDrawingCacheEnabled(true);
		savedImage.buildDrawingCache();
		bm = savedImage.getDrawingCache();

		Bitmap b3 = viewToBitmap(savedImage);
		return b3;

	}

	public Bitmap viewToBitmap(View view) {
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);

		Bitmap mb = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		Canvas c = new Canvas(mb);

		Paint p = new Paint();
		p.setARGB(0, 0, 0, 0); // ARGB for the color, in this case red
		int removeColor = p.getColor(); // store this color's int for later use

		p.setAlpha(0);

		p.setXfermode(new AvoidXfermode(removeColor, 0, AvoidXfermode.Mode.TARGET));

		// draw transparent on the "brown" pixels
		c.drawPaint(p);

		view.draw(c);
		return mb;

	}

	private void animate(double fromDegrees, double toDegrees, long durationMillis, ImageView img) {
		final RotateAnimation rotate = new RotateAnimation((float) fromDegrees, (float) toDegrees,
				RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
		rotate.setDuration(durationMillis);
		rotate.setFillEnabled(true);
		rotate.setFillAfter(true);
		img.startAnimation(rotate);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			finish();
			byteArray = null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		iv = (ImageView) v;
		// int ID = iv.getId();

		return false;
	}

	private void rotateImage() {

		try {
			if (bedFlag.equals(true)) {
				img.setRotation(img.getRotation() + 90);
			} else if (tvrotateflag.equals(true)) {
				imgTv.setRotation(imgTv.getRotation() + 90);
			} else if (sofarotateflag.equals(true)) {
				imgSofa.setRotation(imgSofa.getRotation() + 90);
			} else if (cornerrotatebedroom.equals(true)) {
				imgSmallBed.setRotation(imgSmallBed.getRotation() + 90);
			} else if (mbrrotatecorner.equals(true)) {
				imgmbrcorner.setRotation(imgmbrcorner.getRotation() + 90);
			} else if (mbrrotatesidebox.equals(true)) {
				imgmbrsidebox.setRotation(imgmbrsidebox.getRotation() + 90);
			} else if (mbrrotatesidebox2.equals(true)) {
				imgmbrsidebox2.setRotation(imgmbrsidebox2.getRotation() + 90);
			} else if (mbrrotatetv.equals(true)) {
				imgmbrtv.setRotation(imgmbrtv.getRotation() + 90);
			} else if (mbrrotatechair.equals(true)) {
				imgmbrchair.setRotation(imgmbrchair.getRotation() + 90);
			} else if (mbrrotatewardrobe.equals(true)) {
				imgmbrWardrobe.setRotation(imgmbrWardrobe.getRotation() + 90);
			}

			else if (sbrrotatesidebox.equals(true)) {
				imgsbrsidebox.setRotation(imgsbrsidebox.getRotation() + 90);
			} else if (sbrrotatesofa.equals(true)) {
				imgsbrstudytable.setRotation(imgsbrstudytable.getRotation() + 90);
			} else if (sbrrotatechair.equals(true)) {
				imgsbrchair.setRotation(imgsbrchair.getRotation() + 90);
			} else if (hallrotatecorner.equals(true)) {
				imghallcorner.setRotation(imghallcorner.getRotation() + 90);
			} else if (hallrotatechair1.equals(true)) {
				imgHallChair1.setRotation(imgHallChair1.getRotation() + 90);
			} else if (hallrotatechair2.equals(true)) {
				imgHallChair2.setRotation(imgHallChair2.getRotation() + 90);
			} else if (hallrotatebeanbag.equals(true)) {
				imghallbeanbag.setRotation(imghallbeanbag.getRotation() + 90);
			} else if (hallrotatecornertable.equals(true)) {
				imghallcornertable.setRotation(imghallcornertable.getRotation() + 90);
			} else if (hallrotatediningtable.equals(true)) {
				imgLRDining.setRotation(imgLRDining.getRotation() + 90);
			} else if (kitchenrotatediningtable.equals(true)) {
				imgDining.setRotation(imgDining.getRotation() + 90);
			} else if (kitchenrotateplatform.equals(true)) {
				imgKitchenPlatform.setRotation(imgKitchenPlatform.getRotation() + 90);
			} else if (kitchenrotateplatform2.equals(true)) {
				imgKitchenPlatform2.setRotation(imgKitchenPlatform2.getRotation() + 90);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		try {
			Intent intent = getIntent();
			finish();
			startActivity(intent);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}