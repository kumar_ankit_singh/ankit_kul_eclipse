package com.kumarbuilders.propertyapp.activity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.PopupMenu;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.NavDrawerListAdapter;
import com.kumarbuilders.propertyapp.domain.ComparePropertyServerList;
import com.kumarbuilders.propertyapp.domain.NavDrawerItem;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.fragments.About;
import com.kumarbuilders.propertyapp.fragments.AboutKulFragment;
import com.kumarbuilders.propertyapp.fragments.ChatFragment;
import com.kumarbuilders.propertyapp.fragments.CustomerFavPropertyFragment;
import com.kumarbuilders.propertyapp.fragments.EMI;
import com.kumarbuilders.propertyapp.fragments.EnquiryFragment;
import com.kumarbuilders.propertyapp.fragments.FilterFragment;
import com.kumarbuilders.propertyapp.fragments.FutureRequirement;
import com.kumarbuilders.propertyapp.fragments.MyKul;
import com.kumarbuilders.propertyapp.fragments.TabFragmentnew;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.ComparePropertyServerListService;
import com.kumarbuilders.propertyapp.ormservices.ComparePropertyServerListServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

public class ProjectListNewActivity extends FragmentActivity implements TaskResponse {

	private DrawerLayout mDrawerLayout;

	private ListView mDrawerList;

	private ActionBarDrawerToggle mDrawerToggle;
	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;
	private JSONObject jsonobject;
	private PropertyService propertyService;
	private PropertiesDetails propertyDetails;
	private Boolean login_flag = false, logout_flag = false;
	private String kulUserId;
	public static int count = 0;
	public AddSignUpToDbImpl signupData;
	private UserSignUp signUpInfo;
	private String signupUserId;
	private ComparePropertyServerListService comparePropertyServerListService;
	private Bundle extras, extras2;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {

			setContentView(R.layout.drawerlayout);

			try {
				extras = getIntent().getExtras();
				getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Home" + "</font>"));

				mTitle = mDrawerTitle = getTitle();

				// load slide menu items

				navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items); //

				// nav drawer icons from resources
				navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

				mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
				mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

				navDrawerItems = new ArrayList<NavDrawerItem>();

				// adding nav drawer items to array
				// Home

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
				// Performance
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
				navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));

				navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));

				// Recycle the typed array
				navMenuIcons.recycle();

				mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

				// setting the nav drawer list adapter
				adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
				mDrawerList.setAdapter(adapter);

				// enabling action bar app icon and behaving it as toggle button
				getActionBar().setDisplayHomeAsUpEnabled(true);
				getActionBar().setHomeButtonEnabled(true);
				getActionBar().setDisplayShowHomeEnabled(true);

				getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

				ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#539fa8"));
				getActionBar().setBackgroundDrawable(colorDrawable);

				// getActionBar().setIcon(null);

				mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, // nav
																										// menu
																										// toggle
																										// icon
						R.string.appicon, // nav drawer open - description for
											// accessibility
						R.string.appicon // nav drawer close - description for
											// accessibility
				) {
					public void onDrawerClosed(View view) {

						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + mTitle + "</font>"));

						InputMethodManager inputManager = (InputMethodManager) ProjectListNewActivity.this
								.getSystemService(Activity.INPUT_METHOD_SERVICE);
						inputManager.hideSoftInputFromWindow(
								ProjectListNewActivity.this.getCurrentFocus().getWindowToken(), 0);

						// calling onPrepareOfptionsMenu() to show action bar
						// icons
						invalidateOptionsMenu();
					}

					public void onDrawerOpened(View drawerView) {
						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + mTitle + "</font>"));

						InputMethodManager inputManager = (InputMethodManager) ProjectListNewActivity.this
								.getSystemService(Activity.INPUT_METHOD_SERVICE);
						inputManager.hideSoftInputFromWindow(
								ProjectListNewActivity.this.getCurrentFocus().getWindowToken(), 0);

						// calling onPrepareOptionsMenu() to hide action bar
						// iconsin
						invalidateOptionsMenu();
					}
				};
				mDrawerLayout.setDrawerListener(mDrawerToggle);

				if (extras != null) {
					Boolean flag = extras.getBoolean("filter");
					Boolean signup_flag = extras.getBoolean("signup");
					login_flag = extras.getBoolean("login");
					logout_flag = extras.getBoolean("logout");
					Boolean favFlag = extras.getBoolean("favlist");
					extras.putBoolean("filter", false);
					if (flag.equals(true)) {

						FragmentManager fragmentmanager = getSupportFragmentManager();
						FragmentTransaction ft = fragmentmanager.beginTransaction();
						Fragment fragment = new TabFragmentnew();
						fragment.setArguments(extras);
						ft.addToBackStack(null);
						ft.replace(R.id.frame_container, fragment);
						ft.commit();

					}
					if (signup_flag.equals(true)) {
						FragmentManager fragmentmanager = getSupportFragmentManager();
						FragmentTransaction ft = fragmentmanager.beginTransaction();
						ft.replace(R.id.frame_container, new TabFragmentnew());
						ft.addToBackStack(null);
						ft.commit();

						// mDrawerLayout.closeDrawer(mDrawerList);
						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Home" + "</font>"));
					}

					if (logout_flag.equals(true)) {
						FragmentManager fragmentmanager = getSupportFragmentManager();
						FragmentTransaction ft = fragmentmanager.beginTransaction();
						ft.replace(R.id.frame_container, new TabFragmentnew());
						ft.addToBackStack(null);
						ft.commit();
						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Home" + "</font>"));

					}
					if (favFlag.equals(true)) {
						FragmentManager fragmentmanager = getSupportFragmentManager();
						FragmentTransaction ft = fragmentmanager.beginTransaction();
						ft.replace(R.id.frame_container, new CustomerFavPropertyFragment());
						ft.addToBackStack(null);
						ft.commit();

						// mDrawerLayout.closeDrawer(mDrawerList);
						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Favourite" + "</font>"));
					}

				} else {
					if (savedInstanceState == null) {
						// on first time display view for first nav item
						displayView(0);
						getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + "Home" + "</font>"));
					}
				}

				getPropertiesdata();
			} catch (Exception e) {
				e.printStackTrace();

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void getPropertiesdata() {
		getLoginData();

		new AsyncHttpPost(ProjectListNewActivity.this, UrlConstantUtils.GET_PROPERTIES_DATA2,
				ProjectListNewActivity.this, false).execute(PropertyDetailsFun(Integer.parseInt(signupUserId)));

	}

	private void getLoginData() {
		try {
			signupData = new AddSignUpToDbImpl();

			List<UserSignUp> signuplist = signupData.getAllDetails();
			if (signuplist != null && signuplist.size() != 0) {

				signUpInfo = new UserSignUp();
				if (signuplist.size() != 0) {
					for (int i = 0; i < signuplist.size(); i++) {
						signUpInfo = signuplist.get(i);
						signupUserId = signUpInfo.getUserId();
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static JSONObject PropertyDetailsFun(int userid) {
		JSONObject userObject = new JSONObject();
		try {
			userObject.put("user_id", userid);

		} catch (JSONException e) {
		}
		return userObject;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		// super.onSaveInstanceState(outState);
	}

	/**
	 * Slide menu item click listener
	 */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

			String header = mTitle.toString();
			if (header.equals(navMenuTitles[position])) {

				mDrawerLayout.closeDrawer(mDrawerList);
			} else {
				displayView(position);
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		try {
			// toggle nav drawer on selecting action bar app icon/title
			if (mDrawerToggle.onOptionsItemSelected(item)) {
				return true;
			}
			// Handle action bar actions click
			switch (item.getItemId()) {
			case R.id.action_settings:

				showPopupMenu(R.id.action_settings);
				return true;
			case R.id.notification:
				Intent intent = new Intent(ProjectListNewActivity.this, NotificationActivity.class);
				startActivity(intent);
				break;

			default:
				return super.onOptionsItemSelected(item);
			}

		} catch (Exception e) {
		}
		return true;
	}

	private void showPopupMenu(int id) {
		try {
			final Activity activity = ProjectListNewActivity.this;
			View v = activity.findViewById(id);
			final PopupMenu popupMenu = new PopupMenu(activity, v);
			popupMenu.getMenuInflater().inflate(R.menu.poupup_menu, popupMenu.getMenu());

			popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

				@Override
				public boolean onMenuItemClick(MenuItem item) {

					if (item.toString().equals("Search")) {

						mDrawerList.setItemChecked(3, true);
						mDrawerList.setSelection(3);
						setTitle("Search");
						mDrawerLayout.closeDrawer(mDrawerList);
						Fragment fragment = null;
						fragment = new FilterFragment();
						FragmentManager fragmentManager = getSupportFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
					} else if (item.toString().equals("About KUL")) {
						mDrawerList.setItemChecked(3, true);
						mDrawerList.setSelection(3);
						setTitle("About KUL");
						mDrawerLayout.closeDrawer(mDrawerList);
						Fragment fragment = null;
						fragment = new AboutKulFragment();
						FragmentManager fragmentManager = getSupportFragmentManager();
						fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

					} else if (item.toString().equals("Settings")) {

						Intent intent = new Intent(ProjectListNewActivity.this, SettingActivity.class);
						startActivity(intent);
					}

					return true;
				}
			});

			popupMenu.show();
		} catch (Exception e) {

		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// if nav drawer is opened, hide the action items
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 */
	private void displayView(int position) {
		// update the main content by replacing fragments
		try {
			Fragment fragment = null;
			int pos = position;
			switch (position) {
			case 0:
				fragment = new TabFragmentnew();

				break;

			case 1:

				fragment = new CustomerFavPropertyFragment();

				break;
			case 2:
				fragment = new MyKul();
				break;
			case 3:
				fragment = new EMI();
				break;

			case 4:

				Intent intent = new Intent(ProjectListNewActivity.this, DesginHomeActivity.class);
				startActivity(intent);

				break;
			case 5:
				fragment = new About();
				break;
			case 6:
				fragment = new EnquiryFragment();
				break;
			case 7:
				fragment = new ChatFragment();
				break;
			case 8:
				fragment = new FutureRequirement();
				break;

			default:
				break;

			}

			if (fragment != null) {

				try {

					FragmentManager fragmentManager1 = getSupportFragmentManager();
					fragmentManager1.beginTransaction().replace(R.id.frame_container, fragment).commit();
					mDrawerList.setItemChecked(pos, true);
					mDrawerList.setSelection(pos);
					setTitle(navMenuTitles[pos]);
					fragmentManager1.popBackStack();
					mDrawerLayout.closeDrawer(mDrawerList);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				// error in creating fragment
				Log.e("MainActivity", "Error in creating fragment");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setIcon(int icon) {
		getActionBar().setIcon(icon);
	}

	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
		getActionBar().setTitle(Html.fromHtml("<font color=\"#ffffff\">" + mTitle + "</font>"));

	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onBackPressed() {

		FragmentManager fragmentmanager = getSupportFragmentManager();

		new AlertDialog.Builder(this).setIcon(R.drawable.appiconpopup).setTitle("Kumar Builder")
				.setMessage("Are you sure you want to close app ")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						System.exit(0);
					}
				}).setNegativeButton("No", null).show();
	}

	@Override
	public void taskResponse(String response) {
		try {

			if (response != null) {

				Log.v("property Response :---", response);

				jsonobject = new JSONObject(response);
				JSONArray jsonarray;

				jsonarray = jsonobject.getJSONArray("data");
				try {

					propertyService = new PropertyServiceImpl();

				} catch (SQLException e) {
					e.printStackTrace();
				}

				for (int i = 0; i < jsonarray.length(); i++) {
					new HashMap<String, String>();
					jsonobject = jsonarray.getJSONObject(i);

					try {
						propertyDetails = (PropertiesDetails) propertyService
								.findById(jsonobject.getInt("property_id"));
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					getAminities(jsonobject);
					getGallery(jsonobject);
					getProjectPlan(jsonobject);
					getBuildingPlan(jsonobject);
					getUnitPlan(jsonobject);
					getAngularPlan(jsonobject);
					getPropertyComparison(jsonobject);
					getfloorPlan(jsonobject);
					getconstructionImages(jsonobject);
					try {
						propertyService.createOrUpdate(propertyDetails);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}

			else {
				Toast.makeText(ProjectListNewActivity.this, "Data not Found", Toast.LENGTH_LONG).show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getAminities(JSONObject jsonobject) {

		try {
			JSONArray amenitiesarray;
			ArrayList<String> amenitieslist = new ArrayList<String>();

			amenitiesarray = jsonobject.getJSONArray("amenities_photos");

			for (int j = 0; j < amenitiesarray.length(); j++) {

				amenitieslist.add(amenitiesarray.getString(j));

			}
			propertyDetails.setAmenitiesList(amenitieslist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getGallery(JSONObject jsonobject) {

		try {
			JSONArray galleryarray;
			ArrayList<String> gallerylist = new ArrayList<String>();

			galleryarray = jsonobject.getJSONArray("gallery_photos");

			for (int j = 0; j < galleryarray.length(); j++) {

				gallerylist.add(galleryarray.getString(j));

			}
			propertyDetails.setGalleryList(gallerylist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getProjectPlan(JSONObject jsonobject) {

		try {
			JSONArray projectplanarray;
			ArrayList<String> projectlist = new ArrayList<String>();

			projectplanarray = jsonobject.getJSONArray("project_photos");

			for (int j = 0; j < projectplanarray.length(); j++) {

				projectlist.add(projectplanarray.getString(j));

			}
			propertyDetails.setProjectPlanList(projectlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getBuildingPlan(JSONObject jsonobject) {

		try {
			JSONArray buildingplanarray;
			ArrayList<String> buildinglist = new ArrayList<String>();

			buildingplanarray = jsonobject.getJSONArray("building_photos");

			for (int j = 0; j < buildingplanarray.length(); j++) {

				buildinglist.add(buildingplanarray.getString(j));

			}
			propertyDetails.setBuildingPlanList(buildinglist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getUnitPlan(JSONObject jsonobject) {

		try {
			JSONArray unitplanarray;
			ArrayList<String> unitlist = new ArrayList<String>();

			unitplanarray = jsonobject.getJSONArray("unit_photos");

			for (int j = 0; j < unitplanarray.length(); j++) {

				unitlist.add(unitplanarray.getString(j));

			}
			propertyDetails.setUnitPlanList(unitlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getAngularPlan(JSONObject jsonobject) {

		try {
			JSONArray angularplanarray;
			ArrayList<String> angularlist = new ArrayList<String>();

			angularplanarray = jsonobject.getJSONArray("anguler_photos");

			for (int j = 0; j < angularplanarray.length(); j++) {

				angularlist.add(angularplanarray.getString(j));

			}
			propertyDetails.setAngularList(angularlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getfloorPlan(JSONObject jsonobject) {

		try {
			JSONArray floorplanarray;
			ArrayList<String> floorlist = new ArrayList<String>();

			floorplanarray = jsonobject.getJSONArray("floor_photos");

			for (int j = 0; j < floorplanarray.length(); j++) {

				floorlist.add(floorplanarray.getString(j));

			}
			propertyDetails.setFloorPlanList(floorlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getconstructionImages(JSONObject jsonobject) {

		try {
			JSONArray constructionnarray;
			ArrayList<String> constructionlist = new ArrayList<String>();

			constructionnarray = jsonobject.getJSONArray("construction_photos");

			for (int j = 0; j < constructionnarray.length(); j++) {

				constructionlist.add(constructionnarray.getString(j));

			}
			propertyDetails.setConstructionList(constructionlist);

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void getPropertyComparison(JSONObject jsonobject) {

		try {
			JSONArray comparisonnarray;
			ArrayList<String> comparisonlist = new ArrayList<String>();

			comparePropertyServerListService = new ComparePropertyServerListServiceImpl();

			comparisonnarray = jsonobject.getJSONArray("comparison_properies");

			for (int i = 0; i < comparisonnarray.length(); i++) {
				new HashMap<String, String>();
				ComparePropertyServerList compareServerList = new ComparePropertyServerList();
				JSONObject jsonobjectcompare = comparisonnarray.getJSONObject(i);

				compareServerList.setProperty_id(Integer.parseInt(jsonobjectcompare.getString("property_id")));
				compareServerList.setPropertyName(jsonobjectcompare.getString("property_name"));
				compareServerList.setKulpropertycompare_id(Integer.parseInt(jsonobject.getString("property_id")));

				comparePropertyServerListService.createOrUpdate(compareServerList);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}