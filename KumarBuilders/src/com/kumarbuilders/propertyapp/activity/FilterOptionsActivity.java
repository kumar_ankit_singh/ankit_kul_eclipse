package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.FilterListAdapter;

public class FilterOptionsActivity extends FragmentActivity {

	private ArrayList<String> Filterlist = new ArrayList<String>();
	private ListView list;
	public static FilterListAdapter filteradapter;
	public static String filter_selected_item;
	private String filter_item;
	private int listposition;
	public static String sort_Result = "not Specified", rent_Sale = "not Specified", house_Type = "Any",
			last_Update = "Any", cities_Towns = "Any", amenities = "All";
	private ImageView imgBack;
	private TextView txtheader;
	private Activity activity;

	private ArrayList<String> subFilterItemList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.filteroptionactivity);

		try {
			activity = FilterOptionsActivity.this;

			Bundle bundle = getIntent().getExtras();
			Filterlist = bundle.getStringArrayList("listitem");
			filter_item = bundle.getString("filterlistitem");
			listposition = bundle.getInt("position");

			imgBack = (ImageView) findViewById(R.id.imgback);
			list = (ListView) findViewById(R.id.filteroptionlist);
			txtheader = (TextView) findViewById(R.id.txtheader);
			txtheader.setText("" + filter_item);

			if (filter_item.equals("House Type")) {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
						getforHouseType());
				list.setAdapter(adapter);
			} else if (filter_item.equals("Last Updated")) {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
						getLastUpdate());
				list.setAdapter(adapter);
			} else if (filter_item.equals("Cities,Towns and Location")) {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
						getCitiesTowns());
				list.setAdapter(adapter);
			} else if (filter_item.equals("Amenities")) {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
						getAmenities());
				list.setAdapter(adapter);
			} else {
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
						Filterlist);
				list.setAdapter(adapter);
			}

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					try {
						String select_item = list.getItemAtPosition(position).toString();

						if (filter_item.equals("House Type")) {
							house_Type = select_item;
						} else if (filter_item.equals("Last Updated")) {
							last_Update = select_item;
						} else if (filter_item.equals("Cities,Towns and Location")) {
							cities_Towns = select_item;
						} else if (filter_item.equals("Amenities")) {
							amenities = select_item;
						}

						Intent intent = new Intent(FilterOptionsActivity.this, ProjectListNewActivity.class);
						intent.putExtra("filterlistitem", filter_item);
						intent.putExtra("list_selected_item", select_item);
						intent.putExtra("position", listposition);
						intent.putExtra("filter", true);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						FilterOptionsActivity.this.finish();
					} catch (Exception e) {
					}
				}
			});

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					// finish();

					Intent intent = new Intent(FilterOptionsActivity.this, ProjectListNewActivity.class);
					intent.putExtra("filter", true);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					FilterOptionsActivity.this.finish();

				}
			});

		} catch (Exception e) {
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private List<String> getforHouseType() {

		subFilterItemList.clear();
		subFilterItemList.add("Any");
		subFilterItemList.add("Apartment");
		subFilterItemList.add("House");
		subFilterItemList.add("Land");
		subFilterItemList.add("Commercial Property");
		subFilterItemList.add("Office Space");
		subFilterItemList.add("Row House");
		subFilterItemList.add("Villa");
		subFilterItemList.add("Bungalow");
		subFilterItemList.add("Farm House");
		return subFilterItemList;
	}

	private List<String> getLastUpdate() {

		subFilterItemList.clear();
		subFilterItemList.add("Any");
		subFilterItemList.add("One day ago");
		subFilterItemList.add("One week ago");
		subFilterItemList.add("15 days ago");
		subFilterItemList.add("One month ago");

		return subFilterItemList;
	}

	private List<String> getCitiesTowns() {

		subFilterItemList.clear();
		subFilterItemList.add("Any");
		subFilterItemList.add("Pune");
		subFilterItemList.add("Mumbai");
		subFilterItemList.add("Panchgani");
		subFilterItemList.add("Banglore");
		subFilterItemList.add("Hyderabad");

		return subFilterItemList;
	}

	private List<String> getAmenities() {

		subFilterItemList.clear();
		subFilterItemList.add("Any");
		subFilterItemList.add("Squash court");
		subFilterItemList.add("Guest lounge");
		subFilterItemList.add("Table tennis");
		subFilterItemList.add("Gymnasium");
		subFilterItemList.add("Yoga and Meditation");
		subFilterItemList.add("Health Club");
		subFilterItemList.add("Refuge area");
		subFilterItemList.add("Banquet");
		subFilterItemList.add("Swimming pool");
		subFilterItemList.add("Toddler pool");
		subFilterItemList.add("Card room");
		subFilterItemList.add("Childrens play area");
		subFilterItemList.add("Five temples");
		subFilterItemList.add("Party lawn");
		subFilterItemList.add("Shared Garbage");
		subFilterItemList.add("Private Cinema");
		subFilterItemList.add("Cabanas");
		subFilterItemList.add("Sky Lounge");
		subFilterItemList.add("Wine Lounge");
		subFilterItemList.add("Business Centre");
		subFilterItemList.add("Concierge");
		subFilterItemList.add("Amphitheatre");
		subFilterItemList.add("Hospital");
		subFilterItemList.add("School");
		subFilterItemList.add("Cycling");
		subFilterItemList.add("Police Station");
		subFilterItemList.add("Fire Station");
		subFilterItemList.add("Security Check Post");
		subFilterItemList.add("Jogging Track");
		subFilterItemList.add("Golf Club");
		subFilterItemList.add("Cricket Rink");
		subFilterItemList.add("Movie Theater");
		subFilterItemList.add("Country Club");
		subFilterItemList.add("Residency Club");
		subFilterItemList.add("Themed Gardens");
		subFilterItemList.add("Play Ground");
		subFilterItemList.add("Yoga Centre");
		subFilterItemList.add("Athletic Track");
		subFilterItemList.add("Community Hall");
		subFilterItemList.add("Temples");
		subFilterItemList.add("Basket Ball");
		subFilterItemList.add("Library");
		subFilterItemList.add("Cafeteria");
		subFilterItemList.add("Tracking System");
		subFilterItemList.add("kids gaming Zone");
		subFilterItemList.add("Resting area");
		subFilterItemList.add("Indoor games");
		subFilterItemList.add("Rain water harvesting");
		subFilterItemList.add("Club house");
		subFilterItemList.add("All");
		return subFilterItemList;
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(FilterOptionsActivity.this, ProjectListNewActivity.class);
		intent.putExtra("filter", true);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		FilterOptionsActivity.this.finish();
	}

}
