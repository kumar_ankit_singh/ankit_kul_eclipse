package com.kumarbuilders.propertyapp.activity;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.DesignHome;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeService;
import com.kumarbuilders.propertyapp.ormservices.DesignHomeServiceImpl;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.AvoidXfermode;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class DesginHomeActivity extends Activity {
	/** Called when the activity is first created. */

	private ImageView imgBack, imgMasterBedRoom, imgHall, imgsmallbedroom, imgKitchen, imgbedbathplain, imgbedbath,
			imghallbathplain, imghallbath;
	private byte[] profPhoto;
	private Bitmap bmpImg;
	private ImageView btn_save;
	private DesignHomeService designHomeService;
	private DesignHome designHome;
	public static byte[] byteArray, hallByteArray, kitchenByteArray, smallBedRoomByteArray, MasterBedByteArray;
	FrameLayout savedImage;
	private int return_Id, temp;
	private static HashMap<Integer, Integer> mRandomNumStorage = new HashMap<Integer, Integer>();
	private List<DesignHome> homeList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.designurhomenew);
		imgBack = (ImageView) findViewById(R.id.imgback);
		imgMasterBedRoom = (ImageView) findViewById(R.id.imgmasterbedroom);
		imgHall = (ImageView) findViewById(R.id.imghall);
		imgsmallbedroom = (ImageView) findViewById(R.id.imgsecondbedroom);
		imgKitchen = (ImageView) findViewById(R.id.imgkitchen);
		imgbedbath = (ImageView) findViewById(R.id.imgmbrbathfill);
		imgbedbathplain = (ImageView) findViewById(R.id.imgmbrbath);
		imghallbathplain = (ImageView) findViewById(R.id.imghallBathroom);
		imghallbath = (ImageView) findViewById(R.id.imghallbathfill);
		btn_save = (ImageView) findViewById(R.id.btn_save);

		savedImage = (FrameLayout) findViewById(R.id.frameImage);

		try {
			designHomeService = new DesignHomeServiceImpl();

			ArrayList<DesignHome> designList = new ArrayList<DesignHome>();
			designList.clear();

			designList = (ArrayList<DesignHome>) designHomeService.getAllData();

			if (designList != null && designList.size() != 0) {

				try {
					if (BitmapFactory.decodeByteArray(designList.get(designList.size() - 1).getHallphoto(), 0,
							designList.get(designList.size() - 1).getHallphoto().length) != null) {
						Bitmap bmpHall = BitmapFactory.decodeByteArray(
								designList.get(designList.size() - 1).getHallphoto(), 0,
								designList.get(designList.size() - 1).getHallphoto().length);

						imgHall.setImageBitmap(bmpHall);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					if (BitmapFactory.decodeByteArray(designList.get(designList.size() - 1).getKitchenphoto(), 0,
							designList.get(designList.size() - 1).getKitchenphoto().length) != null) {
						Bitmap bmpkitchen = BitmapFactory.decodeByteArray(
								designList.get(designList.size() - 1).getKitchenphoto(), 0,
								designList.get(designList.size() - 1).getKitchenphoto().length);
						imgKitchen.setImageBitmap(bmpkitchen);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					if (BitmapFactory.decodeByteArray(designList.get(designList.size() - 1).getSmallbedroomphoto(), 0,
							designList.get(designList.size() - 1).getSmallbedroomphoto().length) != null) {
						Bitmap bmpsmallBed = BitmapFactory.decodeByteArray(
								designList.get(designList.size() - 1).getSmallbedroomphoto(), 0,
								designList.get(designList.size() - 1).getSmallbedroomphoto().length);
						imgsmallbedroom.setImageBitmap(bmpsmallBed);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {
					if (BitmapFactory.decodeByteArray(designList.get(designList.size() - 1).getMasterbedroomphoto(), 0,
							designList.get(designList.size() - 1).getMasterbedroomphoto().length) != null) {
						Bitmap bmpmasterBed = BitmapFactory.decodeByteArray(
								designList.get(designList.size() - 1).getMasterbedroomphoto(), 0,
								designList.get(designList.size() - 1).getMasterbedroomphoto().length);
						imgMasterBedRoom.setImageBitmap(bmpmasterBed);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				imgbedbath.setVisibility(View.VISIBLE);
				imgbedbathplain.setVisibility(View.INVISIBLE);
				imghallbath.setVisibility(View.VISIBLE);
				imghallbathplain.setVisibility(View.INVISIBLE);
			} else {

				imgbedbath.setVisibility(View.INVISIBLE);
				imgbedbathplain.setVisibility(View.VISIBLE);
				imghallbath.setVisibility(View.INVISIBLE);
				imghallbathplain.setVisibility(View.VISIBLE);
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		imgBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(DesginHomeActivity.this, ProjectListNewActivity.class);
					// intent.putExtras(extras);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					DesginHomeActivity.this.finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgHall.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(DesginHomeActivity.this, DesginHomeActivity2.class);
					intent.putExtra("type", "hall");
					startActivityForResult(intent, 23);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgMasterBedRoom.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(DesginHomeActivity.this, DesginHomeActivity2.class);
					intent.putExtra("type", "masterbed");
					startActivityForResult(intent, 22);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgsmallbedroom.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(DesginHomeActivity.this, DesginHomeActivity2.class);
					intent.putExtra("type", "smallbed");
					startActivityForResult(intent, 24);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgKitchen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				try {
					Intent intent = new Intent(DesginHomeActivity.this, DesginHomeActivity2.class);
					intent.putExtra("type", "kitchen");
					startActivityForResult(intent, 25);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		imgbedbathplain.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					imgbedbath.setVisibility(View.VISIBLE);
					imgbedbathplain.setVisibility(View.INVISIBLE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		imghallbathplain.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					imghallbath.setVisibility(View.VISIBLE);
					imghallbathplain.setVisibility(View.INVISIBLE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		btn_save.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				new AlertDialog.Builder(DesginHomeActivity.this).setIcon(R.drawable.appicon).setTitle("Kumar Builder")
						.setMessage("Are you sure you want to save this image ")
						.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						try {
							designHomeService = new DesignHomeServiceImpl();

							int dbId = DesginHomeActivity2.temp;
							if (dbId == 0) {
								designHome = (DesignHome) designHomeService.findById(DesginHomeActivity2.temp);
							} else {
								designHome = new DesignHome();
							}

							bmpImg = getAllViewItems();
							ByteArrayOutputStream stream = new ByteArrayOutputStream();
							bmpImg = getResizedBitmap(bmpImg, 140);
							bmpImg.compress(Bitmap.CompressFormat.PNG, 100, stream);
							byteArray = stream.toByteArray();

							designHome.setHallphoto(hallByteArray);
							designHome.setKitchenphoto(kitchenByteArray);
							designHome.setSmallbedroomphoto(smallBedRoomByteArray);
							designHome.setMasterbedroomphoto(MasterBedByteArray);

							designHomeService.createOrUpdate(designHome);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}).setNegativeButton("No", null).show();

			}
		});

	}

	Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 0) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}

	public Bitmap getAllViewItems() {

		Bitmap bm = null;

		savedImage.setDrawingCacheEnabled(true);
		savedImage.buildDrawingCache();
		bm = savedImage.getDrawingCache();
		// imgSelection.setImageBitmap(bm);
		// imgSelection2.setImageBitmap(bm);

		Bitmap b3 = viewToBitmap(savedImage);
		// imgSelection.setImageBitmap(b3);
		return b3;

	}

	public Bitmap viewToBitmap(View view) {
		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);

		Bitmap mb = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		Canvas c = new Canvas(mb);

		Paint p = new Paint();
		p.setARGB(0, 0, 0, 0); // ARGB for the color, in this case red
		int removeColor = p.getColor(); // store this color's int for later use

		p.setAlpha(0);

		p.setXfermode(new AvoidXfermode(removeColor, 0, AvoidXfermode.Mode.TARGET));

		// draw transparent on the "brown" pixels
		c.drawPaint(p);

		view.draw(c);
		return mb;

	}

	// Call Back method to get the Message form other Activity
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		// check if the request code is same as what is passed here it is 2
		try {

			designHomeService = new DesignHomeServiceImpl();
			designHome = new DesignHome();
			// String val = data.getStringExtra("DbId");

			if (requestCode == 22) {
				imgMasterBedRoom.setVisibility(View.VISIBLE);

				if (data == null) {

					Bitmap bmp = BitmapFactory.decodeByteArray(DesginHomeActivity2.byteArray, 0,
							DesginHomeActivity2.byteArray.length);

					imgMasterBedRoom.setImageBitmap(bmp);
					MasterBedByteArray = DesginHomeActivity2.byteArray;

				} else {
					profPhoto = data.getByteArrayExtra("Image1");
					Bitmap bmp = BitmapFactory.decodeByteArray(profPhoto, 0, profPhoto.length);
					imgMasterBedRoom.setImageBitmap(bmp);
					MasterBedByteArray = profPhoto;
				}

			}

			if (requestCode == 23) {
				imgHall.setVisibility(View.VISIBLE);

				if (data == null) {

					Bitmap bmp = BitmapFactory.decodeByteArray(DesginHomeActivity2.byteArray, 0,
							DesginHomeActivity2.byteArray.length);

					imgHall.setImageBitmap(bmp);
					hallByteArray = DesginHomeActivity2.byteArray;

				} else {

					profPhoto = data.getByteArrayExtra("Image1");
					Bitmap bmp = BitmapFactory.decodeByteArray(profPhoto, 0, profPhoto.length);
					imgHall.setImageBitmap(bmp);
					hallByteArray = profPhoto;
				}
				designHomeService.createOrUpdate(designHome);

			}
			if (requestCode == 24) {
				imgsmallbedroom.setVisibility(View.VISIBLE);

				if (data == null) {

					Bitmap bmp = BitmapFactory.decodeByteArray(DesginHomeActivity2.byteArray, 0,
							DesginHomeActivity2.byteArray.length);
					imgsmallbedroom.setImageBitmap(bmp);
					smallBedRoomByteArray = DesginHomeActivity2.byteArray;

				} else {
					profPhoto = data.getByteArrayExtra("Image1");
					Bitmap bmp = BitmapFactory.decodeByteArray(profPhoto, 0, profPhoto.length);
					imgsmallbedroom.setImageBitmap(bmp);
					smallBedRoomByteArray = profPhoto;
				}

			}
			if (requestCode == 25) {

				if (data == null) {

					Bitmap bmp = BitmapFactory.decodeByteArray(DesginHomeActivity2.byteArray, 0,
							DesginHomeActivity2.byteArray.length);
					imgKitchen.setImageBitmap(bmp);
					kitchenByteArray = DesginHomeActivity2.byteArray;

				} else {
					imgKitchen.setVisibility(View.VISIBLE);
					profPhoto = data.getByteArrayExtra("Image1");
					Bitmap bmp = BitmapFactory.decodeByteArray(profPhoto, 0, profPhoto.length);
					imgKitchen.setImageBitmap(bmp);
					kitchenByteArray = profPhoto;
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}