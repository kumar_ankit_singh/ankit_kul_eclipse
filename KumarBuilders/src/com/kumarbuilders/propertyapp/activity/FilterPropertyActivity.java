package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.FilterListAdapter;
import com.kumarbuilders.propertyapp.adapter.PropertiesAdapter;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.fragments.FilterFragment;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;

public class FilterPropertyActivity extends FragmentActivity {

	ArrayList<PropertiesDetails> propertyList = new ArrayList<PropertiesDetails>();
	ListView list;
	public static FilterListAdapter filteradapter;
	public static String filter_selected_item;
	private ImageView imgBack;
	private Activity activity;
	PropertyService propertyService;
	private String houseType = "", maxPrice = "", minArea = "", lastUpdate = "", city = "", amenities = "";
	private PropertiesAdapter adapter;
	private PropertyServiceImpl propertyServiceImpl;
	private TextView txtheader;
	private String allListData = "";
	private Button btnChngFilter;

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.filterresulttivity);

		try {
			activity = FilterPropertyActivity.this;
			imgBack = (ImageView) findViewById(R.id.imgback);
			list = (ListView) findViewById(R.id.filteroptionlist);
			txtheader = (TextView) findViewById(R.id.txtheader);
			btnChngFilter = (Button) findViewById(R.id.btnchngfilter);

			Bundle bundle = getIntent().getExtras();

			city = bundle.getString("city");
			houseType = bundle.getString("houseType");
			maxPrice = bundle.getString("maxPrice");
			minArea = bundle.getString("minArea");
			lastUpdate = bundle.getString("lastUpdate");
			amenities = bundle.getString("amenities");
			allListData = bundle.getString("allList");

			propertyService = propertyServiceImpl.getInstance();

			if (allListData.equals("all")) {
				propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
				adapter = new PropertiesAdapter(activity, propertyList);
				list.setAdapter(adapter);
			} else {

				propertyList = FilterFragment.propertyList;
				adapter = new PropertiesAdapter(activity, propertyList);
				list.setAdapter(adapter);
			}

			if (propertyList.size() <= 0) {
				CheckNetwork.showDataNotFoundMessage(activity);
			}

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					try {
						PropertiesDetails property = propertyList.get(position);

						Intent intent = new Intent(activity, PropertyDetailsActivity.class);
						intent.putExtra("propertyId", property.getProperty_id());
						intent.putExtra("propertyName", property.getProperty_name());
						intent.putExtra("propertylat", property.getProperty_latitude());
						intent.putExtra("propertylong", property.getProperty_longitude());
						intent.putExtra("propertyLocation", property.getProperty_address());
						intent.putExtra("propertyUrl", property.getProperty_imgurl());
						intent.putExtra("description", property.getProperty_description());
						intent.putStringArrayListExtra("gallery", property.getGalleryList());
						intent.putExtra("ameneties", property.getAmenities());
						intent.putStringArrayListExtra("AminitiesList", property.getAmenitiesList());
						intent.putStringArrayListExtra("projectplan", property.getProjectPlanList());
						intent.putStringArrayListExtra("floorplan", property.getFloorPlanList());
						intent.putStringArrayListExtra("construction", property.getConstructionList());
						intent.putStringArrayListExtra("buildingplan", property.getBuildingPlanList());
						intent.putStringArrayListExtra("unitplan", property.getUnitPlanList());
						intent.putStringArrayListExtra("angular", property.getAngularList());
						intent.putExtra("specification", property.getSpecificationList());
						intent.putExtra("CostPdf", property.getPrice_pdf_url());
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						FilterPropertyActivity.this.finish();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

			btnChngFilter.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					finish();

				}
			});

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					FilterPropertyActivity.this.finish();

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// finish();
	}

	private List<PropertiesDetails> getallproperties() {

		try {
			propertyList = (ArrayList<PropertiesDetails>) propertyService.getAllData();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return propertyList;

	}

}
