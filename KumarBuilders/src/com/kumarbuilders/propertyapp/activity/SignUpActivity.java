package com.kumarbuilders.propertyapp.activity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.chat.ChatConnectionService;
import com.kumarbuilders.propertyapp.chat.LocalBinder;
import com.kumarbuilders.propertyapp.databasehandler.ApplicationPreferences;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDb;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.KulApplication;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.CommonFunctions;
import com.kumarbuilders.propertyapp.utils.Config;
import com.kumarbuilders.propertyapp.utils.CustomValidators;
import com.kumarbuilders.propertyapp.utils.ShareExternalServer;
import com.kumarbuilders.propertyapp.utils.StringConstant;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends Activity implements TaskResponse,
		StringConstant, Config {

	private EditText eUser, eMail, eCity, ePhone; // variables
	// declaratio
	private ImageView userImg, clearUser, clearMail, clearCity, clearPhone;
	private Button signupBtn;
	private String user, mail, city, phone, userid;
	private Activity activity;
	private TextView userimage, mailimage, locimage, phoneimage;
	private LinearLayout mParentLayout;

	final int CAMERA_CAPTURE = 1;
	final int CROP_PIC = 2;
	private static final int SELECT_PICTURE = 3;
	private Uri picUri;
	private View rootView;
	private PopupMenu cameraMenu;

	public AddSignUpToDb signUpData;
	public UserSignUp userdata;
	public JSONObject jObject;
	private Boolean isInternetPresent;
	private byte[] byteArray;
	private Typeface tf1;

	// GCM variables
	private String regId;
	private GoogleCloudMessaging gcm;
	public static final String REG_ID = "regId";
	private static final String APP_VERSION = "appVersion";
	private ShareExternalServer appUtils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {

			setContentView(R.layout.sign_up);
			activity = SignUpActivity.this;
			mParentLayout = (LinearLayout) findViewById(R.id.parentLLayout);

			eUser = (EditText) findViewById(R.id.usrNm);
			eMail = (EditText) findViewById(R.id.edtmailup);
			eCity = (EditText) findViewById(R.id.edtcityup);
			ePhone = (EditText) findViewById(R.id.edtphnup);

			signupBtn = (Button) findViewById(R.id.signUpBtn);

			userimage = (TextView) findViewById(R.id.userimage);
			mailimage = (TextView) findViewById(R.id.mailimage);
			locimage = (TextView) findViewById(R.id.locatinimage);
			phoneimage = (TextView) findViewById(R.id.phoneimage);

			userImg = (ImageView) findViewById(R.id.userPics);
			clearUser = (ImageView) findViewById(R.id.clear_username);
			clearMail = (ImageView) findViewById(R.id.clear_mail);
			clearCity = (ImageView) findViewById(R.id.clear_city);

			clearPhone = (ImageView) findViewById(R.id.clear_phon);

			getWindow().setBackgroundDrawableResource(R.drawable.signupbg);

			tf1 = Typeface.createFromAsset(getAssets(), "HealthAssist.ttf");
			userimage.setTypeface(tf1);
			mailimage.setTypeface(tf1);
			locimage.setTypeface(tf1);
			phoneimage.setTypeface(tf1);

			cameraMenu = new PopupMenu(activity, userImg);
			cameraMenu.inflate(R.menu.camera_option);

			isInternetPresent = CheckNetwork.isInternetPresent(activity);

			Animation animation = AnimationUtils.loadAnimation(activity,
					R.anim.slow_slide_in_left);
			mParentLayout.startAnimation(animation);

			getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			signUpData = new AddSignUpToDbImpl();
			// creating ShareExternalServer class object
			appUtils = new ShareExternalServer();
			if (TextUtils.isEmpty(regId)) {
				regId = registerGCM();
			}
			editFunction();
			textChangedFunction();
			userImg.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						cameraMenu.show();
					} catch (Exception e) {

					}

				}
			});

			cameraMenu
					.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
						@Override
						public boolean onMenuItemClick(MenuItem item) {
							switch (item.getItemId()) {
							case R.id.openCamera:
								openCameraInSignUp();
								break;
							case R.id.openGallery:
								openGalleryInSignUp();
								break;
							}
							return false;
						}
					});
			signupBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						isInternetPresent = CheckNetwork
								.isInternetPresent(activity);
						if (isInternetPresent) {
							user = eUser.getText().toString();
							mail = eMail.getText().toString();
							city = eCity.getText().toString();
							phone = ePhone.getText().toString();
							CustomValidators valdition = new CustomValidators();
							if (user.trim().equals("")
									&& mail.trim().equals("")
									&& city.trim().equals("")
									&& phone.trim().equals("")) {
								eUser.setError("Please enter username");
								eMail.setError("Please enter mail");
								eCity.setError("Please enter city");
								ePhone.setError("Please enter phone");

							} else if (user.trim().equals("")) {
								eUser.setError("Please enter username");
								eMail.setError(null);// removes error
								eCity.setError(null);
								ePhone.setError(null);
								eMail.clearFocus();
								eCity.clearFocus();
								ePhone.clearFocus();
							} else if (mail.trim().equals("")) {
								eUser.setError(null);
								eMail.setError("Please enter mail");
								eCity.setError(null);
								ePhone.setError(null);
								eUser.clearFocus();
								eCity.clearFocus();
								ePhone.clearFocus();
							} else if (city.trim().equals("")) {
								eUser.setError(null);
								eMail.setError(null);// removes error
								eCity.setError("Please enter city");
								ePhone.setError(null);
								eMail.clearFocus();
								eUser.clearFocus();
								ePhone.clearFocus();
							} else if (phone.trim().equals("")) {
								eUser.setError(null);
								eMail.setError(null);// removes error
								eCity.setError(null);
								ePhone.setError("Please enter phone no");
								eMail.clearFocus();
								eCity.clearFocus();
								eUser.clearFocus();
							} else if (!(valdition.isValidName(user))) {
								Toast.makeText(activity,
										"user name must be in alphabets only",
										Toast.LENGTH_LONG).show();
								eUser.setText("");
							} else if (!valdition.isValidEmail(mail)) {
								Toast.makeText(activity,
										"please check mail address",
										Toast.LENGTH_LONG).show();
								eMail.setText("");
							} else if (!valdition.isValidName(city)) {
								Toast.makeText(activity,
										"city name must be in alphabets",
										Toast.LENGTH_LONG).show();
								eCity.setText("");
							} else if (!valdition.isValidPhone(phone)) {
								Toast.makeText(activity,
										"please enter only 10 digit",
										Toast.LENGTH_LONG).show();
								ePhone.setText("");
							} else {

								new AsyncHttpPost(activity,
										UrlConstantUtils.REGISTARTION,
										SignUpActivity.this, true)
										.execute(registrationData(user, mail,
												city, phone, regId));
							}
						} else {
							CheckNetwork.showNetworkConnectionMessage(activity);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			});

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// user own method
	public void editFunction() {
		try {
			clearUser.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					eUser.setText("");

				}
			});

			clearMail.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					eMail.setText("");
				}
			});

			clearCity.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					eCity.setText("");
				}
			});

			clearPhone.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					ePhone.setText("");
				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void textChangedFunction() {
		try {
			final String stateString = eUser.getText().toString();
			eUser.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {

					if (!stateString.trim().equals("")) {

						eUser.setError(null);
						clearUser.setVisibility(View.VISIBLE);
					} else {
						clearUser.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

					if (!stateString.trim().equals("")) {

						eUser.setError(null);
						clearUser.setVisibility(View.VISIBLE);
					} else {
						clearUser.setVisibility(View.INVISIBLE);
					}
				}
			});

			eMail.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					String stateString = eMail.getText().toString();
					if (!stateString.trim().equals("")) {

						eMail.setError(null);
						clearMail.setVisibility(View.VISIBLE);
					} else {
						clearMail.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			eCity.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {

					String stateString = eCity.getText().toString();
					if (!stateString.trim().equals("")) {

						eCity.setError(null);
						clearCity.setVisibility(View.VISIBLE);
					} else {
						clearCity.setVisibility(View.INVISIBLE);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			ePhone.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					String stateString = ePhone.getText().toString();
					int pos = stateString.length();
					CommonFunctions.phoneNoLimitWithAutoSoftKeyClose(pos,
							activity);
					if (!stateString.trim().equals("")) {

						ePhone.setError(null);
						clearPhone.setVisibility(View.VISIBLE);
					} else {
						clearPhone.setVisibility(View.INVISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject registrationData(String userName, String mailId,
			String city, String ph, String regId) {
		JSONObject registartion = new JSONObject();
		try {
			registartion.put("name", userName);
			registartion.put("email", mailId);
			registartion.put("city", city);
			registartion.put("phone", ph);
			registartion.put("device_token", regId);
		} catch (JSONException e) {
		}
		return registartion;
	}

	public void openCameraInSignUp() {

		try {
			// use standard intent to capture an image
			Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			// we will handle the returned data in onActivityResult
			startActivityForResult(captureIntent, CAMERA_CAPTURE);
		} catch (ActivityNotFoundException anfe) {
			CommonFunctions.showCustomToast(IMAG_CROP, activity);
		}
	}

	public void openGalleryInSignUp() {
		try {
			Intent intent = new Intent();
			intent.setType("image/*");
			intent.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(intent, SELECT_PICTURE);
		} catch (ActivityNotFoundException anfe) {
			CommonFunctions.showCustomToast(IMAG_CROP, activity);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		try {
			if (data != null) {
				if (resultCode == activity.RESULT_OK) {
					if (requestCode == CAMERA_CAPTURE) {
						picUri = data.getData();
						Bitmap photo = (Bitmap) data.getExtras().get("data");
						// photo = decodeSampledBitmapFromUri(picUri,
						// userImg.getWidth(), userImg.getHeight());
						userImg.setImageBitmap(photo);
						insertOrUpdateProfilePic(photo);

					}
					// user is returning from cropping the image
					else if (requestCode == CROP_PIC) {
						// get the returned data
						Bundle extras = data.getExtras();
						// get the cropped bitmap
						Bitmap profilePic = extras.getParcelable("data");
						ImageView picView = (ImageView) rootView
								.findViewById(R.id.userPics);
						picView.setImageBitmap(profilePic);
						insertOrUpdateProfilePic(profilePic);

					} else if (requestCode == SELECT_PICTURE) {
						Uri selectedImageUri = data.getData();
						Bitmap bitmap;
						bitmap = decodeSampledBitmapFromUri(selectedImageUri,
								userImg.getWidth(), userImg.getHeight());
						if (bitmap == null) {
							Toast.makeText(activity,
									"the image data could not be decoded",
									Toast.LENGTH_LONG).show();
						}

						// getPath(selectedImageUri);
						userImg.setImageBitmap(bitmap);
						// userImg.setImageURI(selectedImageUri);
						// Bitmap bm =
						// BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri));
						insertOrUpdateProfilePic(bitmap);
					}
				}
			} else {
				CommonFunctions.showCustomToast(IMG_DATA, activity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean insertOrUpdateProfilePic(Bitmap photo) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byteArray = stream.toByteArray();

		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	// Class with extends AsyncTask class

	public class LoadImagesFromSDCard extends AsyncTask<String, Void, Void> {

		private ProgressDialog Dialog = new ProgressDialog(activity);

		Bitmap mBitmap;

		protected void onPreExecute() {
			/****** NOTE: You can call UI Element here. *****/

			// Progress Dialog
			Dialog.setMessage(" Loading image from Sdcard..");
			Dialog.show();
		}

		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			Bitmap bitmap = null;
			Bitmap newBitmap = null;
			Uri uri = null;

			try {

				/**
				 * Uri.withAppendedPath Method Description Parameters baseUri
				 * Uri to append path segment to pathSegment encoded path
				 * segment to append Returns a new Uri based on baseUri with the
				 * given segment appended to the path
				 */

				// write code here...
				uri = Uri.withAppendedPath(
						MediaStore.Images.Media.INTERNAL_CONTENT_URI, ""
								+ urls[0]);

				/************** Decode an input stream into a bitmap. *********/

				bitmap = BitmapFactory.decodeStream(activity
						.getContentResolver().openInputStream(uri));

				if (bitmap != null) {

					/*********
					 * Creates a new bitmap, scaled from an existing bitmap.
					 ***********/

					newBitmap = Bitmap.createScaledBitmap(bitmap, 170, 170,
							true);

					bitmap.recycle();

					if (newBitmap != null) {

						mBitmap = newBitmap;
					}
				}
			} catch (IOException e) {
				// Error fetching image, try to recover

				/********* Cancel execution of this task. **********/
				cancel(true);
			}

			return null;
		}

		protected void onPostExecute(Void unused) {

			// NOTE: You can call UI Element here.

			// Close progress dialog
			Dialog.dismiss();

			if (mBitmap != null) {
				// Set Image to ImageView

				userImg.setImageBitmap(mBitmap);
			}

		}

	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = activity
				.managedQuery(uri, projection, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	/**
	 * this function does the crop operation.
	 */
	private void performCrop() {
		// take care of exceptions
		try {
			// call the standard crop action intent (the user device may not
			// support it)
			Intent cropIntent = new Intent("com.android.camera.action.CROP");
			// indicate image type and Uri
			cropIntent.setDataAndType(picUri, "image/*");
			// set crop properties
			cropIntent.putExtra("crop", "true");
			// indicate aspect of desired crop
			cropIntent.putExtra("aspectX", 1);
			cropIntent.putExtra("aspectY", 1);
			// indicate output X and Y
			cropIntent.putExtra("outputX", 256);
			cropIntent.putExtra("outputY", 256);
			// retrieve data on return
			cropIntent.putExtra("return-data", true);
			// start the activity - we handle returning in onActivityResult
			startActivityForResult(cropIntent, CROP_PIC);
		}
		// respond to users whose devices do not support the crop action
		catch (ActivityNotFoundException anfe) {
			CommonFunctions.showCustomToast(IMAG_CROP, activity);
		}
	}

	@Override
	public void taskResponse(String response) {
		String status = null;

		if (response != null) {
			try {
				jObject = new JSONObject(response.toString());
				status = jObject.getString(STATUS);
				Log.v("status", "" + status);
				if (status.trim().equals("SUCCESS")) {
					usersignupData();
					UserSignUp signInfo = new UserSignUp();

					/*
					 * below code is just to check that database of signup
					 * created or not
					 */
					signUpData = new AddSignUpToDbImpl();
					List<UserSignUp> list = signUpData.getAllDetails();
					for (int i = 0; i < list.size(); i++) {
						signInfo = list.get(i);
						String usercity = signInfo.getCity();
						Log.v("city", " " + usercity);

					}
					doBindService();
					// usermykulData();
					Toast.makeText(activity, "Registered Successfully!!!",
							Toast.LENGTH_LONG).show();
					try {
						Intent intent = new Intent(activity,
								ProjectListNewActivity.class);
						startActivity(intent);
						finish();

					} catch (ActivityNotFoundException e) {
						CommonFunctions.showCustomToast(NOT_FOUND, activity);
					}
				} else if (status.trim().equals("ALREADY_EXIST")) {

					new AlertDialog.Builder(this)
							.setIcon(R.drawable.appiconpopup)
							.setTitle("Already Exist")
							.setMessage(
									"Would you like to continue with same account!! ")
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											getSignedData();
											doBindService();
											Intent intent = new Intent(
													activity,
													ProjectListNewActivity.class);
											startActivity(intent);
											finish();
										}
									}).setNegativeButton("No", null).show();

				} else if (status.trim().equals("ACCOUNT_BLOCKED")) {
					new AlertDialog.Builder(this)
							.setIcon(R.drawable.appiconpopup)
							.setTitle("Account Blocked")
							.setMessage(
									"Your account is blocked by admin.Please contact admin. ")
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.dismiss();
										}
									}).show();
					// Toast.makeText(activity, "Your account is blocked by
					// admin", Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(activity,
							"Registration Failed or Insufficient Data!!",
							Toast.LENGTH_LONG).show();
				}

			} catch (NullPointerException e) {
				CommonFunctions.showCustomToast(NULL_POINTER, activity);
			} catch (Exception e) {
				// CheckNetwork.showNetworkTimeOutMessage(activity);
				Toast.makeText(activity, "Server Error", Toast.LENGTH_SHORT)
						.show();
			}
		} else {
			CommonFunctions.showCustomToast(NULL_RESPONSE, activity);
		}

	}

	public void usersignupData() {
		try {
			userdata = new UserSignUp();
			userdata.setUsername(user);
			userdata.setEmailId(mail);
			userdata.setPhone(phone);
			userdata.setCity(city);

			userid = jObject.getString("user_id");
			ApplicationPreferences preferneces = new ApplicationPreferences(
					getApplicationContext());
			preferneces.savePreference("kulUserId", userid);
			userdata.setUserId(userid);
			userdata.setUserPhoto(byteArray);
			signUpData.createOrUpdate(userdata);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// public void usermykulData() throws SQLException {
	// try {
	// usermyKul = new UserMyKul();
	// usermyKul.setEmail(mail);
	// usermyKul.setPhone(phone);
	// usermyKul.setCity(city);
	// userid = jObject.getString("user_id");
	// usermyKul.setUserid(userid);
	// usermyKul.setUserPhoto(byteArray);
	// myKulData.createOrUpdate(usermyKul);
	//
	// } catch (JSONException e) {
	// } catch (Exception e) {
	// }
	// }

	public void getSignedData() {
		try {
			userdata = new UserSignUp();
			JSONArray signedObject = jObject.getJSONArray("data");
			for (int i = 0; i < signedObject.length(); i++) {
				JSONObject sItems = signedObject.getJSONObject(i);
				userid = sItems.getString("user_id");
				user = sItems.getString("name");
				mail = sItems.getString("email");
				phone = sItems.getString("contact_number");
				city = sItems.getString("city");
				ApplicationPreferences preferneces = new ApplicationPreferences(
						getApplicationContext());
				preferneces.savePreference("kulUserId", userid);
				userdata.setUserId(userid);
				userdata.setUsername(user);
				userdata.setEmailId(mail);
				userdata.setPhone(phone);
				userdata.setCity(city);
				userdata.setUserPhoto(byteArray);
				signUpData.createOrUpdate(userdata);
				UserSignUp signInfo = new UserSignUp();

				try {
					signUpData = new AddSignUpToDbImpl();
					List<UserSignUp> list = signUpData.getAllDetails();
					if (list.size() != 0) {
						for (int j = 0; j < list.size(); j++) {
							signInfo = list.get(j);
							String usercity = signInfo.getCity();
							String userid = signInfo.getUserId();
							Log.v("city", " " + usercity);

						}
					} else {

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth,
			int reqHeight) {
		Bitmap bm = null;
		try {
			// First decode with inJustDecodeBounds=true to check dimensions
			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(getContentResolver()
					.openInputStream(uri), null, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			bm = BitmapFactory.decodeStream(getContentResolver()
					.openInputStream(uri), null, options);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Toast.makeText(activity, e.toString(), Toast.LENGTH_LONG).show();
		} catch (Exception e) {

		}
		return bm;
	}

	public String registerGCM() {

		gcm = GoogleCloudMessaging.getInstance(this);
		regId = getRegistrationId(activity);

		if (regId.length() == 0 || regId == null) {
			regId = getRegistrationId(activity);
		}

		if (TextUtils.isEmpty(regId)) {

			registerInBackground();

			Log.d("RegisterActivity",
					"registerGCM - successfully registered with GCM server - regId: "
							+ regId);
		} else {
			Toast.makeText(getApplicationContext(),
					"RegId already available. RegId: " + regId,
					Toast.LENGTH_LONG).show();
		}
		return regId;
	}

	private String getRegistrationId(Activity mActivity) {
		final SharedPreferences prefs = getSharedPreferences(
				SignUpActivity.class.getSimpleName(), Activity.MODE_PRIVATE);
		String registrationId = prefs.getString(REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("Reg id", "Registration not found.");
			return "";
		}
		int registeredVersion = prefs.getInt(APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(mActivity);
		if (registeredVersion != currentVersion) {
			Log.i("Reg id", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private static int getAppVersion(Activity mActivity) {
		try {
			PackageInfo packageInfo = mActivity.getPackageManager()
					.getPackageInfo(mActivity.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			Log.d("RegisterActivity",
					"I never expected this! Going down, going down!" + e);
			throw new RuntimeException(e);
		}
	}

	private void registerInBackground() {
		new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";
				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(activity);
					}
					regId = gcm.register(Config.GOOGLE_PROJECT_ID);
					Log.d("RegisterActivity", "registerInBackground - regId: "
							+ regId);
					msg = "Device registered, registration ID=" + regId;
					Log.v("reg id", "" + regId);

					storeRegistrationId(activity, regId);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();
					Log.d("RegisterActivity", "Error: " + msg);
				}
				Log.d("RegisterActivity", "AsyncTask completed: " + msg);
				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
				// Toast.makeText(getApplicationContext(),
				// "Registered with GCM Server." + msg, Toast.LENGTH_LONG)
				// .show();
			}
		}.execute(null, null, null);
	}

	private void storeRegistrationId(Activity mActivity, String regId) {
		final SharedPreferences prefs = getSharedPreferences(
				SignUpActivity.class.getSimpleName(), Activity.MODE_PRIVATE);
		int appVersion = getAppVersion(mActivity);
		Log.i("TAG", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(REG_ID, regId);
		editor.putInt(APP_VERSION, appVersion);
		editor.commit();
		// appUtils.shareRegIdWithAppServer(mActivity, regId);
	}

	/* Code for connecting through xxmp server */

	private ChatConnectionService mService;
	private boolean mBounded;
	private final ServiceConnection mConnection = new ServiceConnection() {

		@SuppressWarnings("unchecked")
		@Override
		public void onServiceConnected(final ComponentName name,
				final IBinder service) {
			mService = ((LocalBinder<ChatConnectionService>) service)
					.getService();
			mBounded = true;
			Log.d("service connected", "onServiceConnected");
		}

		@Override
		public void onServiceDisconnected(final ComponentName name) {
			mService = null;
			mBounded = false;
			Log.d("service connected", "onServiceDisconnected");
		}
	};

	void doBindService() {
		bindService(new Intent(this, ChatConnectionService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	void doUnbindService() {
		if (mConnection != null) {
			unbindService(mConnection);
		}
	}

	public ChatConnectionService getmService() {
		return mService;
	}
}
