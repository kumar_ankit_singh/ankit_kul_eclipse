package com.kumarbuilders.propertyapp.activity;

import java.text.DecimalFormat;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.kumarbuilders.propertyapp.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CurrencyConverterActivity extends Activity {

	private Spinner mFrom, mTo;
	private EditText mAmount, mConvrtAmt;
	private Button mConvtBtn;
	private TextView mCurrencySymbol;
	private String mToString, fromValue, fromSymbol, toSymbol;
	private String[] symbol;
	private ImageView back;
	private JSONObject jsonObject, rateObject;
	private double getfromValue, gettoValue;
	private static final String url = "https://openexchangerates.org/api/latest.json?app_id=95be4175d157448cba887c8e090c9cc8";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.currency_calci);

		mFrom = (Spinner) findViewById(R.id.fromSpinner);
		mTo = (Spinner) findViewById(R.id.toSpinner);

		mAmount = (EditText) findViewById(R.id.amtEdt);
		mConvrtAmt = (EditText) findViewById(R.id.editText1);

		mConvtBtn = (Button) findViewById(R.id.calBtn);

		mCurrencySymbol = (TextView) findViewById(R.id.chgTxt);

		symbol = getResources().getStringArray(R.array.currency_symbols);

		back = (ImageView) findViewById(R.id.backImage);
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CurrencyConverterActivity.this.finish();

			}
		});

		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		onButtonListener();
		addItemsToSpinner();
		myOnItemSelectListener();
		editTextOnTouchListner();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] response) {
				String rateResponse = new String(response);
				Log.v("rate json ", "" + rateResponse.toString());

				try {
					jsonObject = new JSONObject(rateResponse.toString());
					rateObject = jsonObject.getJSONObject("rates");

					// jsonObject.keys()
				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				Toast.makeText(CurrencyConverterActivity.this,
						"Check your net connection,You need internet connection ", Toast.LENGTH_LONG).show();

			}
		});
	}

	public void addItemsToSpinner() {
		try {

			ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.currency_name,
					android.R.layout.simple_spinner_item);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			mFrom.setAdapter(adapter);

			mTo.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void myOnItemSelectListener() {
		try {

			mFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					fromValue = parent.getItemAtPosition(position).toString();
					fromSymbol = symbol[position].toString();
					Log.v("from symbol", fromSymbol);

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

			mTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mAmount.getWindowToken(), 0);
					mToString = parent.getItemAtPosition(position).toString();
					toSymbol = symbol[position].toString();

				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onButtonListener() {

		mConvtBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mAmount.getWindowToken(), 0);

				try {
					long givenAmount = Long.parseLong(mAmount.getText().toString());

					if (!mAmount.getText().toString().trim().equals("")) {
						if (fromValue.trim().equals(mToString)) {
							DecimalFormat df = new DecimalFormat("#.##");
							mConvrtAmt.setText(df.format(givenAmount));
							mCurrencySymbol.setText(toSymbol);

						} else if (fromSymbol.trim().equals("USD")) {
							for (int i = 0; i < rateObject.names().length(); i++) {
								try {
									if (toSymbol.trim().equals(rateObject.names().getString(i))) {
										double keyValue = rateObject.getDouble(rateObject.names().getString(i));
										double finalAmount = (keyValue * givenAmount);
										DecimalFormat df = new DecimalFormat("#.##");
										mConvrtAmt.setText(df.format(finalAmount));
										mCurrencySymbol.setText(toSymbol);

									}
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						} else {
							for (int i = 0; i < rateObject.names().length(); i++) {
								try {
									if (fromSymbol.trim().equals(rateObject.names().getString(i))) {
										getfromValue = rateObject.getDouble(rateObject.names().getString(i));
									}
									if (toSymbol.trim().equals(rateObject.names().getString(i))) {
										gettoValue = rateObject.getDouble(rateObject.names().getString(i));
									}
									if (getfromValue != 0 && gettoValue != 0) {
										double finalValue = (gettoValue / getfromValue) * givenAmount;
										DecimalFormat df = new DecimalFormat("#.##");
										mConvrtAmt.setText(df.format(finalValue));
										mCurrencySymbol.setText(toSymbol);

									}

								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
						}
					} else {
						Toast.makeText(CurrencyConverterActivity.this, "Please enter some amount ", Toast.LENGTH_LONG)
								.show();
					}
				} catch (NumberFormatException e) {
					// e.printStackTrace();
					Toast.makeText(CurrencyConverterActivity.this, "Please enter some amount ", Toast.LENGTH_LONG)
							.show();

				}
			}
		});
	}

	public void editTextOnTouchListner() {
		mTo.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mAmount.getWindowToken(), 0);
				return false;
			}
		});

		mFrom.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mAmount.getWindowToken(), 0);
				return false;
			}
		});
	}

}
