package com.kumarbuilders.propertyapp.activity;

import java.util.List;

import com.facebook.android.DialogError;
import com.facebook.android.FacebookError;
import com.facebook.android.Facebook.DialogListener;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.utils.StringConstant;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RemoteViews;
import android.widget.RemoteViews.ActionException;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SettingActivity extends Activity implements StringConstant {

	private Button fbButton, twitterButton, saveButton;
	private ToggleButton statusButton, syncButton;
	private TextView loggedName, appVersion, recomdSugg;
	private ImageView imgBack;
	private AddSignUpToDbImpl signData;
	private List<UserSignUp> usrSigned;
	private UserSignUp loggedMail;
	private String mail;

	// private Facebook facebook = new Facebook(APP_ID);
	private SharedPreferences mPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting_fragment);
		try {
			fbButton = (Button) findViewById(R.id.FBButton);
			twitterButton = (Button) findViewById(R.id.TwiterBtn);
			saveButton = (Button) findViewById(R.id.settingSaveBtn);

			statusButton = (ToggleButton) findViewById(R.id.offlineToggle);
			syncButton = (ToggleButton) findViewById(R.id.syncToggle);

			loggedName = (TextView) findViewById(R.id.editloged);
			appVersion = (TextView) findViewById(R.id.editApp);
			recomdSugg = (TextView) findViewById(R.id.RecSugTxt);
			imgBack = (ImageView) findViewById(R.id.imgback);

			statusOnClickListner();
			syncOnClickListener();
			openMailForRecommendation();
			facebookOnClickListener();
			twitterOnClickListener();
			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					SettingActivity.this.finish();

				}
			});

			try {
				signData = new AddSignUpToDbImpl();
				usrSigned = signData.getAllDetails();
				loggedMail = new UserSignUp();
				if (usrSigned.size() != 0) {
					for (int i = 0; i < usrSigned.size(); i++) {
						loggedMail = usrSigned.get(i);
						mail = loggedMail.getEmailId();
					}
					Log.v("mail is", "" + mail);
					loggedName.setText(mail);
				}

			} catch (NullPointerException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		getDataFunction();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		getDataFunction();
	}

	public void statusOnClickListner() {

		try {

			statusButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					if (isChecked) {
						statusButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.on));
					} else {
						statusButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.off));
					}

				}
			});

		} catch (RemoteViews.ActionException e) {
			Toast.makeText(SettingActivity.this, "offline toggle problem", Toast.LENGTH_LONG).show();
		}
	}

	public void syncOnClickListener() {
		try {

			syncButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						syncButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.on));
					} else {
						syncButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.off));
					}

				}
			});
		} catch (ActionException e) {
			Toast.makeText(SettingActivity.this, "sync toggle problem", Toast.LENGTH_LONG).show();
		}
	}

	public void facebookOnClickListener() {

		try {
			fbButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					loginToFacebook();

				}
			});
		} catch (Exception e) {

		}
	}

	public void twitterOnClickListener() {

		try {
			twitterButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					SettingActivity.this.finish();
					Intent intent = new Intent(SettingActivity.this, TwitterActivity.class);
					startActivity(intent);
				}
			});
		} catch (Exception e) {

		}
	}

	public void getDataFunction() {
		try {
			String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

			appVersion.setText(versionName);
		} catch (Exception e) {
		}
	}

	public void openMailForRecommendation() {
		try {

			recomdSugg.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					sendEmail();

				}
			});
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			if (resultCode == RESULT_CANCELED) {

			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void sendEmail() {
		try {

			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			// Mime type of the attachment (or) u can use
			// sendIntent.setType("*/*")
			sendIntent.setType("text/xml");
			// Subject for the message or Email
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Kumar Builder");
			sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "sales@kul.co.in" });

			final PackageManager pm = getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")
						|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

			startActivity(sendIntent);

		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(SettingActivity.this, "Sending failed !!", Toast.LENGTH_SHORT).show();

		}

	}

	public void loginToFacebook() {
		try {
			mPrefs = getPreferences(MODE_PRIVATE);
			String access_token = mPrefs.getString(ACCESS_TOKEN, null);
			long expires = mPrefs.getLong(ACCESS_EXPIRES, 0);

			if (access_token != null) {
				facebook.setAccessToken(access_token);
				Log.d("FB Sessions", "" + facebook.isSessionValid());
				finish();
				Intent intent = new Intent(SettingActivity.this, AndroidFacebookConnectActivity.class);
				startActivity(intent);
			}

			if (expires != 0) {
				facebook.setAccessExpires(expires);
			}

			if (!facebook.isSessionValid()) {
				facebook.authorize(this, new String[] { "email", "publish_stream" }, new DialogListener() {

					@Override
					public void onCancel() {
						// Function to handle cancel event
					}

					@Override
					public void onComplete(Bundle values) {
						// Function to handle complete event
						// Edit Preferences and update facebook acess_token
						SharedPreferences.Editor editor = mPrefs.edit();
						editor.putString(ACCESS_TOKEN, facebook.getAccessToken());
						editor.putLong(ACCESS_EXPIRES, facebook.getAccessExpires());
						editor.commit();

						finish();
						Intent intent = new Intent(SettingActivity.this, AndroidFacebookConnectActivity.class);
						startActivity(intent);

					}

					@Override
					public void onError(DialogError error) {
						// Function to handle error

					}

					@Override
					public void onFacebookError(FacebookError fberror) {
						// Function to handle Facebook errors

					}

				});
			}
		} catch (Exception e) {
		}

	}

}
