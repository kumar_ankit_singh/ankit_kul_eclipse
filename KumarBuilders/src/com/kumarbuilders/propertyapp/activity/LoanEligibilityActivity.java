package com.kumarbuilders.propertyapp.activity;

import com.kumarbuilders.propertyapp.R;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoanEligibilityActivity extends Activity {

	private Button calculate;
	private EditText mIncome, mEMI, mLoan, mTenure, mAnnual_Interset_Rate,
			mEligible_Amount;
	private ImageView back;
	private TextView mYesNo;
	private String income, emi, loan, tenure, rate;
	private String eligibleAmount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {

			setContentView(R.layout.loan_calculator);

			mIncome = (EditText) findViewById(R.id.editIncom);
			mEMI = (EditText) findViewById(R.id.editEMI);
			mLoan = (EditText) findViewById(R.id.editLoan);
			mTenure = (EditText) findViewById(R.id.editTenure);
			mAnnual_Interset_Rate = (EditText) findViewById(R.id.editRate);
			mEligible_Amount = (EditText) findViewById(R.id.eligibleAmtEdtTxt);

			calculate = (Button) findViewById(R.id.calciBtn);

			mYesNo = (TextView) findViewById(R.id.YNTxtview);

			back = (ImageView) findViewById(R.id.backImgVw);
			back.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					LoanEligibilityActivity.this.finish();

				}
			});

			this.getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			// getEditTextValue();
			onButtonListener();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getEditTextValue() {
		try {

			income = mIncome.getText().toString();
			Log.v("income", "" + income);
			emi = mEMI.getText().toString();
			loan = mLoan.getText().toString();
			tenure = mTenure.getText().toString();
			rate = mAnnual_Interset_Rate.getText().toString();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onButtonListener() {
		
			calculate.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
						loanCalculate();
						
						long loanvalue = Long.parseLong(loan);
						long eligibleLoan = Long.parseLong(eligibleAmount);
						long salary = Long.parseLong(income);
						long expense = Long.parseLong(emi);
						if((salary*0.5)<expense){
							mYesNo.setText("NO");
							mYesNo.setTextColor(Color.parseColor("#ff0000"));;
							mEligible_Amount.setText("0");
						}else{
							if (loanvalue > eligibleLoan){
								mYesNo.setText("NO");
								mYesNo.setTextColor(Color.parseColor("#ff0000"));;
								mEligible_Amount.setText(eligibleAmount);
							}else{
								mYesNo.setText("YES");
								mYesNo.setTextColor(Color.parseColor("#00ff00"));;
								mEligible_Amount.setText(eligibleAmount);
							}
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}catch (Exception e) {
						e.printStackTrace();
					}

				}
			});
		
	}

	public void loanCalculate() {
		try {
			income = mIncome.getText().toString();
			Log.v("income", "" + income);
			emi = mEMI.getText().toString();
			loan = mLoan.getText().toString();
			tenure = mTenure.getText().toString();
			rate = mAnnual_Interset_Rate.getText().toString();

			if (income.trim().equals("") && emi.trim().equals("")
					&& loan.trim().equals("") && tenure.trim().equals("")
					&& rate.trim().equals("")) {
				Toast.makeText(this, "fields Could not be empty",
						Toast.LENGTH_LONG).show();
			} else {
				long monthIncome = Long.parseLong(income);
				Log.v("covertvalue1", ""+monthIncome);
				long Emi = Long.parseLong(emi);
				Log.v("covertvalue2", ""+Emi);
				long loanAmt = Long.parseLong(loan);
				Log.v("covertvalue3", ""+loanAmt);
				int period = Integer.parseInt(tenure);
				Log.v("covertvalue4", ""+period);
				double interestRate = Double.parseDouble(rate);
				
				double rateValue = (interestRate/1200);
				Log.v("rate", "" + rateValue);
				
				double powerValue = Math.pow((rateValue+1), (double)period);
				Log.v("power", "" + powerValue);
				double numeratorValue = (loanAmt*rateValue*powerValue);
				Log.v("numeratorValue", "" + numeratorValue);
				double denumenatorValue = (powerValue-1);
				Log.v("denumenatorValue", "" + denumenatorValue);
				
				long equatedEMI = (long) (numeratorValue/denumenatorValue);
				Log.v("equatedEMI", "" + equatedEMI);
				
				long netAmount = (Emi*loanAmt)/equatedEMI;
				Log.v("amount", "" + netAmount);
				eligibleAmount = String.valueOf(netAmount);
				Log.v("netAmtin string", "" + eligibleAmount);
				
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
