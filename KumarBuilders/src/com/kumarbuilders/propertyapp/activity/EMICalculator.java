package com.kumarbuilders.propertyapp.activity;

import java.text.DecimalFormat;

import com.kumarbuilders.propertyapp.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class EMICalculator extends Activity {

	private EditText ePrinciple, eRate, eTerm, eSIamount, eTotalAmount;
	private TextView txtPrinciple, txtRate, txtTerm;
	private Button CalculateBtn;
	private RadioButton radioYear, radioMonth;
	private RadioGroup termGroup;
	private SeekBar principleBar, rateBar, yeartermBar, monthtermBar;
	private Activity activity;
	private ImageView back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emi_cal);

		activity = EMICalculator.this;
		ePrinciple = (EditText) findViewById(R.id.editAmt);
		eRate = (EditText) findViewById(R.id.editRate);
		eTerm = (EditText) findViewById(R.id.editLoan);
		eSIamount = (EditText) findViewById(R.id.editTotal);
		eTotalAmount = (EditText) findViewById(R.id.editTtlAmount);

		txtPrinciple = (TextView) findViewById(R.id.amountTxt);
		txtRate = (TextView) findViewById(R.id.interstTxt);
		txtTerm = (TextView) findViewById(R.id.loanTxt);

		CalculateBtn = (Button) findViewById(R.id.calBtn);

		termGroup = (RadioGroup) findViewById(R.id.radioGroup1);

		radioYear = (RadioButton) findViewById(R.id.yearRadio);
		radioMonth = (RadioButton) findViewById(R.id.monthRadio);

		principleBar = (SeekBar) findViewById(R.id.seekBar1);
		rateBar = (SeekBar) findViewById(R.id.seekBar2);
		yeartermBar = (SeekBar) findViewById(R.id.seekBar3);
		monthtermBar = (SeekBar) findViewById(R.id.seekBar4);

		back = (ImageView) findViewById(R.id.backImg);

		eSIamount.setEnabled(false);
		eTotalAmount.setEnabled(false);

		handleSeekBar();

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				EMICalculator.this.finish();

			}
		});

	}

	public void handleSeekBar() {
		try {
			final int stepSize = 10000;
			principleBar
					.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

						
						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onProgressChanged(SeekBar seekBar,
								int progress, boolean fromUser) {
							progress = ((int) Math.round(progress / stepSize))
									* stepSize;
							seekBar.setProgress(progress);
							ePrinciple.setText(Integer.toString(progress));

						}
					});

			rateBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress,
						boolean fromUser) {

					eRate.setText(Integer.toString(progress));

				}
			});

			monthtermBar
					.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onProgressChanged(SeekBar seekBar,
								int progress, boolean fromUser) {
							eTerm.setText(Integer.toString(progress));

						}
					});

			yeartermBar
					.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

						@Override
						public void onStopTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onStartTrackingTouch(SeekBar seekBar) {

						}

						@Override
						public void onProgressChanged(SeekBar seekBar,
								int progress, boolean fromUser) {
							eTerm.setText(Integer.toString(progress));

						}
					});

			CalculateBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getCurrentFocus()
							.getWindowToken(), 0);
					Calculate();
				}

			});

			try {
				termGroup
						.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							@Override
							public void onCheckedChanged(RadioGroup group,
									int checkedId) {
								termsGroup(checkedId);

							}
						});

			} catch (Exception e) {
				Log.v("msg", "something went wrong");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void Calculate() {
		try {
			String prin = ePrinciple.getText().toString();
			String rate = eRate.getText().toString();
			String nYr = eTerm.getText().toString();
			if (prin.trim().equals("") || rate.trim().equals("")
					|| nYr.trim().equals("")) {
				Toast.makeText(activity, "fields Could not be empty",
						Toast.LENGTH_LONG).show();
			} else {
				long principle1 = Long.parseLong(prin);
				int nYear1 = Integer.parseInt(nYr);
				int period = (nYear1 * 12);
				double rate1 = Double.parseDouble(rate);
				double rateValue = (rate1 / 1200);
				double powerValue = Math.pow((rateValue + 1), period);
				double numeratorValue = (principle1 * rateValue * powerValue);
				double denumenatorValue = (powerValue - 1);
				double equatedEMI = (numeratorValue / denumenatorValue);

				// long amount = (long) (equatedEMI*period);
				// double amt = (amount / 100.0d);

				DecimalFormat df = new DecimalFormat("#.##");
				long TotalAmount = (long) (equatedEMI * period);
				eTotalAmount.setText(df.format(TotalAmount));
				eSIamount.setText(df.format(equatedEMI));
			}
		} catch (NumberFormatException n) {
			n.printStackTrace();
		}
	}

	private void termsGroup(int checkedId) {
		if (checkedId == radioMonth.getId()) {
			yeartermBar.setVisibility(View.GONE);
			monthtermBar.setVisibility(View.VISIBLE);
			eTerm.setText("");
			yeartermBar.setProgress(0);
			CalculateBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						String p = ePrinciple.getText().toString();
						String r = eRate.getText().toString();
						String n = eTerm.getText().toString();
						if (p.trim().equals("") || r.trim().equals("")
								|| n.trim().equals("")) {
							Toast.makeText(activity,
									"fields Could not be empty",
									Toast.LENGTH_LONG).show();
						} else {

							long principle1 = Long.parseLong(p);
							int nMonth = Integer.parseInt(n);
							double rate1 = Double.parseDouble(r);
							double rateValue = (rate1 / 1200);
							double powerValue = Math.pow((rateValue + 1),
									nMonth);
							double numeratorValue = (principle1 * rateValue * powerValue);
							double denumenatorValue = (powerValue - 1);
							double equatedEMI = (numeratorValue / denumenatorValue);

							DecimalFormat df = new DecimalFormat("#.##");
							long TotalAmount = (long) (equatedEMI * nMonth);
							eTotalAmount.setText(df.format(TotalAmount));
							eSIamount.setText(df.format(equatedEMI));
							
						}
					} catch (NumberFormatException n) {
						n.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} else {
			yeartermBar.setVisibility(View.VISIBLE);
			monthtermBar.setVisibility(View.GONE);
			eTerm.setText("");
			monthtermBar.setProgress(0);
			CalculateBtn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						String p = ePrinciple.getText().toString();
						String r = eRate.getText().toString();
						String n = eTerm.getText().toString();
						if (p.trim().equals("") || r.trim().equals("")
								|| n.trim().equals("")) {
							Toast.makeText(activity,
									"fields Could not be empty",
									Toast.LENGTH_LONG).show();
						} else {
							long principle1 = Long.parseLong(p);
							int nYear1 = Integer.parseInt(n);
							int period = (nYear1 * 12);
							double rate1 = Double.parseDouble(r);
							double rateValue = (rate1 / 1200);
							double powerValue = Math.pow((rateValue + 1),
									period);
							double numeratorValue = (principle1 * rateValue * powerValue);
							double denumenatorValue = (powerValue - 1);
							double equatedEMI = (numeratorValue / denumenatorValue);

							DecimalFormat df = new DecimalFormat("#.##");
							long TotalAmount = (long) (equatedEMI * period);
							eTotalAmount.setText(df.format(TotalAmount));
							eSIamount.setText(df.format(equatedEMI));
						}
					} catch (NumberFormatException n) {
						n.printStackTrace();
					}
				}
			});
		}
	}

}
