package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.domain.PropertyCompare;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDbImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyCompareService;
import com.kumarbuilders.propertyapp.ormservices.PropertyCompareServiceImpl;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;
import com.kumarbuilders.propertyapp.tasks.AsyncHttpPost;
import com.kumarbuilders.propertyapp.tasks.TaskResponse;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.TypefacedTextView;
import com.kumarbuilders.propertyapp.utils.UrlConstantUtils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;

public class PropertyCompareListActivity extends Activity implements TaskResponse {

	private Activity activity;
	private ListView mProperty_list;
	private ImageView backButton, searchBtn;
	private TypefacedTextView txtHeader;
	private String mSelectedPropertyName, mHospital, mSchool, mPlcTrans,
			mGrcryMrkt, mVisibility, mFlatArea, mFlrRise, mPropertName,
			mProjectName, mDevelopers, mRatePerSqFt, mLoading, mCarpetArea,
			mBankApproval, signupUserId, userProperty_type;
	private int propertyId;
	private List<String> comparePropertiesNameList = new ArrayList<String>();
	private ArrayAdapter<String> adapter;
	private PropertyCompareService propertyCompareService;
	public List<PropertyCompare> compareServerList = new ArrayList<PropertyCompare>();
	private List<Integer> mOtherPropertyId = new ArrayList<Integer>();
	private TextView mSaveButton;
	private EditText searchProperty;
	private Bundle extras;
	private PropertyService propertyService;
	private PropertiesDetails propertyDetails;
	private JSONObject jObject, jsonobjectUpdate, jsonObject, jsonOtherObject;
	private JSONArray userUpdateJsonArray, jsonarray;
	private UserSignUp signUpInfo;
	public AddSignUpToDbImpl signupData;
	private Boolean isInternetPresent= false;
	private ProgressDialog pDialog;

	// this is the categories text view
	private TextView mKulPropertyName, mKulProjectName, mKulHospital,
			mKulSchool, mKulPublicTrans, mKulGroceryMrkt, mKulVisibility,
			mKulDeveloper, mKulFltArea, mKulRate_per_sq_ft, mKulFlr_rise,
			mKulLoadingArea, mKulCarpetarea, mKulBank_Approval;

	// this is the categories text view
	private TextView mPpt1PropertyName, mPpt1ProjectName, mPpt1Developer;

	private EditText mPpt1Hospital, mPpt1School, mPpt1PublicTrans,
			mPpt1GroceryMrkt, mPpt1Visibility, mPpt1FltArea,
			mPpt1Rate_per_sq_ft, mPpt1Flr_rise, mPpt1LoadingArea,
			mPpt1Carpetarea, mPpt1Bank_Approval;

	// linear layout of kul and other properties
	private LinearLayout mKulLayout, mPpt1Layout, mPpt2Layout, mPpt3Layout,
			mUserPptLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.properties_compare);
		ActionBar bar = getActionBar();
		bar.hide();
		activity = PropertyCompareListActivity.this;
		initFunction();
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		isInternetPresent = CheckNetwork.isInternetPresent(activity);
		try {
			extras = getIntent().getExtras();
			propertyId = extras.getInt("propertyId");

			propertyCompareService = new PropertyCompareServiceImpl();
			compareServerList.clear();
			compareServerList = (List<PropertyCompare>) propertyCompareService
					.getPropertyCompareDatabyId("propertycompare_id",
							propertyId);

			for (int i = 0; i < compareServerList.size(); i++) {
				comparePropertiesNameList.add(compareServerList.get(i)
						.getPropertyName());
			}

			adapter = new ArrayAdapter<String>(getApplicationContext(),
					R.layout.list_item, R.id.product_name,
					comparePropertiesNameList);
			mProperty_list.setAdapter(adapter);
			mProperty_list.setVisibility(View.GONE);

			searchBtn.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					searchProperty.setVisibility(View.VISIBLE);
					txtHeader.setVisibility(View.GONE);

				}
			});
			backButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(PropertyCompareListActivity.this,
							PropertyDetailsActivity.class);
					intent.putExtras(extras);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					activity.finish();

				}
			});

			getLoginData();
			mAsyncTaskCall();
			mSearchProperties();
			setKulProperty();

			mSaveButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					try {

						if (isInternetPresent) {
							postPropertyToServer();
						} else {
							CheckNetwork.showNetworkConnectionMessage(activity);
						}

					} catch (Exception e) {

					}

				}
			});
			searchProperty
					.setOnEditorActionListener(new OnEditorActionListener() {

						@Override
						public boolean onEditorAction(TextView v, int actionId,
								KeyEvent event) {
							if (actionId == EditorInfo.IME_ACTION_DONE) {
								userSearchedProperty();
								return true;
							}
							return false;
						}
					});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void initFunction() {
		try {
			backButton = (ImageView) findViewById(R.id.pptBackImg);
			mSaveButton = (TextView) findViewById(R.id.saveBtn);
			mSaveButton.setVisibility(View.GONE);
			txtHeader = (TypefacedTextView) findViewById(R.id.propComptxtheader);// visibility
			searchBtn = (ImageView) findViewById(R.id.searchicon);
			searchProperty = (EditText) findViewById(R.id.pcwsearchEdtTxt);// visibility
			mProperty_list = (ListView) findViewById(R.id.propComplist_view);// visibility
			mProperty_list.setVisibility(View.GONE);

			// kul view
			mKulPropertyName = (TextView) findViewById(R.id.kulPptName);
			mKulProjectName = (TextView) findViewById(R.id.txtkulprojtname);
			mKulHospital = (TextView) findViewById(R.id.txtkulHospital);
			mKulSchool = (TextView) findViewById(R.id.txtkulSchool);
			mKulPublicTrans = (TextView) findViewById(R.id.txtkulPblcTrnspt);
			mKulGroceryMrkt = (TextView) findViewById(R.id.txtkulGroyMrkt);
			mKulVisibility = (TextView) findViewById(R.id.txtkulVisibility);
			mKulDeveloper = (TextView) findViewById(R.id.txtkuldeveloper);
			mKulFltArea = (TextView) findViewById(R.id.txtkularea);
			mKulRate_per_sq_ft = (TextView) findViewById(R.id.txtkulRate);
			mKulFlr_rise = (TextView) findViewById(R.id.txtkulFlrRise);
			mKulLoadingArea = (TextView) findViewById(R.id.txtkulLoadingArea);
			mKulCarpetarea = (TextView) findViewById(R.id.txtkulCarpetArea);
			mKulBank_Approval = (TextView) findViewById(R.id.txtkulBnkApprvl);

			// property 1 text view
			mPpt1PropertyName = (TextView) findViewById(R.id.Ppt1Name);
			mPpt1ProjectName = (TextView) findViewById(R.id.txtppt1projtname);
			mPpt1Hospital = (EditText) findViewById(R.id.txtPpt1Hospital);
			mPpt1School = (EditText) findViewById(R.id.txtPpt1School);
			mPpt1PublicTrans = (EditText) findViewById(R.id.txtPpt1PblcTrnspt);
			mPpt1GroceryMrkt = (EditText) findViewById(R.id.txtPpt1GroyMrkt);
			mPpt1Visibility = (EditText) findViewById(R.id.txtPpt1Visibility);
			mPpt1Developer = (TextView) findViewById(R.id.txtppt1developer);
			mPpt1FltArea = (EditText) findViewById(R.id.txtPpt1area);
			mPpt1Rate_per_sq_ft = (EditText) findViewById(R.id.txtPpt1Rate);
			mPpt1Flr_rise = (EditText) findViewById(R.id.txtPpt1FlrRise);
			mPpt1LoadingArea = (EditText) findViewById(R.id.txtPpt1LoadingArea);
			mPpt1Carpetarea = (EditText) findViewById(R.id.txtPpt1CarpetArea);
			mPpt1Bank_Approval = (EditText) findViewById(R.id.txtPpt1BnkApprvl);

			// layout of all properties
			mKulLayout = (LinearLayout) findViewById(R.id.kulLinearLayout);
			mPpt1Layout = (LinearLayout) findViewById(R.id.ppt1LinearLayout);

			mSaveButton.setVisibility(View.GONE);
			mPpt1Layout.setVisibility(View.GONE);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mAsyncTaskCall() {
		try {
			new AsyncHttpPost(activity, UrlConstantUtils.PROPERTY_COMPARE,
					PropertyCompareListActivity.this, true)
					.execute(property_compare("" + extras.getInt("propertyId"),
							signupUserId));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject property_compare(String property_id, String user_id) {
		JSONObject compare = new JSONObject();
		try {
			compare.put("property_id", property_id);
			compare.put("user_id", user_id);

		} catch (JSONException e) {
		}
		return compare;
	}

	private void mSearchProperties() {
		try {

			searchProperty.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence propertyName, int arg1,
						int arg2, int arg3) {
					if (propertyName.length() == 0) {
						mProperty_list.setVisibility(View.GONE);
					} else {
						PropertyCompareListActivity.this.adapter.getFilter()
								.filter(propertyName);
						mProperty_list.setVisibility(View.VISIBLE);
					}

				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {

				}

				@Override
				public void afterTextChanged(Editable arg0) {

				}
			});

			mProperty_list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					mSelectedPropertyName = (String) mProperty_list
							.getItemAtPosition(position);
					mProperty_list.setVisibility(View.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
							0);

					for (int i = 0; i < compareServerList.size(); i++) {
						if (mSelectedPropertyName
								.equalsIgnoreCase(compareServerList.get(i)
										.getPropertyName())) {
							try {
								int a = compareServerList.get(i)
										.getProperty_id();
								setFirstOtherProperty(a);
								break;
							} catch (NullPointerException e) {
								e.printStackTrace();
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}

					System.out
							.println("selected name " + mSelectedPropertyName);
					// mSearchTxt.setText(mSelectedPropertyName);

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void userSearchedProperty() {
		try {
			mSelectedPropertyName = searchProperty.getText().toString();
			mProperty_list.setVisibility(View.GONE);
			InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

			Boolean propertySearchedResultFlag = false;
			for (int i = 0; i < compareServerList.size(); i++) {
				if (mSelectedPropertyName.equalsIgnoreCase(compareServerList
						.get(i).getPropertyName())) {
					try {
						propertySearchedResultFlag = true;
						int a = compareServerList.get(i).getProperty_id();
						setFirstOtherProperty(a);
						break;
					} catch (NullPointerException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (propertySearchedResultFlag == false) {
				mycustomAlertDialog();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void mycustomAlertDialog() {
		try {
			new AlertDialog.Builder(this)
					.setIcon(R.drawable.appiconpopup)
					.setTitle("Kumar Builder")
					.setMessage("We have no such property in our database ")
					.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
								}
							}).show();
		} catch (Exception e) {

		}
	}

	public void setKulProperty() {
		try {
			propertyService = new PropertyServiceImpl();

			PropertiesDetails property = (PropertiesDetails) propertyService
					.findById(propertyId);

			try {
				mKulPropertyName
						.setText(property.getProperty_name().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulProjectName.setText(property.getKulproject_name()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulHospital.setText(property.getKulhospital().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				mKulSchool.setText(property.getKulschool().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulPublicTrans.setText(property.getKulpublic_transport()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulGroceryMrkt.setText(property.getKulgrocery_market()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulVisibility.setText(property.getKulvisibility().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulDeveloper.setText(property.getKuldevelopers().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulFltArea.setText(property.getKulflat_area().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulFlr_rise.setText(property.getKulfloor_rise().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulRate_per_sq_ft.setText(property.getKulrate_per_sqrt_foot()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulLoadingArea.setText(property.getKulloading().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulCarpetarea.setText(property.getKulcarpet_area().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mKulBank_Approval.setText(property.getKulbank_approval()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setFirstOtherProperty(int oProperty_id) {
		try {
			mPpt1Layout.setVisibility(View.VISIBLE);
			mSaveButton.setVisibility(View.VISIBLE);
			propertyCompareService = new PropertyCompareServiceImpl();

			PropertyCompare property1 = (PropertyCompare) propertyCompareService
					.findById(oProperty_id);

			try {
				mPpt1PropertyName.setText(property1.getPropertyName()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1ProjectName
						.setText(property1.getProject_name().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Hospital.setText(property1.getHospital().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1School.setText(property1.getSchool().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1PublicTrans.setText(property1.getPublic_transport()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1GroceryMrkt.setText(property1.getGrocery_market()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Visibility.setText(property1.getVisibility().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Developer.setText(property1.getDevelopers().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1FltArea.setText(property1.getFlat_area().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Flr_rise.setText(property1.getFloor_rise().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Rate_per_sq_ft.setText(property1.getRate_per_sqrt_foot()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1LoadingArea.setText(property1.getLoading().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Carpetarea.setText(property1.getCarpet_area().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				mPpt1Bank_Approval.setText(property1.getBank_approval()
						.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		Intent intent = new Intent(PropertyCompareListActivity.this,
				PropertyDetailsActivity.class);
		intent.putExtras(extras);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		activity.finish();
	}

	public void postPropertyToServer() {
		try {
			getUserPropertyValue();
			pDialog = new ProgressDialog(activity);
			pDialog.setMessage("please wait...");
			pDialog.setCancelable(false);
			pDialog.show();
			if (isInternetPresent) {

				// UPDATE PROPERTY
				new AsyncHttpPost(activity,
						UrlConstantUtils.USER_COMPARE_PROPERTY,
						PropertyCompareListActivity.this, true)
						.execute(sendUserPropertyDetails(propertyId,
								mPropertName, signupUserId, mProjectName,
								mHospital, mSchool, mPlcTrans, mGrcryMrkt,
								mVisibility, mDevelopers, mFlatArea,
								mRatePerSqFt, mFlrRise, mLoading, mCarpetArea,
								mBankApproval, "update"));
			} else {
				CheckNetwork.showNetworkConnectionMessage(activity);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject sendUserPropertyDetails(int propert_id,
			String propertyName, String userId, String projectName,
			String hospital, String school, String publicTransport,
			String groceryMrkt, String mVisibility, String developer,
			String fltArea, String ratePerSqFt, String flrRise,
			String loadingArea, String carpetArea, String bankAppr,
			String action) {

		JSONObject userEditProperty = new JSONObject();
		try {
			userEditProperty.put("property_id", propert_id);
			userEditProperty.put("property_name", propertyName);
			userEditProperty.put("user_id", userId);
			userEditProperty.put("project_name", projectName);
			userEditProperty.put("hospital", hospital);
			userEditProperty.put("school", school);
			userEditProperty.put("public_transport", publicTransport);
			userEditProperty.put("grocery_market", groceryMrkt);
			userEditProperty.put("visibility", mVisibility);
			userEditProperty.put("developers", developer);
			userEditProperty.put("flat_area", fltArea);
			userEditProperty.put("rate", ratePerSqFt);
			userEditProperty.put("floor_rise", flrRise);
			userEditProperty.put("loading_area", loadingArea);
			userEditProperty.put("carpet_area", carpetArea);
			userEditProperty.put("bank_approval", bankAppr);
			userEditProperty.put("action", action);

		} catch (JSONException e) {

		} catch (Exception e) {

		}
		return userEditProperty;
	}

	public void getUserPropertyValue() {
		try {

			mPropertName = mPpt1PropertyName.getText().toString();
			mProjectName = mPpt1ProjectName.getText().toString();
			mHospital = mPpt1Hospital.getText().toString();
			mSchool = mPpt1School.getText().toString();
			mPlcTrans = mPpt1PublicTrans.getText().toString();
			mGrcryMrkt = mPpt1GroceryMrkt.getText().toString();
			mVisibility = mPpt1Visibility.getText().toString();
			mDevelopers = mPpt1Developer.getText().toString();
			mRatePerSqFt = mPpt1Rate_per_sq_ft.getText().toString();
			mLoading = mPpt1LoadingArea.getText().toString();
			mCarpetArea = mPpt1Carpetarea.getText().toString();
			mBankApproval = mPpt1Bank_Approval.getText().toString();

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Override
	public void taskResponse(String response) {
		String status = null;
		if (response != null) {
			try {
				jObject = new JSONObject(response.toString());
				status = jObject.getString("STATUS");
				Log.v("status", "" + status);
				if (status.trim().equals("SUCCESS")) {
					Log.v("user create property------>>>>>", "other_properties");
					// Toast.makeText(activity, "other_properties",
					// Toast.LENGTH_LONG).show();
					jsonObject = jObject.getJSONObject("data");

					jsonarray = jsonObject.getJSONArray("other_properties");
					Log.v("KulOtherArray", "" + jsonarray);
					for (int i = 0; i < jsonarray.length(); i++) {
						jsonOtherObject = jsonarray.getJSONObject(i);
						userProperty_type = jsonOtherObject.getString(
								"property_type").toString();

						if (userProperty_type.equalsIgnoreCase("USER")) {
							// setUserProperties();
						}
					}

				}

				if (status.trim().equalsIgnoreCase("ADD")) {

					Log.v("user create property------>>>>>", "ADD");
					// Toast.makeText(activity, "ADD",
					// Toast.LENGTH_LONG).show();
					userUpdateJsonArray = jObject.getJSONArray("data");
					// setUserUpdateProperty();
					pDialog.dismiss();
					Toast.makeText(activity, "Added Successfully!!!",
							Toast.LENGTH_SHORT).show();

				} else if (status.trim().equalsIgnoreCase("UPDATE")) {
					// Toast.makeText(activity, "UPDATE",
					// Toast.LENGTH_LONG).show();
					Log.v("user create property------>>>>>", "UPDATE");
					userUpdateJsonArray = jObject.getJSONArray("data");
					// setUserUpdateProperty();
					pDialog.dismiss();
					Toast.makeText(activity, "Updated Successfully!!!",
							Toast.LENGTH_SHORT).show();

				}
			} catch (Exception e) {
			}
		}

	}

	private void getLoginData() {
		try {
			signupData = new AddSignUpToDbImpl();

			List<UserSignUp> signuplist = signupData.getAllDetails();
			if (signuplist != null && signuplist.size() != 0) {

				signUpInfo = new UserSignUp();
				if (signuplist.size() != 0) {
					for (int i = 0; i < signuplist.size(); i++) {
						signUpInfo = signuplist.get(i);
						signupUserId = signUpInfo.getUserId();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
