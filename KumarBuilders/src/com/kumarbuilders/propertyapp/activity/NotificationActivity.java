package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.NotificationAdapter;
import com.kumarbuilders.propertyapp.domain.NotificationDB;
import com.kumarbuilders.propertyapp.ormservices.NotificationService;
import com.kumarbuilders.propertyapp.ormservices.NotificationServiceImpl;

public class NotificationActivity extends FragmentActivity {

	ListView list;
	public static NotificationAdapter notificationAdapter;
	private ArrayList<String> noificationList = new ArrayList<String>();
	ImageView imgBack;
	private TextView txtNotifyMsg;
	NotificationService notifyService;
	ArrayList<NotificationDB> notifyList = new ArrayList<NotificationDB>();

	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.notification);

		// Intent Message sent from Broadcast Receiver
		String str = getIntent().getStringExtra("msg");

		try {

			list = (ListView) findViewById(R.id.notificationlist);

			imgBack = (ImageView) findViewById(R.id.imgback);
			txtNotifyMsg = (TextView) findViewById(R.id.txtnotifymsg);

			try {
				notificationAdapter = new NotificationAdapter(this, getallnotifications());
				if (notifyList.size() <= 0) {
					txtNotifyMsg.setVisibility(View.VISIBLE);
					list.setVisibility(View.GONE);
				}

				// Set adapter to listview
				if (notifyList != null || !notifyList.isEmpty()) {
					list.setAdapter(notificationAdapter);
				}
				notificationAdapter.notifyDataSetChanged();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
					new AlertDialog.Builder(NotificationActivity.this).setIcon(R.drawable.appicon)
							.setTitle("Kumar Builder").setMessage("Are you sure you want delete this notification ")
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							try {

								NotificationDB notify = (NotificationDB) notifyService
										.findById(notifyList.get(position).getId());
								int statusDelete = notifyService.deleteById(notify.getId());

								notifyList.remove(notify);
								notifyList.remove(position);
								notificationAdapter.notifyDataSetChanged();

							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}).setNegativeButton("No", null).show();

				}
			});

			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					NotificationActivity.this.finish();

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private List<NotificationDB> getallnotifications() {

		try {
			notifyService = new NotificationServiceImpl();
			notifyList = (ArrayList<NotificationDB>) notifyService.getAllData();
			if (notifyList.size() <= 0) {
				// CheckNetwork.showDataNotFoundMessage(getActivity());
				txtNotifyMsg.setVisibility(View.VISIBLE);
				list.setVisibility(View.GONE);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return notifyList;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// finish();
	}

}
