//**************************************************************************************************
//**************************************************************************************************
//                                    Copyright (c) 2010 by PURPLETALK INC//                                              ALL RIGHTS RESERVED
//**************************************************************************************************
//**************************************************************************************************
//
//    Project name                    		: Heath Assist
//    Class Name                              : HealthAssist
//    Date                                    : June-24-2011
//    Author                                  : A .Praveena
//    Version                                 : 1.1
//
//***************************************************************************************************
//    Class Description: This is the splash screen 
//
//***************************************************************************************************
//    Update history:
//    Date :                          Developer Name :Praveena A     Modification Comments :
//
//***************************************************************************************************

package com.kumarbuilders.propertyapp.activity;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.adapter.GalleryAdapter;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

public class GalleryActivity extends FragmentActivity {

	private Activity activity;
	private ImageView imgBack;

	private GridView gv;
	private ImageView selected_imageview;
	private TextView txtheader;
	private Bundle extras;
	private String activityName;
	private ArrayList<String> drawables = null;
	private int propertyId;
	PropertyService propertyService;
	private ImageLoader imageLoader;
	public static ProgressBar loading_spinner;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			setContentView(R.layout.gallery);

			activity = GalleryActivity.this;

			gv = (GridView) findViewById(R.id.gridView1);
			imgBack = (ImageView) findViewById(R.id.imgback);
			txtheader = (TextView) findViewById(R.id.txtheader);
			selected_imageview = (ImageView) findViewById(R.id.selected_imageview);
			loading_spinner = (ProgressBar) findViewById(R.id.loading_spinner);

			extras = getIntent().getExtras();
			activityName = extras.getString("activityname");
			drawables = extras.getStringArrayList("gallery");
			propertyId = extras.getInt("propertyId");

			propertyService = new PropertyServiceImpl();

			imageLoader = new ImageLoader(activity.getApplicationContext(), true,"gallery");
			propertyService = new PropertyServiceImpl();
			final PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);

			try {
				if (drawables != null && drawables.size() > 0) {

					if (drawables.get(0).equals("") || drawables.isEmpty()) {

						String imageUrl3 = property.getProperty_imgurl();
						final String[] urls3 = imageUrl3.split(" ");

						imageLoader.DisplayImage(urls3[0], selected_imageview, 0);
						gv.setAdapter(new GalleryAdapter(this, drawables, selected_imageview, propertyId));
					} else {
						gv.setAdapter(new GalleryAdapter(this, drawables, selected_imageview, propertyId));
						String imageUrl3 = drawables.get(0);
						final String[] urls3 = imageUrl3.split(" ");

						imageLoader.DisplayImage(urls3[0], selected_imageview, 0);

					}

				} else {
					String imageUrl2 = property.getProperty_imgurl();
					final String[] urls2 = imageUrl2.split(" ");

					imageLoader.DisplayImage(urls2[0], selected_imageview, 0);
					gv.setAdapter(new GalleryAdapter(this, drawables, selected_imageview, propertyId));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (activityName.equals("gallery")) {
				txtheader.setText("Gallery");
			} else if (activityName.equals("projectplan")) {
				txtheader.setText("Project Plans");
			} else if (activityName.equals("floorplan")) {
				txtheader.setText("Floor Plans");
			} else if (activityName.equals("buildingplan")) {
				txtheader.setText("Building Plans");
			} else if (activityName.equals("unitplan")) {
				txtheader.setText("Unit Plans");
			} else if (activityName.equals("constructionplan")) {
				txtheader.setText("Construction Photos");
			}
			imgBack.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {

					if (activityName.equals("projectplan") || activityName.equals("floorplan")
							|| activityName.equals("buildingplan") || activityName.equals("unitplan")
							|| activityName.equals("constructionplan")) {
						finish();

					} else {
						Intent intent = new Intent(GalleryActivity.this, PropertyDetailsActivity.class);
						intent.putExtras(extras);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						activity.finish();
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@SuppressLint("NewApi")
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		// this.finish();
		if (activityName.equals("projectplan") || activityName.equals("floorplan")
				|| activityName.equals("buildingplan") || activityName.equals("unitplan")
				|| activityName.equals("constructionplan")) {
			finish();

		} else {
			Intent intent = new Intent(GalleryActivity.this, PropertyDetailsActivity.class);
			intent.putExtras(extras);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			activity.finish();
		}
	}

	@Override
	protected void onStop() {

		super.onStop();
	}

}
