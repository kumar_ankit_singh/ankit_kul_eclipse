package com.kumarbuilders.propertyapp.adapter;

import java.util.HashMap;
import java.util.List;

import com.kumarbuilders.propertyapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	private Context mcontext;
	private List<String> parentList;
	private HashMap<String, List<String>> childList;

	public ExpandableListAdapter(Context mcontext, List<String> parentList,
			HashMap<String, List<String>> childList) {

		this.mcontext = mcontext;
		this.parentList = parentList;
		this.childList = childList;
	}

	@Override
	public int getGroupCount() {

		return this.parentList.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {

		return this.childList.get(this.parentList.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {

		return this.parentList.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {

		return this.childList.get(this.parentList.get(groupPosition)).get(
				childPosition);
	}

	@Override
	public long getGroupId(int groupPosition) {

		return groupPosition;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {

		return childPosition;
	}

	@Override
	public boolean hasStableIds() {

		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		try {
			final String parentList = (String) getGroup(groupPosition);
			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) this.mcontext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.parent_list, null);
			}
			TextView parentTxtView = (TextView) convertView
					.findViewById(R.id.parentTxt);
			parentTxtView.setText(parentList);
			
		} catch (Exception e) {

		}
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		try {
			final String childText = (String) getChild(groupPosition,
					childPosition);

			if (convertView == null) {
				LayoutInflater inflater = (LayoutInflater) this.mcontext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.child_list, null);
			}
			TextView childTxtView = (TextView) convertView
					.findViewById(R.id.childTxt);
			childTxtView.setText(childText);
		} catch (Exception e) {

		}
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {

		return true;
	}

}
