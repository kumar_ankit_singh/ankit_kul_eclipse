
package com.kumarbuilders.propertyapp.adapter;

import android.database.DataSetObserver;


public interface WheelAdapter {
	/**
	 * Gets items count
	 * @return the count of wheel items
	 */
	public int getItemsCount();
	
	/**
	 * Gets a wheel item by index.
	 * 
	 * @param index the item index
	 * @return the wheel item text or null
	 */
	public String getItem(int index);
	
	/**
	 * Gets maximum item length. It is used to determine the wheel width. 
	 * If -1 is returned there will be used the default wheel width.
	 * 
	 * @return the maximum item length or -1
	 */
	public int getMaximumLength();
	public CharSequence getItemText(int index);
	 public void registerDataSetObserver(DataSetObserver observer);
     
     /**
      * Unregister an observer that has previously been registered
      * @param observer the observer to be unregistered
      */
     void unregisterDataSetObserver (DataSetObserver observer);
}
