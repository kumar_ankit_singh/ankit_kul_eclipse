package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.UserMessage;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChatMessageAdapter extends BaseAdapter {

	
	private static LayoutInflater inflater = null;
    private List<UserMessage> userMessageList = null;
    public View rootView = null;
    private TextView msg,time;
 
    public ChatMessageAdapter(Activity activity, ArrayList<UserMessage> list) {
    	userMessageList = list;
        inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
    }
	
	@Override
	public int getCount() {
		return userMessageList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		UserMessage userMessage = userMessageList.get(position);
		rootView = convertView;
		if (convertView == null){
			rootView = inflater.inflate(R.layout.from_chat_bubble, null);
			msg = (TextView) rootView.findViewById(R.id.message_text);
			time = (TextView) rootView.findViewById(R.id.text_time);
	        msg.setText(userMessage.getMessage());
	        time.setText(userMessage.getTime());
	        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.bubble_layout);
	        LinearLayout parent_layout = (LinearLayout) rootView.findViewById(R.id.bubble_layout_parent);
	        
	        if (userMessage.isUser()) {
	            layout.setBackgroundResource(R.drawable.bubble2);
	            parent_layout.setGravity(Gravity.RIGHT);
	        }
	        // If not mine then align to left
	        else {
	            layout.setBackgroundResource(R.drawable.bubble1);
	            parent_layout.setGravity(Gravity.LEFT);
	        }
	        msg.setTextColor(Color.BLACK);
	        return rootView;
		}
		return convertView;
	}
	
	

}
