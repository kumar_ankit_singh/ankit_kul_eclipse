package com.kumarbuilders.propertyapp.adapter;

import java.util.List;

import com.kumarbuilders.propertyapp.utils.CommonFunctions;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class GalleryImageAdapter extends BaseAdapter {

	private Activity context;

	private static ImageView imageView;

	private List<Integer> plotsImages;

	private static ViewHolder holder;

	public GalleryImageAdapter(Activity context, List<Integer> plotsImages) {

		this.context = context;
		this.plotsImages = plotsImages;

	}

	@Override
	public int getCount() {
		return plotsImages.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		try {
			if (convertView == null) {

				holder = new ViewHolder();

				imageView = new ImageView(this.context);

				imageView.setPadding(3, 3, 3, 3);

				convertView = imageView;

				holder.imageView = imageView;
				convertView.setTag(holder);

			} else {

				holder = (ViewHolder) convertView.getTag();
			}

			// holder.imageView.setImageDrawable(plotsImages.get(position));

			holder.imageView.setImageBitmap(CommonFunctions.decodeFile(plotsImages.get(position), context));

			holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			holder.imageView.setLayoutParams(new Gallery.LayoutParams(150, 90));
		} catch (Exception e) {

		}

		return imageView;
	}

	private static class ViewHolder {
		ImageView imageView;
	}

}