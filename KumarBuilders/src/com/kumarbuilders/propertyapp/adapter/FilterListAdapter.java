package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.FilterOptionsActivity;
import com.kumarbuilders.propertyapp.activity.WheelActivity;

public class FilterListAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater = null;
	private List<String> mPropertyList = new ArrayList<String>();
	public FilterListAdapter(Activity a, List<String> list, String description,
			String list_item) {
		activity = a;
		this.mPropertyList = list;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return mPropertyList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try {
			if (convertView == null)
				vi = inflater.inflate(R.layout.filterpropertyrow, null);
			TextView txtTitle = (TextView) vi.findViewById(R.id.title);
			TextView txtsubTitle = (TextView) vi.findViewById(R.id.description);

			if (mPropertyList.get(position).toString().equals("Sort result By")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.sort_Result);

			} else if (mPropertyList.get(position).toString()
					.equals("For Rent/For Sale")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.rent_Sale);
			} else if (mPropertyList.get(position).toString()
					.equals("House Type")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.house_Type);
			} else if (mPropertyList.get(position).toString()
					.equals("Min-Max Price")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + WheelActivity.max_Price);
			} else if (mPropertyList.get(position).toString()
					.equals("Bedrooms")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + WheelActivity.min_Bedroom);
			} else if (mPropertyList.get(position).toString()
					.equals("Area")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + WheelActivity.min_Area);
			} else if (mPropertyList.get(position).toString()
					.equals("Last Updated")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.last_Update);
			} else if (mPropertyList.get(position).toString()
					.equals("Cities,Towns and Location")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.cities_Towns);
			} else if (mPropertyList.get(position).toString()
					.equals("Amenities")) {
				txtTitle.setText(mPropertyList.get(position).toString());
				txtsubTitle.setText("" + FilterOptionsActivity.amenities);
			} else {

				txtTitle.setText(mPropertyList.get(position).toString());

			}
		} catch (Exception e) {

		}
		return vi;
	}

	public int getPosition(int list_item) {
		// TODO Auto-generated method stub
		return list_item;
	}

}