package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.FavProperty;
import com.kumarbuilders.propertyapp.fragments.CustomerFavPropertyFragment;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;

public class FavPropertiesAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater = null;
	private ImageLoader imageLoader;
	private List<FavProperty> mPropertyList = new ArrayList<FavProperty>();

	public FavPropertiesAdapter(Activity a, List<FavProperty> list) {
		activity = a;
		this.mPropertyList = list;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return mPropertyList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try {
			if (convertView == null)
				vi = inflater.inflate(R.layout.favpropertiesrow, null);
			TextView favuserpropertyname = (TextView) vi.findViewById(R.id.favpropertyName);
			ImageView favImage = (ImageView) vi.findViewById(R.id.favpropertyimage);

			TextView favpropertylocation = (TextView) vi.findViewById(R.id.favpropertyLocation);

			if (mPropertyList.get(position).getPropertyName() == null) {

				mPropertyList.remove(position);
				notifyDataSetChanged();
				notifyDataSetInvalidated();

			} else {
				favuserpropertyname.setText(mPropertyList.get(position).getPropertyName());

				try {
					String imageUrl = mPropertyList.get(position).getPropertyimage();
					final String[] urls = imageUrl.split(" ");

					imageLoader.DisplayImage(urls[0], favImage, position);
				} catch (Exception e) {
					e.printStackTrace();
				}

				favpropertylocation.setText(mPropertyList.get(position).getPropertyLocation());
			}
		} catch (Exception e) {

		}

		return vi;
	}

	public void sendEmail(String string) {
		try {

			Intent sendIntent = new Intent(Intent.ACTION_SEND);
			// Mime type of the attachment (or) u can use
			// sendIntent.setType("*/*")
			sendIntent.setType("text/xml");
			// Subject for the message or Email
			sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Hello");
			sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { string });

			final PackageManager pm = activity.getPackageManager();
			final List<ResolveInfo> matches = pm.queryIntentActivities(sendIntent, 0);
			ResolveInfo best = null;
			for (final ResolveInfo info : matches)
				if (info.activityInfo.packageName.endsWith(".gm")
						|| info.activityInfo.name.toLowerCase().contains("gmail"))
					best = info;
			if (best != null)
				sendIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);

			activity.startActivity(sendIntent);

		} catch (Exception e) {

		}

	}

}