package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;
import com.kumarbuilders.propertyapp.ormservices.PropertyService;
import com.kumarbuilders.propertyapp.ormservices.PropertyServiceImpl;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class GalleryAdapter extends BaseAdapter {

	Context context;
	ArrayList<String> imageId;
	private static LayoutInflater inflater = null;
	ImageView img;
	private ImageLoader imageLoader;
	private Integer propertyId;
	PropertyService propertyService;

	public GalleryAdapter(Activity mainActivity, ArrayList<String> drawables, ImageView imgview, int propertyId) {

		context = mainActivity;
		imageId = drawables;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		img = imgview;
		imageLoader = new ImageLoader(context);
		this.propertyId = propertyId;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (imageId != null && imageId.size() > 0) {
			return imageId.size();
		} else {
			return 0;
		}

	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder {
		ImageView img;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final Holder holder = new Holder();
		View rowView = null;
		// TODO Auto-generated method stub
		try {
			propertyService = PropertyServiceImpl.getInstance();
			final String[] urls;
			rowView = inflater.inflate(R.layout.gallery_list, null);
			holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

			// holder.img.setImageResource(imageId.get(0));

			String imageUrl = imageId.get(position);
			

			if (imageUrl.length() != 0) {
				urls = imageUrl.split(" ");
				imageLoader = new ImageLoader(context.getApplicationContext());
				imageLoader.DisplayImage(urls[0], holder.img, position);
			} else {

				propertyService = new PropertyServiceImpl();
				final PropertiesDetails property = (PropertiesDetails) propertyService.findById(propertyId);
				imageUrl = property.getProperty_imgurl();
				urls = imageUrl.split(" ");
				imageLoader = new ImageLoader(context.getApplicationContext());
				imageLoader.DisplayImage(urls[0], holder.img, position);

			}

			rowView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					imageLoader.DisplayImage(urls[0], img, position);

				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}
		return rowView;
	}

}