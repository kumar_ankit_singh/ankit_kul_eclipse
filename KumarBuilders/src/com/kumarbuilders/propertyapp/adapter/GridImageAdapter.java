package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import com.kumarbuilders.propertyapp.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridImageAdapter extends BaseAdapter {

	String[] result;
	Context context;
	int[] imageId;
	private static LayoutInflater inflater = null;
	List<Integer> amenitiesList = new ArrayList<Integer>();;

	public GridImageAdapter(Activity activity, List<Integer> intList) {
		context = activity;
		amenitiesList = intList;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return amenitiesList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder {
		TextView tv;
		ImageView img;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = new Holder();
		View rowView = null;

		try {
			rowView = inflater.inflate(R.layout.program_list, null);
			holder.tv = (TextView) rowView.findViewById(R.id.textView1);
			holder.img = (ImageView) rowView.findViewById(R.id.imageView1);

			int pos = amenitiesList.get(position);

			if (amenitiesList.contains(pos)) {

				if (pos == 1) {
					holder.tv.setText(R.string.squashcourt);
					holder.img.setImageResource(R.drawable.squashcourt);
				} else if (pos == 2) {
					holder.tv.setText(R.string.guestlounge);
					holder.img.setImageResource(R.drawable.guestlounge);
				} else if (pos == 3) {
					holder.tv.setText(R.string.tabletennis);
					holder.img.setImageResource(R.drawable.tabletennis);
				} else if (pos == 4) {
					holder.tv.setText(R.string.gymnasium);
					holder.img.setImageResource(R.drawable.gymnasium);
				} else if (pos == 5) {
					holder.tv.setText(R.string.yogameditation);
					holder.img.setImageResource(R.drawable.yoga);
				} else if (pos == 6) {
					holder.tv.setText(R.string.separatehealthclub);
					holder.img.setImageResource(R.drawable.separatehealthclub);
				} else if (pos == 7) {
					holder.tv.setText(R.string.refugearea);
					holder.img.setImageResource(R.drawable.refugearea);
				} else if (pos == 8) {
					holder.tv.setText(R.string.banquet);
					holder.img.setImageResource(R.drawable.banquet);
				} else if (pos == 9) {
					holder.tv.setText(R.string.swimmingpool);
					holder.img.setImageResource(R.drawable.swimmingpool);
				} else if (pos == 10) {
					holder.tv.setText(R.string.toddlerpool);
					holder.img.setImageResource(R.drawable.toddlerpool);
				} else if (pos == 11) {
					holder.tv.setText(R.string.cardroom);
					holder.img.setImageResource(R.drawable.cardroom);
				} else if (pos == 12) {
					holder.tv.setText(R.string.childrenplayarea);
					holder.img.setImageResource(R.drawable.childrenplayarea);
				} else if (pos == 13) {
					holder.tv.setText(R.string.fivetemples);
					holder.img.setImageResource(R.drawable.fivetemples);
				} else if (pos == 14) {
					holder.tv.setText(R.string.partylawn);
					holder.img.setImageResource(R.drawable.partylawn);
				} else if (pos == 15) {
					holder.tv.setText(R.string.sharedgarbage);
					holder.img.setImageResource(R.drawable.sharedgarbage);
				} else if (pos == 16) {
					holder.tv.setText(R.string.privatecinema);
					holder.img.setImageResource(R.drawable.privatecinema);
				} else if (pos == 17) {
					holder.tv.setText(R.string.cabanas);
					holder.img.setImageResource(R.drawable.cabanas);
				} else if (pos == 18) {
					holder.tv.setText(R.string.skylounge);
					holder.img.setImageResource(R.drawable.skylounge);
				} else if (pos == 19) {
					holder.tv.setText(R.string.wineandcigar);
					holder.img.setImageResource(R.drawable.wineandcigar);
				} else if (pos == 20) {
					holder.tv.setText(R.string.businesscentre);
					holder.img.setImageResource(R.drawable.businesscentre);
				} else if (pos == 21) {
					holder.tv.setText(R.string.concierge);
					holder.img.setImageResource(R.drawable.concierge);
				} else if (pos == 22) {
					holder.tv.setText(R.string.amphitheatre);
					holder.img.setImageResource(R.drawable.amphitheatre);
				} else if (pos == 23) {
					holder.tv.setText(R.string.hospital);
					holder.img.setImageResource(R.drawable.hospital);
				} else if (pos == 24) {
					holder.tv.setText(R.string.school);
					holder.img.setImageResource(R.drawable.school);
				} else if (pos == 25) {
					holder.tv.setText(R.string.cycling);
					holder.img.setImageResource(R.drawable.cycling);
				} else if (pos == 26) {
					holder.tv.setText(R.string.policestation);
					holder.img.setImageResource(R.drawable.policestation);
				} else if (pos == 27) {
					holder.tv.setText(R.string.busstop);
					holder.img.setImageResource(R.drawable.busstop);
				} else if (pos == 28) {
					holder.tv.setText(R.string.firestation);
					holder.img.setImageResource(R.drawable.firestation);
				} else if (pos == 29) {
					holder.tv.setText(R.string.securitychkpost);
					holder.img.setImageResource(R.drawable.securitychkpost);
				} else if (pos == 30) {
					holder.tv.setText(R.string.joggingtrack);
					holder.img.setImageResource(R.drawable.joggingtrack);
				} else if (pos == 31) {
					holder.tv.setText(R.string.golfclub);
					holder.img.setImageResource(R.drawable.golfclub);
				} else if (pos == 32) {
					holder.tv.setText(R.string.cricketskating);
					holder.img.setImageResource(R.drawable.cricketskating);
				} else if (pos == 33) {
					holder.tv.setText(R.string.movietheatre);
					holder.img.setImageResource(R.drawable.movietheatre);
				} else if (pos == 34) {
					holder.tv.setText(R.string.countryclub);
					holder.img.setImageResource(R.drawable.countryclub);
				} else if (pos == 35) {
					holder.tv.setText(R.string.residencyclub);
					holder.img.setImageResource(R.drawable.residencyclub);
				} else if (pos == 36) {
					holder.tv.setText(R.string.themedgardens);
					holder.img.setImageResource(R.drawable.themedgardens);
				} else if (pos == 37) {
					holder.tv.setText(R.string.playground);
					holder.img.setImageResource(R.drawable.playground);
				} else if (pos == 38) {
					holder.tv.setText(R.string.yogacentre);
					holder.img.setImageResource(R.drawable.yogacentre);
				} else if (pos == 39) {
					holder.tv.setText(R.string.athletictrack);
					holder.img.setImageResource(R.drawable.athletictrack);
				} else if (pos == 40) {
					holder.tv.setText(R.string.communityhall);
					holder.img.setImageResource(R.drawable.communityhall);
				} else if (pos == 41) {
					holder.tv.setText(R.string.toddlerpool);
					holder.img.setImageResource(R.drawable.toddlerpool);
				} else if (pos == 42) {
					holder.tv.setText(R.string.temple);
					holder.img.setImageResource(R.drawable.temple);
				} else if (pos == 43) {
					holder.tv.setText(R.string.basketball);
					holder.img.setImageResource(R.drawable.basketball);
				} else if (pos == 44) {
					holder.tv.setText(R.string.library);
					holder.img.setImageResource(R.drawable.library);
				} else if (pos == 45) {
					holder.tv.setText(R.string.cafeteria);
					holder.img.setImageResource(R.drawable.cafeteria);
				} else if (pos == 46) {
					holder.tv.setText(R.string.visitortrackingsystem);
					holder.img.setImageResource(R.drawable.visitortrackingsystem);
				} else if (pos == 47) {
					holder.tv.setText(R.string.kidgamingzone);
					holder.img.setImageResource(R.drawable.kidgamingzone);
				} else if (pos == 48) {
					holder.tv.setText(R.string.restingfatreacitizen);
					holder.img.setImageResource(R.drawable.restingfatreacitizen);
				} else if (pos == 49) {
					holder.tv.setText(R.string.indoorgames);
					holder.img.setImageResource(R.drawable.indoorgames);
				} else if (pos == 50) {
					holder.tv.setText(R.string.rainwater);
					holder.img.setImageResource(R.drawable.rainwater);
				} else if (pos == 51) {
					holder.tv.setText(R.string.clubhouse);
					holder.img.setImageResource(R.drawable.clubhouse);
				}
			}

			rowView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

				}
			});
		} catch (Exception e) {
			e.printStackTrace();

		}

		return rowView;
	}
}