package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.NotificationDB;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NotificationAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater = null;
	private ImageLoader imageLoader;
	private List<NotificationDB> mnotifyList = new ArrayList<NotificationDB>();

	public NotificationAdapter(Activity a, List<NotificationDB> list) {
		activity = a;
		this.mnotifyList = list;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return mnotifyList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try {
			if (convertView == null)
				vi = inflater.inflate(R.layout.notificationrow, null);
			TextView message = (TextView) vi.findViewById(R.id.message);
			TextView datetime = (TextView) vi.findViewById(R.id.datetime);

			message.setText(mnotifyList.get(position).getMessage());
			datetime.setText(mnotifyList.get(position).getDatetime());

		} catch (Exception e) {
			e.printStackTrace();

		}

		return vi;
	}

}