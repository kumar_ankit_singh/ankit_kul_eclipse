package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;

public class SpecificationAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater = null;
	private ImageLoader imageLoader;
	private List<String> mPropertyList = new ArrayList<String>();

	public SpecificationAdapter(Activity a, List<String> list) {
		activity = a;
		this.mPropertyList = list;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return mPropertyList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try {
			if (convertView == null)
				vi = inflater.inflate(R.layout.specificationrow, null);
			TextView specName = (TextView) vi.findViewById(R.id.specname);

			specName.setText(mPropertyList.get(position).toString());

		} catch (Exception e) {
			e.printStackTrace();

		}

		return vi;
	}
}