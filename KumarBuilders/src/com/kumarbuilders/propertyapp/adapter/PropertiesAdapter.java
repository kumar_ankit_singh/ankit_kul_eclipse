package com.kumarbuilders.propertyapp.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.domain.PropertiesDetails;
import com.kumarbuilders.propertyapp.imageloader.ImageLoader;

public class PropertiesAdapter extends BaseAdapter {

	private Activity activity;
	private LayoutInflater inflater = null;
	private ImageLoader imageLoader;
	private List<PropertiesDetails> mPropertyList = new ArrayList<PropertiesDetails>();

	public PropertiesAdapter(Activity a, List<PropertiesDetails> list) {
		activity = a;
		this.mPropertyList = list;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(activity.getApplicationContext());
	}

	public int getCount() {
		return mPropertyList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		try {
			if (convertView == null)
				vi = inflater.inflate(R.layout.propertyrow, null);
			TextView mpropertyName = (TextView) vi.findViewById(R.id.propertyName);
			TextView mpropertyLoc = (TextView) vi.findViewById(R.id.propertyLocation);
			TextView propertyCity = (TextView) vi.findViewById(R.id.propertycity);

			mpropertyName.setText(mPropertyList.get(position).getProperty_name());
			mpropertyLoc.setText(mPropertyList.get(position).getProperty_address());
			propertyCity.setText(mPropertyList.get(position).getCity());

			ImageView image = (ImageView) vi.findViewById(R.id.propertyimage);

			try {
				String imageUrl = mPropertyList.get(position).getProperty_imgurl();
				final String[] urls = imageUrl.split(" ");

				imageLoader.DisplayImage(urls[0], image, position);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();

		}

		return vi;
	}
}