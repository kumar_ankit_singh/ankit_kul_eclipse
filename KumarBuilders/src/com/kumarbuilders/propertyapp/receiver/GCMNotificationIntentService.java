package com.kumarbuilders.propertyapp.receiver;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.kumarbuilders.propertyapp.R;
import com.kumarbuilders.propertyapp.activity.NotificationActivity;
import com.kumarbuilders.propertyapp.activity.NotificationPopUp;
import com.kumarbuilders.propertyapp.domain.NotificationDB;
import com.kumarbuilders.propertyapp.ormservices.NotificationService;
import com.kumarbuilders.propertyapp.ormservices.NotificationServiceImpl;
import com.kumarbuilders.propertyapp.utils.CheckNetwork;
import com.kumarbuilders.propertyapp.utils.Config;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class GCMNotificationIntentService extends IntentService implements Config {

	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;
	public static final String TAG = "GCMNotificationIntentService";
	private int numMsg = 0;
	private static HashMap<Integer, Integer> mRandomNumStorage = new HashMap<Integer, Integer>();
	NotificationService notificationService;
	private int notificationId;
	private int unreadNotification = 0;

	public GCMNotificationIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);

		String messageType = gcm.getMessageType(intent);

		Log.v("notification ", "" + messageType);
		if (!extras.isEmpty()) {
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
				sendNotification("Deleted messages on server: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

				for (int i = 0; i < 3; i++) {
					Log.i(TAG, "Working... " + (i + 1) + "/5 @ " + SystemClock.elapsedRealtime());
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
					}

				}
				Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());

				sendNotification("" + extras.get(Config.MESSAGE_KEY));
				Log.i(TAG, "Received: " + extras.toString());
			}
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);

	}

	private void sendNotification(String msg) {

		// store in db--

		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
		String strDate = sdf.format(c.getTime());
		System.out.println("Date =" + strDate);

		saveNotificationDB(msg, strDate);

		Notification notification = null;
		NotificationManager mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		notification = new Notification(R.drawable.appicon, "Notification", System.currentTimeMillis());

		int id = randomBox();
		Intent notificationIntent = new Intent(this, NotificationPopUp.class);
		notificationIntent.putExtra("message", msg);
		notificationIntent.putExtra("id", id);
		notificationIntent.putExtra("notificationId", notificationId);

		notificationIntent.setAction(id + "");
		PendingIntent intent2 = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, "Message:", msg, intent2);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		mNotificationManager.notify(id, notification);
		CheckNetwork.vibratephone(this);

	}

	private void saveNotificationDB(String msg, String strDate) {

		try {
			notificationService = new NotificationServiceImpl();
			NotificationDB notifylist = new NotificationDB();
			notificationId = randomBox();
			notifylist.setMessage(msg);
			notifylist.setDatetime(strDate);
			notifylist.setReadUnreadFlag(false);
			notifylist.setUnreadNo(unreadNotification++);

			notificationService.createOrUpdate(notifylist);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int randomBox() {

		Random rand = new Random();
		int pickedNumber = rand.nextInt(10000 + 1);

		if (mRandomNumStorage.containsKey(pickedNumber) || mRandomNumStorage.containsValue(pickedNumber)) {
			randomBox();
		} else {
			mRandomNumStorage.put(pickedNumber, pickedNumber);

		}
		return pickedNumber;

	}

}
