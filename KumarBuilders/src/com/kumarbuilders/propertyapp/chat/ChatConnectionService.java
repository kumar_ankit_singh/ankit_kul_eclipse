package com.kumarbuilders.propertyapp.chat;

import java.util.List;

import org.jivesoftware.smack.chat.Chat;

import com.kumarbuilders.propertyapp.databasehandler.ApplicationPreferences;
import com.kumarbuilders.propertyapp.domain.UserSignUp;
import com.kumarbuilders.propertyapp.ormservices.AddSignUpToDb;
import com.kumarbuilders.propertyapp.ormservices.KulApplication;
import com.kumarbuilders.propertyapp.utils.StringConstant;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.IBinder;

public class ChatConnectionService extends Service {

	public static ConnectivityManager cm;
	public static MyXMPPReceiver xmpp;
	public static boolean ServerchatCreated = false;
	public AddSignUpToDb signUpData;
	public UserSignUp signInfo;
	public String signupUserId = null;

	@Override
	public IBinder onBind(final Intent intent) {
		return new LocalBinder<ChatConnectionService>(this);
	}

	public Chat chat;

	@Override
	public void onCreate() {
		super.onCreate();
		cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		// String user
		// =KulApplication.applicationPreferences.getPreference("partnerchatId",
		// null);
		String user = "vilas@geekyworks.com";
		if (user != null) {
			String USERNAME = user + StringConstant.CHAT_SERVICE;
			xmpp = MyXMPPReceiver.getInstance(ChatConnectionService.this,
					StringConstant.CHAT_HOST, USERNAME,
					StringConstant.CHAT_PASSWORD);
			xmpp.connect("onCreate");
		}

	}

	@Override
	public int onStartCommand(final Intent intent, final int flags,
			final int startId) {
		return Service.START_NOT_STICKY;
	}

	@Override
	public boolean onUnbind(final Intent intent) {
		return super.onUnbind(intent);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		xmpp.connection.disconnect();
	}

	public static boolean isNetworkConnected() {
		return cm.getActiveNetworkInfo() != null;
	}

	public String getUserId() {
		try {
			signInfo = new UserSignUp();
			List<UserSignUp> signList = signUpData.getAllDetails();
			for (int i = 0; i < signList.size(); i++) {
				signInfo = signList.get(i);
				signupUserId = signInfo.getUserId();
			}
			return signupUserId;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return signupUserId;
	}

}
